import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
  id(Config.Plugins.androidApplication)
  id(Config.Plugins.kotlinAndroid)
  id(Config.Plugins.kotlinKapt)
  id(Config.Plugins.navigationSafeArgs)
  id(Config.Plugins.hilt)
  id(Config.Plugins.extenstion)
  id(Config.Plugins.google_services)
}

android {
  compileSdk = Config.AppConfig.compileSdkVersion

  defaultConfig {
    applicationId = Config.AppConfig.appId
    minSdk = Config.AppConfig.minSdkVersion
    targetSdk = Config.AppConfig.compileSdkVersion
    versionCode = Config.AppConfig.versionCode
    versionName = Config.AppConfig.versionName

    vectorDrawables.useSupportLibrary = true
    multiDexEnabled = true
    testInstrumentationRunner = Config.AppConfig.testRunner
  }

  signingConfigs {
    // ALREADY HAS BEEN UPLOADED TO GOOGLE PLAY BY BELOW KEY NOT PROVIDER ONE ISA.
    create("releaseConfig") {
      storeFile = file(rootProject.file("GrandKey.jks"))
      storePassword = "grand2017"
      keyAlias = "grand"
      keyPassword = "grand2017"
    }
  }

  buildTypes {
    debug {
      isMinifyEnabled = false

      proguardFiles(
        getDefaultProguardFile("proguard-android-optimize.txt"),
        "proguard-rules.pro"
      )
    }
    release {
      signingConfig = signingConfigs.getByName("releaseConfig")

      isMinifyEnabled = false
      isShrinkResources = false

      proguardFiles(
        getDefaultProguardFile("proguard-android-optimize.txt"),
        "proguard-rules.pro"
      )
    }
  }

  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
  }

  kotlinOptions {
    jvmTarget = "11"
  }

  dataBinding {
    isEnabled = true
  }
}

dependencies {
  implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

  // Networking
  implementation(Libraries.retrofit)
  implementation(Libraries.retrofitConverter)
  implementation(Libraries.gson)
  implementation(Libraries.interceptor)
  implementation(Libraries.chuckLogging)

  // Utils
  implementation(Libraries.playServices)
  implementation(Libraries.localization)
  implementation(Libraries.multidex)

  //firebase
  implementation(Libraries.firebase_platform)
  implementation(Libraries.firebase_messaging)
//  implementation(Libraries.firebase_analytics)


  //payment
  implementation(Libraries.googlePay)
  implementation(Libraries.googlePayKTX)

  // Hilt
  implementation(Libraries.hilt)
  implementation(Libraries.navigation)
//  implementation("com.google.firebase:firebase-messaging-ktx:22.0.0")
  kapt(Libraries.hiltDaggerCompiler)
  // Project Modules
  implementation(project(Config.Modules.domain))
  implementation(project(Config.Modules.data))
  implementation(project(Config.Modules.presentation))


}