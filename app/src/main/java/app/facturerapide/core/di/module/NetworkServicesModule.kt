package app.facturerapide.core.di.module

import com.structure.base_mvvm.data.account.data_source.remote.AccountServices
import com.structure.base_mvvm.data.auth.data_source.remote.AuthServices
import com.structure.base_mvvm.data.general.data_source.remote.GeneralServices
import com.structure.base_mvvm.data.home.data_source.remote.HomeServices
import com.structure.base_mvvm.data.invoice.data_source.remote.InvoiceServices
import com.structure.base_mvvm.data.search.data_source.remote.SearchServices
import com.structure.base_mvvm.data.settings.datasource.remote.SettingsServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkServicesModule {

  @Provides
  @Singleton
  fun provideAuthServices(retrofit: Retrofit): AuthServices =
    retrofit.create(AuthServices::class.java)

  @Provides
  @Singleton
  fun provideAccountServices(retrofit: Retrofit): AccountServices =
    retrofit.create(AccountServices::class.java)

  @Provides
  @Singleton
  fun provideGeneralServices(retrofit: Retrofit): GeneralServices =
    retrofit.create(GeneralServices::class.java)

  @Provides
  @Singleton
  fun provideSearchServices(retrofit: Retrofit): SearchServices =
    retrofit.create(SearchServices::class.java)

  @Provides
  @Singleton
  fun provideHomeServices(retrofit: Retrofit): HomeServices =
    retrofit.create(HomeServices::class.java)

  @Provides
  @Singleton
  fun provideSettingsServices(retrofit: Retrofit): SettingsServices =
    retrofit.create(SettingsServices::class.java)

  @Provides
  @Singleton
  fun provideInvoiceServices(retrofit: Retrofit): InvoiceServices =
    retrofit.create(InvoiceServices::class.java)
}