package app.facturerapide.core.di.module

import android.content.Context
import android.util.Log
import app.facturerapide.core.BuildConfig
import app.facturerapide.core.MyApplication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.structure.base_mvvm.data.local.preferences.AppPreferences
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
//import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

  const val REQUEST_TIME_OUT: Long = 60

  private const val TAG = "RetrofitModule"
  @Provides
  @Singleton
  fun provideHeadersInterceptor(appPreferences: AppPreferences) =
    Interceptor { chain ->
      Log.d(TAG, "token: "+appPreferences.getKey(Constants.TOKEN))
      Log.d(TAG, "language: "+LanguagesHelper.getCurrentLanguage(MyApplication.instance))
      var lang = LanguagesHelper.getCurrentLanguage(MyApplication.instance)
      chain.proceed(
        chain.request().newBuilder()
          .addHeader("Authorization", "Bearer ${appPreferences.getKey(Constants.TOKEN) ?: ""}")
          .addHeader("Accept", "application/json")
          .addHeader("language", LanguagesHelper.getCurrentLanguage(MyApplication.instance))
          .addHeader("platform", "Android")
          .addHeader("version", "${BuildConfig.VERSION_CODE}")
          .build()
      )
    }

  @Provides
  @Singleton
  fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
      override fun log(message: String) {
        Log.e("HttpLogging", "HttpLogging msg -> $message")
      }
    })
    logging.level = HttpLoggingInterceptor.Level.BODY
    return logging
  }

  @Provides
  @Singleton
  fun provideOkHttpClient(
    headersInterceptor: Interceptor,
    logging: HttpLoggingInterceptor,
    @ApplicationContext context: Context
  ): OkHttpClient {
    return if (BuildConfig.DEBUG) {
      OkHttpClient.Builder()
        .readTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
        .connectTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
        .addInterceptor(headersInterceptor)
        .addNetworkInterceptor(logging)
//        .addInterceptor(ChuckInterceptor(context))
        .build()
    } else {
      OkHttpClient.Builder()
        .readTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
        .connectTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
        .addInterceptor(headersInterceptor)
        .addNetworkInterceptor(logging)
        .build()
    }
  }

  @Provides
  @Singleton
  fun provideGson(): Gson {
    return GsonBuilder()
      .setLenient()
      .serializeNulls() // To allow sending null values
      .create()
  }

  @Provides
  @Singleton
  fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create(gson))
    .baseUrl("http://facturerapide.com/api/")
    .build()
}