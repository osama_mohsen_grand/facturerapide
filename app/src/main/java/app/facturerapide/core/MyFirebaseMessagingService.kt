package app.facturerapide.core

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.structure.base_mvvm.data.local.preferences.AppPreferences
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.lifecycle.HiltViewModel

//import com.readystatesoftware.chuck.internal.ui.MainActivity

class MyFirebaseMessagingService() :
  FirebaseMessagingService() {
  override fun onMessageReceived(remoteMessage: RemoteMessage) {
    sendNotification(remoteMessage.data)
  }

  override fun onNewToken(token: String) {
  }

  private fun sendNotification(messageBody: MutableMap<String, String>) {
    val generalUseCases = AppPreferences(this);
    if (generalUseCases.isLoggedIn()) {
      val bundle = Bundle()
      bundle.putBoolean(Constants.NOTIFICATION,true)
//    if (generalUseCases.checkLoggedInUserUseCase.invoke()) {
      val pendingIntent = NavDeepLinkBuilder(this)
        .setComponentName(HomeActivity::class.java)
        .setGraph(R.navigation.nav_home)
        .setDestination(R.id.notificationFragment)
        .setArguments(bundle)
        .createPendingIntent()


      val channelId = "channelId"
      val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
      val notificationBuilder = NotificationCompat.Builder(this, channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setContentTitle(messageBody["title"])
        .setContentText(messageBody["body"])
        .setAutoCancel(true)
        .setSound(defaultSoundUri)
        .setNumber(0)
        .setContentIntent(pendingIntent)

      val notificationManager =
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


      // Since android Oreo notification channel is needed.
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(
          channelId,
          "Channel human readable title",
          NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(channel)
      }

      notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
//    }
  }
}