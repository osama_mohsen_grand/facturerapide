object Config {
  object AppConfig {
    const val appId = "app.facturerapide.core1"
    const val compileSdkVersion = 31
    const val minSdkVersion = 23
    const val versionCode = 12
    const val versionName = "2.1.1"
    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
  }

  object Dependencies {
    const val jitPackURL = "https://jitpack.io"
    const val gradleVersion = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val navigationSafeArgs =
      "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.androidNavigation}"
    const val hilt = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hiltVersion}"
    const val google_services = "com.google.gms:google-services:${Versions.google_services}"
  }

  object Plugins {
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinKapt = "kotlin-kapt"
    const val navigationSafeArgs = "androidx.navigation.safeargs"
    const val hilt = "dagger.hilt.android.plugin"
    const val androidLibrary = "com.android.library"
    const val kotlinJvm = "org.jetbrains.kotlin.jvm"
    const val ktLint = "org.jlleitschuh.gradle.ktlint"
    const val google_services = "com.google.gms.google-services"
    const val extenstion = "kotlin-android-extensions"
  }

  object Modules {
    const val domain = ":domain"
    const val app = ":app"
    const val data = ":data"
    const val presentation = ":presentation"
    const val prettyPopUp = ":prettyPopUp"
    const val appTutorial = ":appTutorial"
    const val imagesSlider = ":imagesSlider"

  }

  object Environments {
    const val debugBaseUrl = "\"https://facturerapide.com/api/\""
    const val releaseBaseUrl = "\"https://facturerapide.com/api/\""
  }
}