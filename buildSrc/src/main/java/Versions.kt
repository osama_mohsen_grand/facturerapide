object Versions {
  const val gradleVersion = "7.0.2"
  const val kotlin = "1.5.21"
  const val ktLint = "10.2.0"

  // Java Inject
  const val javaInject = "1"

  // Support
  const val appcompat = "1.3.1"
  const val coreKtx = "1.6.0"
  const val supportVersion = "1.0.0"

  // Arch Components
  const val lifecycle = "2.4.0-alpha03"
  const val paging_version = "2.1.2"

  // Kotlin Coroutines
  const val kotlinCoroutines = "1.4.1"

  // Networking
  const val retrofit = "2.9.0"
  const val gson = "2.8.8"
  const val interceptor = "4.8.1"
  const val chuckLogging = "1.1.0"

  // UI
  const val materialDesign = "1.4.0"
  const val androidNavigation = "2.4.0-alpha01"
  const val loadingAnimations = "1.4.0"
  const val alerter = "7.2.4"
  const val coil = "1.3.2"

  // Utils
  const val playServices = "19.2.0"
  const val localization = "1.1.4"
  const val multidex = "2.0.1"
  const val permissions = "3.0.0-RC4"

  // Hilt
  const val hiltVersion = "2.38.1"

  // Map
  const val map = "17.0.1"
  const val playServicesLocation = "18.0.0"
  const val rxLocation = "1.0"

  //Fire base
  const val firebase = "29.0.3"
  const val google_services = "4.3.10"

  //ted bottom picker
  const val ted_bottom_picker = "2.0.1"
  //cards slider
  const val cards_slider = "1.0.1"
  //Pin View
  const val pin_code = "1.4.3"
  //smarteist
  const val smarteist="1.3.2-appcompat"
  //webview
  const val webView="v3.2.1"
  //lottie
  const val lottie="3.4.0"
  //badge
  const val notificationBadge="1.0.4"
  //lottie
  const val pdfViewer="2.7.0-beta.3"
  //google pay
  const val googlePay="5.1.0"

}