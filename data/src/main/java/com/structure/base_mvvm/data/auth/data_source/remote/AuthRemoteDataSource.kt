package com.structure.base_mvvm.data.auth.data_source.remote

import android.util.Log
import com.structure.base_mvvm.data.remote.BaseRemoteDataSource
import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
import com.structure.base_mvvm.domain.auth.entity.request.*
import javax.inject.Inject


class AuthRemoteDataSource @Inject constructor(private val apiService: AuthServices) :
  BaseRemoteDataSource() {

  private val TAG = "AuthRemoteDataSource"

  suspend fun logIn(request: LogInRequest) = safeApiCall {
    apiService.logIn(request)
  }

  suspend fun forgetPassword(request: ForgetPasswordRequest) = safeApiCall {
    apiService.forgetPassword(request)
  }

  suspend fun changePassword(request: ChangePasswordRequest) = safeApiCall {
    apiService.changePassword(request)
  }

  suspend fun changePasswordForForget(request: ChangePasswordRequest) = safeApiCall {
    apiService.changePasswordForForget(request)
  }

  suspend fun register(request: RegisterRequest) = safeApiCall {
    when (request.images.size) {
      0 -> apiService.register(request)
      else -> apiService.register(getParameters(request), request.images[0])
    }
  }


  suspend fun profileUpdate(request: RegisterRequest) = safeApiCall {
    when (request.images.size) {
      0 -> apiService.profileUpdate(request)
      else -> apiService.profileUpdate(getParameters(request), request.images[0])
    }

  }

//  suspend fun registerCompany(request: RegisterCompanyRequest) = safeApiCall {
//    when(request.isLogin){
//      true -> {
//        apiService.updateCompany(getParameters(request), request.image)
//      }
//      else -> {
//        apiService.registerCompany(getParameters(request), request.image)
//      }
//    }
//  }

  suspend fun registerCompany(request: RegisterCompanyRequest) = safeApiCall {
//    Log.d(TAG, "register: yes here yes there")
//    val name: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.name
//    )
//    val email: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.email
//    )
//    val phone: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.phone
//    )
//    val address: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.address
//    )
//    val street: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.street
//    )
//    val postal: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.postalCode
//    )
//    val commercial: RequestBody = RequestBody.create(
//      MultipartBody.FORM, request.commercialNumber
//    )
    Log.d(TAG, "registerCompany: " + request.isLogin + " , " + request.registerSteps)
    when (request.isLogin) {
      true -> {
        when (request.images.size) {
          0 -> apiService.updateCompany(request)
          else -> apiService.updateCompany(getParameters(request), request.images[0])
        }
      }
      else -> {
        when(request.images.size){
          0 -> apiService.registerCompany(request)
          else -> apiService.registerCompany(getParameters(request), request.images[0])
        }
      }
    }


  }


  suspend fun confirmCode(request: ConfirmCodeRequest) = safeApiCall {
    apiService.confirmCode(request)
  }

  suspend fun sendCode(request: ConfirmCodeRequest) = safeApiCall {
    apiService.sendCode(request)
  }

  suspend fun getPackages() = safeApiCall {
    apiService.getPackages()
  }

  suspend fun subscribe(request: PackageGooglePayRequest) = safeApiCall {
    apiService.subscribe(request)
  }
}