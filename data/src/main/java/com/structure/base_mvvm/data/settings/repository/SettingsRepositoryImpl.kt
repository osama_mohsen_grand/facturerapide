package com.structure.base_mvvm.data.settings.repository

import com.structure.base_mvvm.data.settings.datasource.remote.SettingsRemoteDataSource
import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.settings.repository.SettingsRepository
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import javax.inject.Inject

class SettingsRepositoryImpl @Inject constructor(private val remoteDataSource: SettingsRemoteDataSource) :
  SettingsRepository {
  override suspend fun welcome(): Resource<BaseResponse<IntroModel>> = remoteDataSource.welcome()
  override suspend fun info(type: String): Resource<BaseResponse<IntroModel>> = remoteDataSource.info(type)
  override suspend fun support(request: SupportRequest): Resource<BaseResponse<*>> = remoteDataSource.support(request)
  override suspend fun notifications(page:Int): Resource<BaseResponse<NotificationPaginateData>>  = remoteDataSource.getNotifications(page)
  override suspend fun deleteNotification(id:Int): Resource<BaseResponse<*>>  = remoteDataSource.deleteNotification(id)
}