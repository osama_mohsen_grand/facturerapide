package com.structure.base_mvvm.data.auth.data_source.remote

import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.*
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.BaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface AuthServices {

  @POST("v1/auth/login")
  suspend fun logIn(@Body request: LogInRequest): BaseResponse<UserModel>

  @Multipart
  @POST("v1/auth/register")
  suspend fun registerCompany(
    @QueryMap map: Map<String, String>,
    @Part image: MultipartBody.Part? = null
  ): BaseResponse<UserModel>

  @POST("v1/auth/register")
  suspend fun registerCompany(
    @Body request: RegisterCompanyRequest
  ): BaseResponse<UserModel>


  @Multipart
  @POST("v1/company")
  suspend fun updateCompany(
    @QueryMap map: Map<String, String>,
    @Part image: MultipartBody.Part? = null
  ): BaseResponse<UserModel>

  @POST("v1/company")
  suspend fun updateCompany(@Body request: RegisterCompanyRequest): BaseResponse<UserModel>


//  @Part("company_name") name: RequestBody,
//  @Part("company_email") email: RequestBody,
//  @Part("company_phone") phone: RequestBody,
//  @Part("company_address") address: RequestBody,
//  @Part("company_commercial_register") commercialNumber: RequestBody,
//  @Part("company_street") street: RequestBody,
//  @Part("company_postal_code") postalCode: RequestBody,




  @Multipart
  @POST("v1/auth/register")
  suspend fun register(
    @QueryMap map: Map<String, String>,
    @Part image: MultipartBody.Part?
  ): BaseResponse<*>


  @POST("v1/auth/register")
  suspend fun register(
    @Body request: RegisterRequest
  ): BaseResponse<*>


  @Multipart
  @POST("v1/profile")
  suspend fun profileUpdate(
    @QueryMap map: Map<String, String>,
    @Part image: MultipartBody.Part? = null
  ): BaseResponse<UserModel>

  @POST("v1/profile")
  suspend fun profileUpdate(
    @Body request: RegisterRequest
  ): BaseResponse<UserModel>




  @POST("v1/auth/send_code")
  suspend fun forgetPassword(@Body request: ForgetPasswordRequest): BaseResponse<*>

  @POST("v1/profile/password")
  suspend fun changePassword(@Body request: ChangePasswordRequest): BaseResponse<*>

  @POST("v1/change_password")
  suspend fun changePasswordForForget(@Body request: ChangePasswordRequest): BaseResponse<*>


  @POST("v1/auth/verify_code")
  suspend fun confirmCode(@Body request: ConfirmCodeRequest): BaseResponse<UserModel>

  @POST("v1/auth/send_code")
  suspend fun sendCode(@Body request: ConfirmCodeRequest): BaseResponse<UserModel>

  @GET("v1/packages")
  suspend fun getPackages(): BaseResponse<List<Package>>

  @POST("v1/set-payment-android")
  suspend fun subscribe(@Body request: PackageGooglePayRequest): BaseResponse<*>
}