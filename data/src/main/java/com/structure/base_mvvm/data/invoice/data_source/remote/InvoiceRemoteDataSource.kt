package com.structure.base_mvvm.data.invoice.data_source.remote

import com.structure.base_mvvm.data.remote.BaseRemoteDataSource
import com.structure.base_mvvm.domain.auth.entity.request.LogInRequest
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import javax.inject.Inject

class InvoiceRemoteDataSource @Inject constructor(private val apiService: InvoiceServices) :
  BaseRemoteDataSource() {
  suspend fun createInvoice(request: InvoiceRequest) = safeApiCall {
    apiService.createInvoice(request)
  }

  suspend fun getInvoices(id: Int,page:Int,search: String) = safeApiCall {
    println("get Invoices DataSource")
    apiService.getInvoices(id,page,search)
  }

  suspend fun getInvoiceDetails(id: Int) = safeApiCall {
    println("get Invoices DataSource")
    apiService.getInvoiceDetails(id)
  }

  suspend fun getCustomers(page: Int) = safeApiCall {
    println("get Invoices DataSource")
    apiService.getCustomers(page)
  }

  suspend fun getServices(page: Int) = safeApiCall {
    println("get Invoices DataSource")
    apiService.getServices(page)
  }




  suspend fun update(id: Int,request: InvoiceRequest) = safeApiCall {
    println("get Invoices DataSource")
    apiService.updateInvoice(id,request)
  }

  suspend fun deleteInvoice(id: Int) = safeApiCall {
    println("get Invoices DataSource")
    apiService.deleteInvoice(id)
  }
}