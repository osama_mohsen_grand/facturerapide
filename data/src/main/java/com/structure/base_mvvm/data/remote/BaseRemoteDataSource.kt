package com.structure.base_mvvm.data.remote

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.ErrorResponse
import com.structure.base_mvvm.domain.utils.FailureStatus
import com.structure.base_mvvm.domain.utils.Resource
import org.json.JSONObject
import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.HashMap
import javax.inject.Inject

open class BaseRemoteDataSource @Inject constructor() {
  var gson: Gson = Gson()
  protected fun getParameters(requestData: Any): Map<String, String> {
    val params: MutableMap<String, String> = HashMap()
    try {
      val jsonObject = JSONObject(gson.toJson(requestData))
      for (i in 0 until jsonObject.names().length()) {
        params[jsonObject.names().getString(i)] =
          jsonObject[jsonObject.names().getString(i)].toString() + ""
      }
    } catch (e: Exception) {
      e.stackTrace
    }
    return params
  }


  suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T> {
    println("Start Call $apiCall")
    try {
      val apiResponse = apiCall.invoke()
      println("=> $apiResponse")
      when ((apiResponse as BaseResponse<*>).code) {
        200 -> {
          return Resource.Success(apiResponse)
        }
        401 -> {
          return Resource.Failure(
            FailureStatus.EMPTY,
            (apiResponse as BaseResponse<*>).code,
            (apiResponse as BaseResponse<*>).message
          )
        }
        403 -> {
          return Resource.Failure(FailureStatus.TOKEN_EXPIRED)
        }
        405 -> {
          return Resource.Failure(FailureStatus.VERIFY_CODE)
        }
        else -> {
          return Resource.Failure(FailureStatus.API_FAIL)
        }
      }
    } catch (throwable: Throwable) {
      println(throwable)
      when (throwable) {
        is HttpException -> {
          when(throwable.code()) {
            422 -> {
              val jObjError = JSONObject(throwable.response()!!.errorBody()!!.string())
              val apiResponse = jObjError.toString()
              val response = Gson().fromJson(apiResponse, BaseResponse::class.java)

              return Resource.Failure(FailureStatus.API_FAIL, throwable.code(), response.message)
            }
            403 -> {
              return Resource.Failure(
                FailureStatus.TOKEN_EXPIRED,
                throwable.code(),
                ""
              )
            }
            else -> {
              return if (throwable.response()?.errorBody()!!.charStream().readText()
                  .isNullOrEmpty()
              ) {
                Resource.Failure(FailureStatus.API_FAIL, throwable.code())
              } else {
                try {
                  return Resource.Failure(FailureStatus.EMPTY,throwable.code(),throwable.response()?.message())
                } catch (ex: JsonSyntaxException) {
                  Resource.Failure(FailureStatus.EMPTY, throwable.code())
                }
              }
            }
          }
        }

        is UnknownHostException -> {
          return Resource.Failure(FailureStatus.NO_INTERNET)
        }

        is ConnectException -> {
          return Resource.Failure(FailureStatus.NO_INTERNET)
        }

        else -> {
          return Resource.Failure(FailureStatus.OTHER)
        }
      }
    }
  }
}