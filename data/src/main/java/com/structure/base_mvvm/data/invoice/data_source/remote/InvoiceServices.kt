package com.structure.base_mvvm.data.invoice.data_source.remote

import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.LogInRequest
import com.structure.base_mvvm.domain.customer.CustomersPaginateData
import com.structure.base_mvvm.domain.invoice.model.InvoiceDetailsResponse
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.service.ServicesPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import retrofit2.http.*

interface InvoiceServices{
  @POST("v1/invoice")
  suspend fun createInvoice(@Body request: InvoiceRequest): BaseResponse<*>

  @GET("v1/invoice")
  suspend fun getInvoices( @Query("invoice_type_id") invoice_type_id: Int,@Query("page") page: Int,@Query("search") search: String): BaseResponse<InvoicePaginateData>


  @GET("v1/invoice/{id}")
  suspend fun getInvoiceDetails( @Path("id") id: Int): BaseResponse<InvoiceDetailsResponse>

  @GET("v1/customers")
  suspend fun getCustomers(@Query("page") page: Int): BaseResponse<CustomersPaginateData>


  @GET("v1/services")
  suspend fun getServices(@Query("page") page: Int): BaseResponse<ServicesPaginateData>




  @PUT("v1/invoice/{id}")
  suspend fun updateInvoice(@Path("id") id: Int,@Body request: InvoiceRequest): BaseResponse<*>

  @DELETE("v1/invoice/{id}")
  suspend fun deleteInvoice(@Path("id") id: Int): BaseResponse<*>
}