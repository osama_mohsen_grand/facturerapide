package com.structure.base_mvvm.data.local.preferences

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.utils.Constants
import javax.inject.Inject

class AppPreferences @Inject constructor(context: Context) {

  companion object {
    private const val APP_PREFERENCES_NAME = "APP-NAME-Cache"
    private const val SESSION_PREFERENCES_NAME = "APP-NAME-UserCache"
    private const val MODE = Context.MODE_PRIVATE

    private val USER_DATA = Pair("USER_DATA", "")
    private val FIRST_TIME = Pair("FIRST_TIME", true)
    private val FIREBASE_TOKEN = Pair("FIREBASE_TOKEN", "")
    private val USER_TOKEN = Pair("USER_TOKEN", "")
    private val TOKEN = Pair("TOKEN", "")
  }

  private val appPreferences: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES_NAME, MODE)
  private val sessionPreferences: SharedPreferences =
    context.getSharedPreferences(SESSION_PREFERENCES_NAME, MODE)

  /**
   * SharedPreferences extension function, so we won't need to call edit() and apply()
   * ourselves on every SharedPreferences operation.
   */
  private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
    val editor = edit()
    operation(editor)
    editor.apply()
  }

  var userModelData: UserModel
    get() {
      val value: String? = sessionPreferences.getString(USER_DATA.first, USER_DATA.second)
      return Gson().fromJson(value, UserModel::class.java)
    }
    set(value) = sessionPreferences.edit {
      Log.d(TAG, "save USER HERE "+value.token)
      it.putString(USER_DATA.first, Gson().toJson(value))
      it.putString(Constants.TOKEN, value.token)
      it.putInt("id",1)
      it.apply()
    }

  var isFirstTime: Boolean
    get() {
      return appPreferences.getBoolean(FIRST_TIME.first, FIRST_TIME.second)
    }
    set(value) = appPreferences.edit {
      it.putBoolean(FIRST_TIME.first, value)
    }


  fun getKey(key: String) : String {
    return sessionPreferences.getString(key,"").toString()
  }

  fun saveKey(key: String, value: String){
    sessionPreferences.edit {
      it.putString(key,value)
    }
  }

  private val TAG = "AppPreferences"

  var firebaseToken: String?
    get() {
      return sessionPreferences.getString(FIREBASE_TOKEN.first, FIREBASE_TOKEN.second)
    }
    set(value) = sessionPreferences.edit {
      it.putString(FIREBASE_TOKEN.first, value)
    }

  fun clearPreferences() {
    sessionPreferences.edit {
      it.clear().apply()
    }
  }

  fun isLoggedIn() : Boolean {
    Log.d(TAG, "isLoggedIn: "+sessionPreferences.getInt("id",-1))
    return sessionPreferences.getInt("id",-1) != -1
  }

  fun clearUserPreferences() {
    sessionPreferences.edit {
      it.putString(USER_DATA.first, null)
      it.apply()
    }
  }
}