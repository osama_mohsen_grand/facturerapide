package com.structure.base_mvvm.data.account.repository

import android.util.Log
import com.structure.base_mvvm.data.account.data_source.remote.AccountRemoteDataSource
import com.structure.base_mvvm.data.local.preferences.AppPreferences
import com.structure.base_mvvm.domain.account.entity.request.SendFirebaseTokenRequest
import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.utils.Constants
import javax.inject.Inject

class AccountRepositoryImpl @Inject constructor(
  private val remoteDataSource: AccountRemoteDataSource,
  private val appPreferences: AppPreferences
) : AccountRepository {

  override
  suspend fun sendFirebaseToken(request: SendFirebaseTokenRequest) = remoteDataSource.sendFirebaseToken(request)

  override
  suspend fun logOut() = appPreferences.clearPreferences()

  override
  fun isFirstTime() = appPreferences.isFirstTime

  override
  fun isLoggedIn() = appPreferences.isLoggedIn()

  override
  fun saveFirebaseTokenToLocal(firebaseToken: String) {
    appPreferences.firebaseToken = firebaseToken
  }

  //email not used , but put it because of invoce method
  override fun saveUserToken(token: String, email: String) {
    appPreferences.saveKey(Constants.TOKEN,token)
  }

  override fun getKey(key: String) : String {
    return appPreferences.getKey(key)
  }

  override
  fun getFirebaseToken() = appPreferences.firebaseToken

  private  val TAG = "AccountRepositoryImpl"
  override
  fun saveUserToLocal(userModel: UserModel) {
    appPreferences.userModelData = userModel
    Log.d(TAG, "saveUserToLocal: "+appPreferences.getKey(Constants.TOKEN))
  }

  override fun getUserLocal(): UserModel {
    return appPreferences.userModelData
  }

  override
  fun setFirstTime(isFirstTime: Boolean) {
    appPreferences.isFirstTime = isFirstTime
  }

  override
  fun clearPreferences() = appPreferences.clearPreferences()

  override fun clearUser() {
    appPreferences.clearUserPreferences()
  }
}