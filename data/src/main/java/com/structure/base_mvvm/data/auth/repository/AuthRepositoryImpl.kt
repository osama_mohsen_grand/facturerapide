package com.structure.base_mvvm.data.auth.repository

import com.structure.base_mvvm.data.auth.data_source.remote.AuthRemoteDataSource
import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.*
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
  private val remoteDataSource: AuthRemoteDataSource
) : AuthRepository {

  override suspend fun logIn(request: LogInRequest) = remoteDataSource.logIn(request)
  override suspend fun forgetPassword(request: ForgetPasswordRequest) = remoteDataSource.forgetPassword(request)
  override suspend fun changePassword(request: ChangePasswordRequest) = remoteDataSource.changePassword(request)
  override suspend fun changePasswordInForget(request: ChangePasswordRequest): Resource<BaseResponse<*>> = remoteDataSource.changePasswordForForget(request)

  override suspend fun register(request: RegisterRequest) = remoteDataSource.register(request)
  override suspend fun registerCompany(request: RegisterCompanyRequest): Resource<BaseResponse<UserModel>> = remoteDataSource.registerCompany(request)
  override suspend fun profileUpdate(request: RegisterRequest) = remoteDataSource.profileUpdate(request)
//  override suspend fun registerCompany(request: RegisterCompanyRequest): Resource<BaseResponse<User>> = remoteDataSource.registerCompany(request)

  override suspend fun confirmCode(request: ConfirmCodeRequest) = remoteDataSource.confirmCode(request)
  override suspend fun sendCode(request: ConfirmCodeRequest) = remoteDataSource.sendCode(request)

  override suspend fun packages(): Resource<BaseResponse<List<Package>>> = remoteDataSource.getPackages()
  override suspend fun subscribe(request: PackageGooglePayRequest): Resource<BaseResponse<*>> = remoteDataSource.subscribe(request)

  override suspend fun updateProfile(request: ForgetPasswordRequest): Resource<BaseResponse<UserModel>> {
    TODO("Not yet implemented")
  }

}