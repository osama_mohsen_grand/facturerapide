package com.structure.base_mvvm.data.home.data_source.remote

import com.structure.base_mvvm.domain.home.models.HomeModel
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeServices {
  @GET("v1/client/notifications?")
  suspend fun getNotifications(@Query("page=")page: Int): BaseResponse<NotificationPaginateData>

  @GET("v1/home")
  suspend fun home(@Query("device_id")deviceId: String,@Query("device_token")deviceToken: String): BaseResponse<HomeModel>
}