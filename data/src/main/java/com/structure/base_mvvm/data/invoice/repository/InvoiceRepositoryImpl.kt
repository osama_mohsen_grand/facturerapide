package com.structure.base_mvvm.data.invoice.repository

import com.structure.base_mvvm.data.invoice.data_source.remote.InvoiceRemoteDataSource
import com.structure.base_mvvm.data.search.data_source.remote.SearchRemoteDataSource
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.invoice.model.InvoiceDetailsResponse
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.repository.InvoiceRepository
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.search.repository.SearchRepository
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import javax.inject.Inject

class InvoiceRepositoryImpl @Inject constructor(private val remoteDataSource: InvoiceRemoteDataSource) :
  InvoiceRepository{
  override suspend fun createInvoice(request: InvoiceRequest) = remoteDataSource.createInvoice(request)
  override suspend fun getInvoices(id: Int,page:Int,search:String) = remoteDataSource.getInvoices(page,id,search)
  override suspend fun getInvoiceDetails(id: Int) = remoteDataSource.getInvoiceDetails(id)
  override suspend fun getCustomers(page: Int) = remoteDataSource.getCustomers(page)
  override suspend fun getServices(page: Int) = remoteDataSource.getServices(page)

  override suspend fun deleteInvoice(id: Int) = remoteDataSource.deleteInvoice(id)
  override suspend fun update(id:Int, request: InvoiceRequest) = remoteDataSource.update(id,request)
}