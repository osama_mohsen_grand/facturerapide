package com.structure.base_mvvm.data.home.repository

import com.structure.base_mvvm.data.home.data_source.remote.HomeRemoteDataSource
import com.structure.base_mvvm.domain.home.models.HomeModel
import com.structure.base_mvvm.domain.home.repository.HomeRepository
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(private val remoteDataSource: HomeRemoteDataSource) :
  HomeRepository {
  override suspend fun notifications(page: Int): Resource<BaseResponse<NotificationPaginateData>> =
    remoteDataSource.getNotifications(page)

  override suspend fun home(
    device_id: String,
    device_token: String
  ): Resource<BaseResponse<HomeModel>>  = remoteDataSource.home(device_id,device_token)
}