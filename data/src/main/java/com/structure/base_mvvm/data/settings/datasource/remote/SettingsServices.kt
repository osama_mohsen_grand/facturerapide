package com.structure.base_mvvm.data.settings.datasource.remote

import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import com.structure.base_mvvm.domain.utils.BaseResponse
import retrofit2.http.*

interface SettingsServices {
  //  suspend fun home(@Query("page=")page: Int): BaseResponse<HomePaginateData>
  @GET("v1/welcome")
  suspend fun welcome(): BaseResponse<IntroModel>

  @GET("v1/settings")
  suspend fun settings(@Query("type") type: String): BaseResponse<IntroModel>

  @POST("v1/contact_app")
  suspend fun support(@Body request: SupportRequest): BaseResponse<*>

  @GET("v1/notifications")
  suspend fun getNotifications(@Query("page") page: Int): BaseResponse<NotificationPaginateData>

  @DELETE("v1/notifications/{id}")
  suspend fun deleteNotification(@Path("id") id: Int): BaseResponse<*>
}