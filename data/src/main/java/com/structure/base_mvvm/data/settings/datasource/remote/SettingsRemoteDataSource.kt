package com.structure.base_mvvm.data.settings.datasource.remote

import android.util.Log
import com.structure.base_mvvm.data.remote.BaseRemoteDataSource
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import javax.inject.Inject

class SettingsRemoteDataSource @Inject constructor(private val apiService: SettingsServices) :
  BaseRemoteDataSource() {

  suspend fun welcome() = safeApiCall {
    apiService.welcome()
  }

  suspend fun info(type: String) = safeApiCall {
    apiService.settings(type)
  }

  suspend fun support(request: SupportRequest) = safeApiCall {
    println("support before api")
    apiService.support(request)
  }


    suspend fun getNotifications(page: Int) = safeApiCall {
      println("support before api")
      apiService.getNotifications(page)
    }

  suspend fun deleteNotification(id: Int) = safeApiCall {
    println("support before api")
    apiService.deleteNotification(id)
  }
}