package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.RegisterCompanyRequest
import com.structure.base_mvvm.domain.auth.entity.request.RegisterValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class RegisterCompanyUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val generalUseCases: GeneralUseCases,
  private val saveUserToLocalUseCase: SaveUserToLocalUseCase
) {

  private val TAG = "RegisterUseCase"
  val user = MutableStateFlow<Resource<BaseResponse<UserModel>>>(Resource.Default)

  @Throws(RegisterValidationException::class)
  fun submit(request: RegisterCompanyRequest): Flow<Resource<BaseResponse<UserModel>>> = flow {
    println(request.toString())
//    if (request.name.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_NAME.value.toString())
//    }
//    if (request.address.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_ADDRESS.value.toString())
//    }
//    if (request.street.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_POSTAL_CODE.value.toString())
//    }
//    if (request.phone.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_PHONE.value.toString())
//    }
//    if (request.email.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
//    }
//
//    if (request.email.isNotEmpty() && !request.email.isValidEmail()) {
//      throw RegisterValidationException(AuthValidation.INVALID_EMAIL.value.toString())
//    }
//    if (request.commercialNumber.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_COMMERCIAL.value.toString())
//    }
//    if (request.companyVatNumber.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_VAT.value.toString())
//    }



//    if (request.street.isEmpty()) {
//      throw RegisterValidationException(AuthValidation.EMPTY_STREET.value.toString())
//    }




//    if (request.image == null && !request.isLogin) {
//      throw RegisterValidationException(AuthValidation.IMAGE.value.toString())
//    }

    print("imagePath:"+request.imagePath)

    emit(Resource.Loading)
    val result = authRepository.registerCompany(request)
    if (result is Resource.Success) {
//      when (request.isLogin) {
//        true -> {
          val user = (result.value.data as UserModel)
          saveUserToLocalUseCase(user)
//        }
//        else -> generalUseCases.clearPreferencesUseCase.invoke()
//      }

//      result.value.data?.let { saveUserToLocalUseCase(it) }
    }
    emit(result)
  }.flowOn(Dispatchers.IO)
}