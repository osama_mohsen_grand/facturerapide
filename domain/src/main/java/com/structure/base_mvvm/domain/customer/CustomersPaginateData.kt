package com.structure.base_mvvm.domain.customer

import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.general.paginate.Links
import com.structure.base_mvvm.domain.general.paginate.Meta
import com.structure.base_mvvm.domain.general.paginate.Paginate

class CustomersPaginateData(
  @SerializedName("data")
  val list: ArrayList<CustomerModel> = arrayListOf(), meta: Meta= Meta(), links: Links=Links()
) : Paginate(meta, links)