package com.structure.base_mvvm.domain.account.repository

import com.structure.base_mvvm.domain.account.entity.request.SendFirebaseTokenRequest
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource

interface AccountRepository {

  suspend fun sendFirebaseToken(request: SendFirebaseTokenRequest): Resource<BaseResponse<Boolean>>

  suspend fun logOut()

  fun isFirstTime(): Boolean

  fun isLoggedIn(): Boolean

  fun saveFirebaseTokenToLocal(firebaseToken: String)

  fun saveUserToken(token: String, email: String)

  fun getKey(key: String) : String

  fun getFirebaseToken(): String?

  fun saveUserToLocal(userModel: UserModel)

  fun getUserLocal() : UserModel

  fun setFirstTime(isFirstTime: Boolean)

  fun clearPreferences()

  fun clearUser()
}