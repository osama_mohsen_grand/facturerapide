package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LogInRequest(
  @SerializedName("email")
  @Expose
  var email: String = "",
  @SerializedName("password")
  @Expose
  var password: String = "",
  @SerializedName("device_token")
  @Expose
  var device_token: String = ""
){}
class LogInValidationException(private val validationType: String) : Exception(validationType)