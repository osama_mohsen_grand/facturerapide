package com.structure.base_mvvm.domain.auth.entity.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Company(
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("email")
    var email: String? = "",
    @SerializedName("phone")
    var phone: String? = "",
    @SerializedName("image")
    var image: String? = "",
    @SerializedName("commercial_register")
    var commercialNumber: String? = "",
    @SerializedName("address")
    var address: String? = "",
    @SerializedName("address_2")
    var address2: String? = "",
    @SerializedName("street")
    var street: String? = "",
    @SerializedName("postal_code")
    var postalCode: String? = "",
    @SerializedName("image_size")
    @Expose
    var image_size: String? = "s",
    @SerializedName("vat_number")
    @Expose
    var companyVatNumber: String? = "",
    @SerializedName("website")
    @Expose
    var website: String? = "",
    @SerializedName("invoice_color")
    @Expose
    var invoiceColor: String? = "",
)