package com.structure.base_mvvm.domain.invoice.model

import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.general.paginate.Links
import com.structure.base_mvvm.domain.general.paginate.Meta
import com.structure.base_mvvm.domain.general.paginate.Paginate

class InvoicePaginateData(
  @SerializedName("data")
  val list: ArrayList<InvoiceItem> = arrayListOf(), meta: Meta= Meta(), links: Links=Links()
) : Paginate(meta, links)