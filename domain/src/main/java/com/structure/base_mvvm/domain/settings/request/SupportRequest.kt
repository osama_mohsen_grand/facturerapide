package com.structure.base_mvvm.domain.settings.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.utils.Constants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


data class SupportRequest(
  @SerializedName("type")
  @Expose
  var type: String = "",
  @SerializedName("name")
  @Expose
  var name: String = "",
  @SerializedName("email")
  @Expose
  var email: String = "",
  @SerializedName("phone")
  @Expose
  var phone: String = "",
  @SerializedName("message")
  @Expose
  var message: String = "",
) {
    fun reset() {
      type = ""
      name = ""
      email = ""
      phone = ""
      message = ""
    }
}

class SupportValidationException(private val validationType: String) : Exception(validationType)