package com.structure.base_mvvm.domain.invoice.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class InvoiceRequest(
  @SerializedName("invoice_type_id")
  @Expose
  var invoiceId: Int = 0,
  @SerializedName("invoice_created_at")
  @Expose
  var date: String = "",
  @SerializedName("invoice_number")
  @Transient
  var number: String = "",
  @SerializedName("customer_name")
  @Expose
  var name: String = "",
  @SerializedName("customer_email")
  @Expose
  var email: String = "",
  @SerializedName("customer_address")
  @Expose
  var address: String = "",

  @SerializedName("customer_postal_code")
  @Expose
  var postalCode: String = "",

  @SerializedName("tax")
  @Expose
  var tax: String = "",
  @SerializedName("subtotal")
  @Expose
  var subTotal: String = "0",
  @SerializedName("total")
  @Expose
  var total: String = "0",
  @SerializedName("notes")
  @Expose
  var notes: String? = "",
  @SerializedName("notes2")
  @Expose
  var notes2: String = "",
  @SerializedName("services")
  @Expose
  var services: ArrayList<InvoiceServiceModel> = arrayListOf()
) : Serializable {

}

class InvoiceValidationException(private val varidationType: String) : Exception(varidationType)