package com.structure.base_mvvm.domain.account.use_case

//interactor
class AccountUseCases(
  val user: GetUserFromLocalUseCase,
  val saveUser: SaveUserToLocalUseCase,
  val sendFirebaseTokenUseCase: SendFirebaseTokenUseCase
)