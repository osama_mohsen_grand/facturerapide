package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.RegisterRequest
import com.structure.base_mvvm.domain.auth.entity.request.RegisterValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class RegisterUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val saveUserToLocalUseCase: SaveUserToLocalUseCase
) {

  private val TAG = "RegisterUseCase"
  @Throws(RegisterValidationException::class)
  fun submit(request: RegisterRequest): Flow<Resource<BaseResponse<*>>> = flow {
    println(request.toString())
    if (request.name.isEmpty()) {
      throw RegisterValidationException(AuthValidation.EMPTY_NAME.value.toString())
    }
    if (request.email.isEmpty()) {
      throw RegisterValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
    }

    if (!request.email.isValidEmail()) {
      throw RegisterValidationException(AuthValidation.INVALID_EMAIL.value.toString())
    }

    if (request.password.isEmpty() && !request.isLogin) {
      throw RegisterValidationException(AuthValidation.EMPTY_PASSWORD.value.toString())
    }

//    if (request.images.isEmpty() && !request.isLogin) {
//      throw RegisterValidationException(AuthValidation.IMAGE.value.toString())
//    }


    emit(Resource.Loading)
    val result = authRepository.register(request)
    emit(result)
  }.flowOn(Dispatchers.IO)


  fun profileUpdate(request: RegisterRequest): Flow<Resource<BaseResponse<UserModel>>> = flow {
    println(request.toString())
    if (request.name.isEmpty()) {
      throw RegisterValidationException(AuthValidation.EMPTY_NAME.value.toString())
    }
    if (request.email.isEmpty()) {
      throw RegisterValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
    }

    if (!request.email.isValidEmail()) {
      throw RegisterValidationException(AuthValidation.INVALID_EMAIL.value.toString())
    }


    emit(Resource.Loading)
    val result = authRepository.profileUpdate(request)


    if (result is Resource.Success) {
      emit(Resource.HideProgress)
      val user = (result.value.data as UserModel)
      saveUserToLocalUseCase(user)
    }

    emit(result)
  }.flowOn(Dispatchers.IO)
}