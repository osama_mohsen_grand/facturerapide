package com.structure.base_mvvm.domain.utils

import androidx.annotation.Keep

@Keep
data class BaseResponse<T>(
  val data: T?,
  val message: String,
  val code: Int?,
)