package com.structure.base_mvvm.domain.settings.models

import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.general.paginate.Links
import com.structure.base_mvvm.domain.general.paginate.Meta
import com.structure.base_mvvm.domain.general.paginate.Paginate

class NotificationPaginateData(
  @SerializedName("data")
  val list: ArrayList<NotificationData> = arrayListOf(), meta: Meta= Meta(), links: Links=Links()
) : Paginate(meta, links)