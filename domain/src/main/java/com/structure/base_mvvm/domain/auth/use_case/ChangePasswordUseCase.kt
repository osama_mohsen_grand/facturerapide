package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.auth.entity.request.*
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class ChangePasswordUseCase @Inject constructor(
  private val authRepository: AuthRepository,val checkLoggedInUserUseCase: CheckLoggedInUserUseCase
) {

  @Throws(ForgetPasswordValidationException::class)
  operator fun invoke(request: ChangePasswordRequest): Flow<Resource<BaseResponse<*>>> = flow {
    if (request.oldPassword.isEmpty() && checkLoggedInUserUseCase.invoke()) {
      throw ChangePasswordRequestException(AuthValidation.EMPTY_OLD_PASSWORD.value.toString())
    }
    if (request.newPassword.isEmpty()) {
      throw ChangePasswordRequestException(AuthValidation.EMPTY_NEW_PASSWORD.value.toString())
    }
    if (request.confirmPassword.isEmpty()) {
      throw ChangePasswordRequestException(AuthValidation.EMPTY_CONFIRM_PASSWORD.value.toString())
    }
    if (!request.confirmPassword.equals(request.newPassword)) {
      throw ChangePasswordRequestException(AuthValidation.BOTH_PASSWORDS_MUST_BE_SAME.value.toString())
    }

    emit(Resource.Loading)
    val result = if(checkLoggedInUserUseCase.invoke()) authRepository.changePassword(request)
    else {
      request.password = request.newPassword
      request.passwordConfirmation = request.confirmPassword
      authRepository.changePasswordInForget(request)
    }
    emit(result)
  }.flowOn(Dispatchers.IO)
}