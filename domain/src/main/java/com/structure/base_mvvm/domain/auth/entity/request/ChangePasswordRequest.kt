package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ChangePasswordRequest(
  @SerializedName("old_password")
  @Expose
  var oldPassword: String = "",
  @SerializedName("new_password")
  @Expose
  var newPassword: String = "",
  @SerializedName("new_password_confirmation")
  @Expose
  var confirmPassword: String = "",
  @SerializedName("password")
  @Expose
  var password: String = "",
  @SerializedName("password_confirmation")
  @Expose
  var passwordConfirmation: String = "",
) {
}

class ChangePasswordRequestException(private val validationType: String) :
  Exception(validationType)