package com.structure.base_mvvm.domain.settings.use_case

import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.settings.repository.SettingsRepository
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import com.structure.base_mvvm.domain.settings.request.SupportValidationException
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class SettingsUseCase @Inject constructor(
  private val repository: SettingsRepository
) {

  fun welcome(): Flow<Resource<BaseResponse<IntroModel>>> =
    flow {
      emit(Resource.Loading)
      val result = repository.welcome()

      emit(result)
    }.flowOn(Dispatchers.IO)

  fun info(type: String): Flow<Resource<BaseResponse<IntroModel>>> =
    flow {
      println("info api will call")
      emit(Resource.Loading)
      val result = repository.info(type)

      emit(result)
    }.flowOn(Dispatchers.IO)


  fun notifications(page:Int): Flow<Resource<BaseResponse<NotificationPaginateData>>> = flow {
    println("get Invoices UseCase")
    if(page == 1 ) emit(Resource.Loading)
    val result = repository.notifications(page)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun deleteNotification(id: Int): Flow<Resource<BaseResponse<*>>> = flow {
    println("get Invoices UseCase")
    emit(Resource.Loading)
    val result = repository.deleteNotification(id)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun support(request: SupportRequest): Flow<Resource<BaseResponse<*>>> =
    flow {
      println(request.toString())
      if (request.name.isEmpty()) {
        throw SupportValidationException(AuthValidation.EMPTY_NAME.value.toString())
      }
      if (request.email.isEmpty()) {
        throw SupportValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
      }

      if (!request.email.isValidEmail()) {
        throw SupportValidationException(AuthValidation.INVALID_EMAIL.value.toString())
      }

      if (request.phone.isEmpty()) {
        throw SupportValidationException(AuthValidation.EMPTY_PHONE.value.toString())
      }

      if (request.message.isEmpty()) {
        throw SupportValidationException(AuthValidation.EMPTY_MESSAGE.value.toString())
      }


      emit(Resource.Loading)
      println("support repository.support")
      val result = repository.support(request)

      emit(result)
    }.flowOn(Dispatchers.IO)

}