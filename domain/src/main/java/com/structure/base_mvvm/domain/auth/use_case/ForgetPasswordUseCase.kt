package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.auth.entity.request.ForgetPasswordRequest
import com.structure.base_mvvm.domain.auth.entity.request.ForgetPasswordValidationException
import com.structure.base_mvvm.domain.auth.entity.request.LogInValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class ForgetPasswordUseCase @Inject constructor(
  private val authRepository: AuthRepository) {

  @Throws(ForgetPasswordValidationException::class)
  operator fun invoke(request: ForgetPasswordRequest): Flow<Resource<BaseResponse<*>>> = flow {
    if (request.email.isEmpty()) {
      throw ForgetPasswordValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
    }
    if (!request.email.isValidEmail()) {
      throw ForgetPasswordValidationException(AuthValidation.INVALID_EMAIL.value.toString())
    }

    emit(Resource.Loading)
    val result = authRepository.forgetPassword(request)
    emit(result)
  }.flowOn(Dispatchers.IO)
}