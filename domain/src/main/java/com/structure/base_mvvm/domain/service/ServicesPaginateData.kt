package com.structure.base_mvvm.domain.service

import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.general.paginate.Links
import com.structure.base_mvvm.domain.general.paginate.Meta
import com.structure.base_mvvm.domain.general.paginate.Paginate
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel

class ServicesPaginateData(
  @SerializedName("data")
  val list: ArrayList<InvoiceServiceModel> = arrayListOf(), meta: Meta= Meta(), links: Links=Links()
) : Paginate(meta, links)