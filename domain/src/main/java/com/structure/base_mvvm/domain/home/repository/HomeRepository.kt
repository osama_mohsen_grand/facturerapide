package com.structure.base_mvvm.domain.home.repository
import com.structure.base_mvvm.domain.home.models.HomeModel
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource

interface HomeRepository {
  suspend fun notifications(page:Int): Resource<BaseResponse<NotificationPaginateData>>
  suspend fun home(device_id : String , device_token : String): Resource<BaseResponse<HomeModel>>
}