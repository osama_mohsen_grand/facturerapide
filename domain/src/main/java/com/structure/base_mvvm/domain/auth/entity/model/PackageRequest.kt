package com.structure.base_mvvm.domain.auth.entity.model


import com.google.gson.annotations.SerializedName

data class PackageRequest(
    @SerializedName("package_id")
    var packageId: Int = 0
)