package com.structure.base_mvvm.domain.invoice.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class InvoiceServiceModel(
  @Transient
  var id: Int = 0,
  @SerializedName("name")
  @Expose
  var name: String = "",
  @SerializedName("description")
  @Expose
  var description: String = "",
  @SerializedName("quantity")
  @Expose
  var quantity: String = "",
  @SerializedName("unit")
  @Expose
  var unit: String = "",
  @SerializedName("price")
  @Expose
  var price: String = "",
) : Serializable{
  fun clear(){
    id = 0
    this.name = ""
    this.description = ""
    this.quantity = ""
    this.unit = ""
    this.price = ""
  }
}