package com.structure.base_mvvm.domain.invoice.usecase

import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.LogInValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.customer.CustomersPaginateData
import com.structure.base_mvvm.domain.invoice.model.InvoiceDetailsResponse
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.repository.InvoiceRepository
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.invoice.request.InvoiceValidationException
import com.structure.base_mvvm.domain.service.ServicesPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class InvoiceUseCase @Inject constructor(
  private val repository: InvoiceRepository
) {

  @Throws(InvoiceValidationException::class)
  fun submit(request: InvoiceRequest): Flow<Resource<BaseResponse<*>>> = flow {
//    if (request.time.isEmpty()) {
//      throw InvoiceValidationException(AuthValidation.EMPTY_TIME.value.toString())
//    }

//    if (request.tax.isEmpty()) {
//      throw InvoiceValidationException(AuthValidation.EMPTY_TAX.value.toString())
//    }

//    if (request.notes.isEmpty()) {
//      throw InvoiceValidationException(AuthValidation.EMPTY_NOTES.value.toString())
//    }

    if (request.name == "") {
      throw InvoiceValidationException(AuthValidation.EMPTY_CUSTOMER.value.toString())
    }

    if (request.services.size == 0) {
      throw InvoiceValidationException(AuthValidation.EMPTY_SERVICES.value.toString())
    }

    emit(Resource.Loading)
    request.tax = if (request.tax.isEmpty()) "0" else request.tax
    val result = repository.createInvoice(request)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun getInvoices(
    page: Int,
    type: Int,
    search: String
  ): Flow<Resource<BaseResponse<InvoicePaginateData>>> = flow {
    println("get Invoices UseCase")
    if (page == 1) emit(Resource.Loading)
    val result = repository.getInvoices(page, type, search)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun getInvoiceDetails(id: Int): Flow<Resource<BaseResponse<InvoiceDetailsResponse>>> = flow {
    println("get Invoices UseCase")
    emit(Resource.Loading)
    val result = repository.getInvoiceDetails(id)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun getCustomers(page: Int): Flow<Resource<BaseResponse<CustomersPaginateData>>> = flow {
    println("get Invoices UseCase")
    emit(Resource.Loading)
    val result = repository.getCustomers(page)
    emit(result)

  }.flowOn(Dispatchers.IO)


  fun getServices(page: Int): Flow<Resource<BaseResponse<ServicesPaginateData>>> = flow {
    println("get Invoices UseCase")
    emit(Resource.Loading)
    val result = repository.getServices(page)
    emit(result)

  }.flowOn(Dispatchers.IO)




  fun update(id: Int, request: InvoiceRequest): Flow<Resource<BaseResponse<*>>> = flow {
    if (request.name == "") {
      throw InvoiceValidationException(AuthValidation.EMPTY_CUSTOMER.value.toString())
    }

    if (request.services.size == 0) {
      throw InvoiceValidationException(AuthValidation.EMPTY_SERVICES.value.toString())
    }
    request.tax = if (request.tax.isEmpty()) "0" else request.tax

    emit(Resource.Loading)
    val result = repository.update(id, request)
    emit(result)

  }.flowOn(Dispatchers.IO)

  fun deleteInvoice(id: Int): Flow<Resource<BaseResponse<*>>> = flow {
    println("get Invoices UseCase")
    emit(Resource.Loading)
    val result = repository.deleteInvoice(id)
    emit(result)

  }.flowOn(Dispatchers.IO)

}