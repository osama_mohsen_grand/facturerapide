package com.structure.base_mvvm.domain.auth.entity.model


import com.google.gson.annotations.SerializedName

data class PackageGooglePayRequest(
    @SerializedName("package_name")
    var packageName: String = "",
    @SerializedName("package_id")
    var packageId: String = "",
    @SerializedName("duration")
    var duration: Int = 1,
    @SerializedName("paid")
    var paid: String = "",
    @SerializedName("duration_type")
    var durationType: String = "",
)