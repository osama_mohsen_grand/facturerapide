package com.structure.base_mvvm.domain.utils

object Constants {

  val DATE_TIME: String = "date_time"
    const val LIST_PAGE_SIZE = 1
  const val REGISTER = "register"
  const val FORGET_PASSWORD = "forget_password"
  const val PROFILE = "profile"
  const val LOGIN = "login"
  const val LOGOUT = "logout"
  const val VERIFY = "verify"
  const val RESET = "reset"

  const val PACKAGES = "packages"
  const val HOME = "home"
  const val WELCOME = "welcome"
  const val AUTH = "auth"
  const val COMPANY_REGISTER = "company_register"
  const val BUNDLE = "bundle"
  const val CONTACT_US = "contact_us"
  const val COMPLAINT = "complaint"
  const val SHARE = "share"
  const val RATE_APP = "rate_app"
  const val SUBMIT = "submit"
  const val CREATE_INVOICE = "create_invoice"
  const val ADD_CUSTOMER = "add_customer"
  const val ADD_SERVICE = "add_service"
  const val DISMISS = "dismiss"
  const val SAVE = "save"
  const val PRINT = "print"
  const val PREVIEW = "preview"
  const val COPY = "copy"

  const val CHANGE_PASSWORD = "change_password"
  const val INVOICES = "invoices"
  const val NOTIFICATION = "notifications"
  const val EDIT = "edit"
  const val DELETE = "delete"
  const val PAGE = "page"
  const val TOKEN = "token"
  const val RECALL = "recall"
  const val LANGUAGE_DATA = "APP-NAME-Cache"
  const val LANGUAGE = "language"
  const val DEFAULT_LANGUAGE = "en"
  const val LANGUAGE_ENGLISH = "en"
  const val LANGUAGE_SPANISH = "es"
  const val LANGUAGE_FRENCH = "fr"
  const val COLOR = "color"









    const val PRIVACY = "privacy_policy"
  const val ABOUT_US = "about_us"
  const val TERMS_AND_CONDITION = "terms_conditions"


  const val IMAGE = "image"

  const val PICKER_IMAGE = "picker_image"

  const val PERMISSION_GALLERY = 1000

  const val SUCCESS = "success"


}