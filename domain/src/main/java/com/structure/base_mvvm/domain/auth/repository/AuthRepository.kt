package com.structure.base_mvvm.domain.auth.repository

import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.*
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource

interface AuthRepository {

  suspend fun logIn(request: LogInRequest): Resource<BaseResponse<UserModel>>
  suspend fun register(request: RegisterRequest): Resource<BaseResponse<*>>
  suspend fun profileUpdate(request: RegisterRequest): Resource<BaseResponse<UserModel>>
  suspend fun registerCompany(request: RegisterCompanyRequest): Resource<BaseResponse<UserModel>>
  suspend fun confirmCode(request: ConfirmCodeRequest): Resource<BaseResponse<UserModel>>
  suspend fun sendCode(request: ConfirmCodeRequest): Resource<BaseResponse<UserModel>>
  suspend fun updateProfile(request: ForgetPasswordRequest): Resource<BaseResponse<UserModel>>
  suspend fun changePassword(request: ChangePasswordRequest): Resource<BaseResponse<*>>
  suspend fun changePasswordInForget(request: ChangePasswordRequest): Resource<BaseResponse<*>>
  suspend fun forgetPassword(request: ForgetPasswordRequest): Resource<BaseResponse<*>>
  suspend fun packages(): Resource<BaseResponse<List<Package>>>
  suspend fun subscribe(request: PackageGooglePayRequest): Resource<BaseResponse<*>>

}