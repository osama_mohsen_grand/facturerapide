package com.structure.base_mvvm.domain.auth.entity.model

import com.google.gson.annotations.SerializedName

data class UserModel (
  @SerializedName("name")
  var name: String = "",
  @SerializedName("email")
  var email: String = "",
  @SerializedName("phone")
  var phone: String = "",
  @SerializedName("image")
  var image: String = "",
  @SerializedName("token")
  val token: String = "",
  @SerializedName("register_steps")
  val register_steps: Int = 0,
  @SerializedName("company")
  var company: Company = Company()
)