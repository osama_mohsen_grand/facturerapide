package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.utils.BaseRequest
import com.structure.base_mvvm.domain.utils.Constants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


data class RegisterRequest(
  @SerializedName("name")
  @Expose
  var name: String = "",
  @SerializedName("email")
  @Expose
  var email: String = "",
  @SerializedName("password")
  @Expose
  var password: String = "",
  @SerializedName("register_steps")
  @Expose
  val register_steps: Int = 1,
  @SerializedName("device_token")
  @Expose
  var device_token: String = "",
//  var image:MultipartBody.Part? = null,
//  @Transient
//  var imagePath:String =""
) : BaseRequest(){
  var isLogin: Boolean = false
}

class RegisterValidationException(private val validationType: String) : Exception(validationType)