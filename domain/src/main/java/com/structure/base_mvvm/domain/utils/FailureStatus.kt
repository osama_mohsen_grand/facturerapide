package com.structure.base_mvvm.domain.utils

enum class FailureStatus {
  EMPTY,
  VERIFY_CODE,
  API_FAIL,
  NO_INTERNET,
  OTHER,
  TOKEN_EXPIRED
}