package com.structure.base_mvvm.domain.home.models


import com.google.gson.annotations.SerializedName

data class HomeModel(
    @SerializedName("is_subscribed")
    var isSubscribed: Int = 0,
    @SerializedName("notification_count")
    var notificationCount: Int = 0
)