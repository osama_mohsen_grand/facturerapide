package com.structure.base_mvvm.domain.packages.entity


import com.google.gson.annotations.SerializedName

data class Package(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("title")
    var title: String = "",
    @SerializedName("is_free")
    var isFree: Int = 0, // control on image
    @SerializedName("period")
    var period: String = "0",
    @SerializedName("period_type")
    var periodType: String = "",
    @SerializedName("price")
    var price: Double = 0.0,
    @SerializedName("price_before")
    var priceBefore: Double = 0.0,
    @SerializedName("description")
    var description: String = "",
    @SerializedName("is_subscribed")
    var isSubscribed: Boolean = false,
    @SerializedName("remaining_time")
    var remainingTime: String = "",
    var productDetails: Int = 0,
    var durationType: String = "",
    var packageId: String = ""
){
}