package com.structure.base_mvvm.domain.home.models


data class CategoryModel(
    val id : Int = 1,
    val title: String = "",
    var desc: String= "",
    var drawable: Int
)