package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
import com.structure.base_mvvm.domain.auth.entity.request.LogInValidationException
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class PackagesUseCase @Inject constructor(
  private val authRepository: AuthRepository,
) {
  @Throws(LogInValidationException::class)
  fun getPackages(): Flow<Resource<BaseResponse<List<Package>>>> = flow {
    println("calling service")
    emit(Resource.Loading)
    val result = authRepository.packages()
    emit(result)
  }.flowOn(Dispatchers.IO)

  fun setPackage(request: PackageGooglePayRequest): Flow<Resource<BaseResponse<*>>> = flow {
    println("calling service")
    emit(Resource.Loading)
    val result = authRepository.subscribe(request)
    emit(result)
  }.flowOn(Dispatchers.IO)
}