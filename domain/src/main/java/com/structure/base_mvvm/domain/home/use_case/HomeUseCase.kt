package com.structure.base_mvvm.domain.home.use_case

import com.structure.base_mvvm.domain.home.models.HomeModel
import com.structure.base_mvvm.domain.home.repository.HomeRepository
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class HomeUseCase @Inject constructor(
  private val homeRepository: HomeRepository
) {
  fun home(deviceId : String, deviceToken : String): Flow<Resource<BaseResponse<HomeModel>>> =
    flow {
      emit(Resource.Loading)
      val result = homeRepository.home(deviceId,deviceToken)
      emit(result)
    }.flowOn(Dispatchers.IO)

  fun getNotifications(page: Int, showProgress: Boolean): Flow<Resource<BaseResponse<NotificationPaginateData>>> =
    flow {
      emit(Resource.Loading)
      val result = homeRepository.notifications(page)
      emit(result)
    }.flowOn(Dispatchers.IO)
}