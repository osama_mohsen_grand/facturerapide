package com.structure.base_mvvm.domain.settings.repository
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource

interface SettingsRepository {
  suspend fun welcome(): Resource<BaseResponse<IntroModel>>
  suspend fun info(type: String): Resource<BaseResponse<IntroModel>>
  suspend fun support(request: SupportRequest): Resource<BaseResponse<*>>
  suspend fun notifications(page: Int): Resource<BaseResponse<NotificationPaginateData>>
  suspend fun deleteNotification(id: Int): Resource<BaseResponse<*>>
}