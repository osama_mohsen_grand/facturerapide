package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.LogInRequest
import com.structure.base_mvvm.domain.auth.entity.request.LogInValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class LogInUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val saveUserToLocalUseCase: SaveUserToLocalUseCase
) {

  @Throws(LogInValidationException::class)
  fun login(request: LogInRequest): Flow<Resource<BaseResponse<UserModel>>> = flow {
    if (request.email.isEmpty()) {

      throw LogInValidationException(AuthValidation.EMPTY_EMAIL.value.toString())
    }

    if (!request.email.isValidEmail()) {
      throw LogInValidationException(AuthValidation.INVALID_EMAIL.value.toString())
    }

    if (request.password.isEmpty()) {
      throw LogInValidationException(AuthValidation.EMPTY_PASSWORD.value.toString())
    }



    emit(Resource.Loading)
    val result = authRepository.logIn(request)
    var data = result
    if (result is Resource.Success) {
      emit(Resource.HideProgress)
      val user = (result.value.data as UserModel)
      when {
        user.register_steps >= 2 -> saveUserToLocalUseCase(user)
        else -> {
          saveUserToLocalUseCase(user.token, user.email)
        }
      }
      data = when (result.value.data.register_steps) {
        1 -> Resource.CompanyRegister
        else -> result
//        }
      }
    }
    emit(data)
  }.flowOn(Dispatchers.IO)
}