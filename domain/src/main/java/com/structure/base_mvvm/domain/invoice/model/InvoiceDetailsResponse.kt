package com.structure.base_mvvm.domain.invoice.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import com.structure.base_mvvm.domain.auth.entity.model.Company
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel

@Keep
data class InvoiceDetailsResponse(
  @SerializedName("company")
  @Expose val company: Company = Company(),
  @SerializedName("customer_address")
  @Expose val customerAddress: String? = "",
  @SerializedName("customer_email")
  @Expose val customerEmail: String? = "",
  @SerializedName("customer_name")
  @Expose val customerName: String? = "",
  @SerializedName("customer_postal_code")
  @Expose val customerPostalCode: String? = "",
  @SerializedName("id")
  @Expose val id: Int = 0,
  @SerializedName("invoice_number")
  @Expose val invoiceNumber: String? = "",
  @SerializedName("invoice_type_id")
  @Expose val invoiceTypeId: Int = 0,
  @SerializedName("invoice_created_at")
  @Expose val invoiceCreatedAt: String = "",
  @SerializedName("pdf")
  @Expose val pdf: String = "",
  @SerializedName("image_invoice")
  @Expose val imageInvoice: String = "",
  @SerializedName("notes")
  @Expose val notes: String = "",
  @SerializedName("notes2")
  @Expose val notes2: String? = "",
  @SerializedName("services")
  @Expose val services: List<InvoiceServiceModel> = listOf(),
  @SerializedName("subtotal")
  @Expose val subtotal: Double = 0.0,
  @SerializedName("tax")
  @Expose val tax: Int = 0,
  @SerializedName("total")
  @Expose val total: Double = 0.0
) {
}