package com.structure.base_mvvm.domain.language


import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("title")
    var title: String = "",
    @SerializedName("abbreviation")
    var abbreviation: String = "",
){
}