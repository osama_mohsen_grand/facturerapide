package com.structure.base_mvvm.domain.auth.use_case

import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.ConfirmCodeRequest
import com.structure.base_mvvm.domain.auth.entity.request.ConfirmCodeValidationException
import com.structure.base_mvvm.domain.auth.entity.request.LogInValidationException
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.auth.repository.AuthRepository
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class ConfirmCodeUseCase @Inject constructor(
  private val authRepository: AuthRepository,
  private val saveUserToLocalUseCase: SaveUserToLocalUseCase
) {

  @Throws(LogInValidationException::class)
  fun submit(request: ConfirmCodeRequest): Flow<Resource<BaseResponse<UserModel>>> = flow {

    if (request.code.isEmpty()) {
      throw ConfirmCodeValidationException(AuthValidation.EMPTY_CODE.value.toString())
    }

    emit(Resource.Loading)
    val result = authRepository.confirmCode(request)
    if (result is Resource.Success) {
      result.value.data?.let {
        when (request.from) {
          Constants.FORGET_PASSWORD,Constants.LOGIN -> {
            saveUserToLocalUseCase(it.token, "")
          }
          Constants.REGISTER -> saveUserToLocalUseCase(it)
        }
      }
    }
    emit(result)
  }.flowOn(Dispatchers.IO)
}