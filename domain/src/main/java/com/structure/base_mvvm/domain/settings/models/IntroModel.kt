package com.structure.base_mvvm.domain.settings.models

data class IntroModel(
    var content: String= "",
    var image: String= "",
    var title: String = ""
){
}