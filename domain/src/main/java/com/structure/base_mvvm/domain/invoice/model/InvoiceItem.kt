package com.structure.base_mvvm.domain.invoice.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.google.gson.annotations.Expose
import java.io.Serializable

@Keep
data class InvoiceItem(
    @SerializedName("customer_email")
    @Expose var customerEmail: String = "",
    @SerializedName("customer_name")
    @Expose var customerName: String = "",
    @SerializedName("id")
    @Expose var id: Int = 0,
    @SerializedName("invoice_number")
    @Expose var invoiceNumber: String = "",
    @SerializedName("items")
    @Expose var items: String = "",
    @SerializedName("maturity_time")
    @Expose var maturityTime: String = "",
    @SerializedName("total")
    @Expose var total: String = "",
    @SerializedName("pdf")
    @Expose var pdf: String? = null
):Serializable{

}