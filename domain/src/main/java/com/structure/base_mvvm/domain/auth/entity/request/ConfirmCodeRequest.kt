package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.flow.StateFlow

data class ConfirmCodeRequest(
  @SerializedName("code")
  @Expose
  var code: String = "",
  @SerializedName("email")
  @Expose
  var email: String = "",
  @SerializedName("type")
  @Expose
  var type: String = "",
  @Transient
  var from: String = ""
)

class ConfirmCodeValidationException(private val validationType: String) : Exception(validationType)