package com.structure.base_mvvm.domain.customer


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class CustomerModel(
  @SerializedName("id")
  val id: Int = -1,
    @SerializedName("address")
    val address: String? = "",
//    @SerializedName("company")
//    val company: Any = Any(),
//    @SerializedName("image")
//    val image: Any = Any(),
    @SerializedName("name")
    val name: String? = "",
//    @SerializedName("phone")
//    val phone: Any = Any(),
    @SerializedName("postal_code")
    val postalCode: String? = "",
//    @SerializedName("register_steps")
//    val registerSteps: Any = Any(),
//    @SerializedName("token")
//    val token: Any = Any()
)