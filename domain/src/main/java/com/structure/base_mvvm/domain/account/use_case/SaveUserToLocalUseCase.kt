package com.structure.base_mvvm.domain.account.use_case

import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import javax.inject.Inject

class SaveUserToLocalUseCase @Inject constructor(public val accountRepository: AccountRepository) {
  operator fun invoke(userModel: UserModel) = accountRepository.saveUserToLocal(userModel)
  operator fun invoke() = accountRepository.isLoggedIn()
  operator fun invoke(token: String,email: String) =
    accountRepository.saveUserToken(token,email)
  operator fun invoke(firebaseToken: String) =
    accountRepository.saveFirebaseTokenToLocal(firebaseToken)

}