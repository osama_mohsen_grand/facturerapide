package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.structure.base_mvvm.domain.utils.BaseRequest
import com.structure.base_mvvm.domain.utils.Constants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

data class RegisterCompanyRequest(
  @SerializedName("company_name")
  @Expose
  var name: String = "",
  @SerializedName("company_phone")
  @Expose
  var phone: String = "",
  @SerializedName("company_email")
  @Expose
  var email: String = "",
  @SerializedName("company_commercial_register")
  @Expose
  var commercialNumber: String = "",
  @SerializedName("company_address")
  @Expose
  var address: String = "",
  @SerializedName("company_address_2")
  @Expose
  var address2: String = "",
  @SerializedName("company_street")
  @Expose
  var street: String = "",
  @SerializedName("vat_number")
  @Expose
  var companyVatNumber: String = "",
  @SerializedName("image_size")
  @Expose
  var image_size: String = "s",
  @SerializedName("image")
  @Expose
  var image: String = "",
  @SerializedName("company_website")
  @Expose
  var company_website: String = "",

  @SerializedName("invoice_color")
  @Expose
  var invoiceColor: String = "",
  @SerializedName("register_steps")
  @Expose
  var registerSteps: Int = 2,
) : BaseRequest() {
  var isLogin: Boolean = false
}
