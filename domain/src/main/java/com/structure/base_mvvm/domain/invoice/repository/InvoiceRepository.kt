package com.structure.base_mvvm.domain.invoice.repository

import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.*
import com.structure.base_mvvm.domain.customer.CustomersPaginateData
import com.structure.base_mvvm.domain.invoice.model.InvoiceDetailsResponse
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.service.ServicesPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource

interface InvoiceRepository {

  suspend fun createInvoice(request: InvoiceRequest): Resource<BaseResponse<*>>
  suspend fun getInvoices(page:Int, id: Int,search : String): Resource<BaseResponse<InvoicePaginateData>>
  suspend fun getInvoiceDetails(id: Int): Resource<BaseResponse<InvoiceDetailsResponse>>
  suspend fun getCustomers(page: Int): Resource<BaseResponse<CustomersPaginateData>>
  suspend fun getServices(page: Int): Resource<BaseResponse<ServicesPaginateData>>

  suspend fun update(id: Int,request: InvoiceRequest): Resource<BaseResponse<*>>
  suspend fun deleteInvoice(id: Int): Resource<BaseResponse<*>>

}