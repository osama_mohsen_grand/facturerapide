package com.structure.base_mvvm.domain.auth.entity.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ForgetPasswordRequest(
  @SerializedName("email")
  @Expose
  var email: String = "",
  @SerializedName("type")
  @Expose
  var type: String = "verify",
) {
}

class ForgetPasswordValidationException(private val validationType: String) :
  Exception(validationType)