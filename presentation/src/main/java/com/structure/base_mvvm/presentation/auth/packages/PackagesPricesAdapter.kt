package com.structure.base_mvvm.presentation.auth.packages

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.databinding.ItemPackageBinding
import com.structure.base_mvvm.presentation.databinding.ItemPackagePriceBinding

class PackagesPricesAdapter: RecyclerView.Adapter<PackagesPricesAdapter.ViewHolder>() {

  lateinit var iPrintSubmit: IPriceSubmit
  var selected = -1
  val list = ArrayList<Package>()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_package_price, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = list[position]
    Log.d(TAG, "onBindViewHolder: ${data.title}")
    val itemViewModel = ItemPackageViewModel(data,position == selected,position)
    if(this::iPrintSubmit.isInitialized)
      holder.itemLayoutBinding.iPrintSubmit = iPrintSubmit
    holder.setViewModel(itemViewModel)
  }

  private  val TAG = "PackagesPricesAdapter"
  fun insert(list: ArrayList<Package>){
    this.list.clear()
    this.list.addAll(list)
    Log.d(TAG, "insert: ${this.list.size}")
    notifyDataSetChanged()
  }

  override fun getItemCount(): Int {
    return list.size
  }


  fun updateSelected(model: Package) {
    list.forEachIndexed { index, p ->
      if(p.id == model.id)
        selected = index
    }
    notifyDataSetChanged()
  }

  fun setPosition(position: Int) {
    this.selected = position
    notifyDataSetChanged()
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemPackagePriceBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemPackageViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }

  /**
   * This method should update the contents of the {@link VH#itemView} to reflect the item at the
   * given position.
   *
   * @param holder The ViewHolder which should be updated to represent the contents of the
   *        item at the given position in the data set.
   * @param position The position of the item within the adapter's data set.
   */



}