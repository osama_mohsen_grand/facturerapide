package com.structure.base_mvvm.presentation.account

import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import codes.pretty_pop_up.PrettyPopUpHelper
import com.structure.base_mvvm.domain.utils.Constants

import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.AccountFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class AccountFragment : BaseFragment<AccountFragmentBinding>() {

  private val viewModel: AccountViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.account_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  override
  fun setUpViews() {
    setUpToolBar()
  }

  private fun setUpToolBar() {
//    binding.includedToolbar.toolbarTitle.text = getMyString(R.string.account)
//    binding.includedToolbar.backIv.hide()
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      when (it) {
        Constants.LOGOUT -> openActivityAndClearStack(AuthActivity::class.java)
        Constants.PROFILE -> navigateSafe(AccountFragmentDirections.actionAccountFragmentToProfile())
        Constants.COMPANY_REGISTER -> navigateSafe(AccountFragmentDirections.actionAccountFragmentToProfileCompanyFragment())
        Constants.PACKAGES -> navigateSafe(AccountFragmentDirections.actionAccountFragmentToUpdatePackagesFragment())
      }
      if (it == Constants.LOGOUT) {
      }
//      showLogOutPopUp()
    }
  }

//  private fun showLogOutPopUp() {
//    PrettyPopUpHelper.Builder(childFragmentManager)
//      .setStyle(PrettyPopUpHelper.Style.STYLE_1_HORIZONTAL_BUTTONS)
//      .setTitle(R.string.log_out)
//      .setTitleColor(getMyColor(R.color.colorPrimaryDark))
//      .setContent(R.string.log_out_hint)
//      .setContentColor(getMyColor(R.color.colorDarkGray))
//      .setPositiveButtonBackground(R.drawable.btn_action_positive)
//      .setPositiveButtonTextColor(getMyColor(R.color.colorWhite))
//      .setNegativeButtonBackground(R.drawable.btn_action_negative)
//      .setImage(R.drawable.alerter_ic_notifications)
//      .setPositiveButton(R.string.log_out) {
//        it.dismiss()
//        logOut()
//      }
////      .setNegativeButtonBackground(R.drawable.btn_gray)
//      .setNegativeButtonTextColor(getMyColor(R.color.colorDarkGray))
//      .setNegativeButton(getMyString(R.string.cancel), null)
//      .create()
//  }

  private fun logOut() {
//    viewModel.logOut()
//
//    lifecycleScope.launchWhenResumed {
//      viewModel.logOutResponse.collect {
//        when (it) {
//          Resource.Loading -> {
//            hideKeyboard()
//            showLoading()
//          }
//          is Resource.Success -> {
//            hideLoading()
//            viewModel.clearUser()
//            openLogInScreen()
//          }
//          is Resource.Failure -> {
//            hideLoading()
//            handleApiError(it)
//          }
//          else -> {}
//        }
//      }
//    }
    viewModel.clearUser()
    openLogInScreen()
  }

  private fun openLogInScreen() {
    requireActivity().openActivityAndClearStack(AuthActivity::class.java)
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}