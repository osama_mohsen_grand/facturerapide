package com.structure.base_mvvm.presentation.invoice.customer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Constants.ADD_CUSTOMER
import com.structure.base_mvvm.domain.utils.Constants.DISMISS
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.backToPreviousScreen
import com.structure.base_mvvm.presentation.base.extensions.showMessageSuccess
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.SwipeToDeleteCallback
import com.structure.base_mvvm.presentation.databinding.CustomerFragmentBinding
import com.structure.base_mvvm.presentation.databinding.CustomersFragmentBinding
import com.structure.base_mvvm.presentation.databinding.NotificationFragmentBinding
import com.structure.base_mvvm.presentation.notification.viewmodel.NotificationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class CustomersFragment : BaseFragment<CustomersFragmentBinding>() {

  val viewModel: CustomersViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.customers_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }


  override
  fun setUpViews() {

  }

  override fun setupObservers() {
    setRecyclerViewScrollListener()
    lifecycleScope.launchWhenResumed {
      viewModel.responseCustomers.collect {
        println("collection here $it")
        handleLoading(it)
        if(it is Resource.Success){
          viewModel.setData(it.value.data)
        }
      }
    }
    viewModel.clickEvent.observe(this,{
      when(it){
        DISMISS -> {
          if(this::dialog.isInitialized){
            if(dialog.isShowing) dialog.dismiss()
          }
          val n = findNavController()
          n.navigateUp()
          n.currentBackStackEntry?.savedStateHandle?.set(
            ADD_CUSTOMER,
            viewModel.request
          )
        }
        ADD_CUSTOMER -> {
          openCustomers()
        }
      }
    })
  }


  private fun setRecyclerViewScrollListener() {

    val layoutManager = LinearLayoutManager(requireContext())
    binding.recyclerView.layoutManager = layoutManager

    binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(1)){
          viewModel.callService()
        }
      }
    })
  }


  private lateinit var dialog: BottomSheetDialog

  fun openCustomers(){
    dialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogStyle)

    val binding: CustomerFragmentBinding = DataBindingUtil.inflate(
      LayoutInflater.from(requireContext()),
      com.structure.base_mvvm.presentation.R.layout.customer_fragment,
      null,
      false
    )
    binding.viewModel = viewModel
    dialog.setContentView(binding.root)

    dialog.show()
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}