package com.structure.base_mvvm.presentation.invoice.service

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import com.structure.base_mvvm.domain.customer.CustomerModel
import com.structure.base_mvvm.domain.general.model.Mutable
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ItemServiceBinding

class ServicesAdapter: CardSliderAdapter<ServicesAdapter.ViewHolder>() {
  var click: SingleLiveEvent<Mutable> = SingleLiveEvent()
  var position = -1
  var showDelete = false
  lateinit var serviceViewModel: ServicesViewModel
  override fun bindVH(holder: ViewHolder, position: Int) {
    Log.d(TAG, "bindVH: "+differ.currentList.size)
    val data = differ.currentList[position]
    val itemViewModel = ItemServiceViewModel(data,position,showDelete,serviceViewModel)
    holder.setViewModel(itemViewModel)
  }

  private val differCallback = object : DiffUtil.ItemCallback<InvoiceServiceModel>() {
    override fun areItemsTheSame(oldItem: InvoiceServiceModel, newItem: InvoiceServiceModel): Boolean {
      Log.d(TAG, "areItemsTheSame: "+newItem.id)
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: InvoiceServiceModel, newItem: InvoiceServiceModel): Boolean {
      Log.d(TAG, "areContentsTheSame: "+oldItem.id)
      return oldItem == newItem
    }
  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_service, parent, false)
    Log.d(TAG, "areContentsTheSame: onCreateViewHolder")
    return ViewHolder(view)
  }

  private  val TAG = "ServicesAdapter"

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    Log.d(TAG, "onBindViewHolder: "+data.name+","+data.description)
    val itemViewModel = ItemServiceViewModel(data,position,showDelete,serviceViewModel)
    holder.setViewModel(itemViewModel)
    holder.itemLayoutBinding.delete.setOnClickListener {
      click.value = Mutable(Constants.DELETE,position)
    }
    holder.itemLayoutBinding.tvItemService.setOnClickListener {
      this.position = position
      click.value = Mutable(Constants.EDIT,position)
    }
  }


  fun insertData(insertList: List<InvoiceServiceModel>) {
    val array = ArrayList<InvoiceServiceModel>(differ.currentList)
    val size = array.size
    array.addAll(insertList)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }

  fun insertData(item:InvoiceServiceModel) {
    val array = ArrayList<InvoiceServiceModel>(differ.currentList)
    val size = array.size
    array.add(item)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(null)
    differ.submitList(array)
    notifyDataSetChanged()
  }



  override fun getItemId(position: Int): Long {
    return super.getItemId(position)
  }

  override fun getItemCount(): Int {
    Log.e(TAG, "getItemCount: "+differ.currentList.size )
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

    fun update(invoiceServiceModel: InvoiceServiceModel, position: Int) {
      Log.d(TAG, "update: ${invoiceServiceModel.name}")
      Log.d(TAG, "update: ${invoiceServiceModel.name}")
      if(position>=0 && position < differ.currentList.size){
        val array = ArrayList<InvoiceServiceModel>(differ.currentList)
        array[position] = invoiceServiceModel
        differ.submitList(null)
        differ.submitList(array)
        notifyDataSetChanged()
      }
    }

    inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemServiceBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemServiceViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }

  /**
   * This method should update the contents of the {@link VH#itemView} to reflect the item at the
   * given position.
   *
   * @param holder The ViewHolder which should be updated to represent the contents of the
   *        item at the given position in the data set.
   * @param position The position of the item within the adapter's data set.
   */



}