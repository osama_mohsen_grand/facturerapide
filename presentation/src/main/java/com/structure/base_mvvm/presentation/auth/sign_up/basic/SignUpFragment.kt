package com.structure.base_mvvm.presentation.auth.sign_up.basic

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.FragmentSignUpBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignUpBinding>() {

  private val viewModel: SignUpViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_sign_up

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.device_token = getDeviceId(requireContext())
  }

  private val TAG = "SignUpFragment"

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      Log.d(TAG, "event $it")
      when (it) {
        Constants.LOGIN -> backToPreviousScreen()
        Constants.PICKER_IMAGE -> {
          singleTedBottomPicker(requireActivity())
        }
      }
    }
    selectedImages.observeForever {
      selectedImages.value?.path?.let { it1 ->
        run {
          viewModel.request.setImage(it1,Constants.IMAGE)
          viewModel.notifyChange()
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          navigateSafe(SignUpFragmentDirections.actionOpenConfirmCodeFragment(viewModel.request.email,Constants.VERIFY,Constants.LOGIN))
        }
      }
    }
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}