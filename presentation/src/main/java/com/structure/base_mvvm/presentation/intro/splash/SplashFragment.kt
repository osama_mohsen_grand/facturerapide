package com.structure.base_mvvm.presentation.intro.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.base.BaseActivity
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.FragmentSplashBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>() {

  private val viewModel: SplashViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_splash

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    Log.d(TAG, "setBindingVariables: ")
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    Log.d(TAG, "onCreateView: ")
    return super.onCreateView(inflater, container, savedInstanceState)
  }

  override
  fun setUpViews() {
    Log.d(TAG, "setUpViews: ")
    init()
  }

  override fun setupObservers() {
//    Log.d(TAG, "setupObservers: ")
//    init()

  }

  private val TAG = "SplashFragment"

  private fun init() {
    hideKeyboard()
    Log.d(TAG, "init: ")
    viewModel.setupFirebaseToken()
    Log.d(TAG, "init: is HERE")
    Handler(Looper.getMainLooper()).postDelayed({
      //Do something after 100ms
      viewModel.clickEventPressed.observe(this, {
        Log.d(TAG, "init: $it")
        when (it) {

          Constants.AUTH -> {
            navigateSafe(SplashFragmentDirections.actionSplashFragment2ToLogInFragment())
          }
          Constants.HOME -> {

            requireActivity().finishAffinity()
            val intent = Intent(requireContext(), HomeActivity::class.java)
            startActivity(intent)
          }
          Constants.WELCOME -> {
            navigateSafe(SplashFragmentDirections.actionSplashFragment2ToIntroFragment())
          }
          Constants.COMPANY_REGISTER -> {
            navigateSafe(SplashFragmentDirections.actionSplashFragment2ToSignUpCompanyFragment())
          }
        }
      })
    }, 2500)
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }


}