package com.structure.base_mvvm.presentation.invoice

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.presentation.databinding.InvoiceFragmentBinding

import androidx.fragment.app.setFragmentResult
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import android.text.Editable

import android.text.TextWatcher
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import codes.pretty_pop_up.PrettyPopUpHelper
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.invoice.service.ServicesViewModel
import java.util.*


@AndroidEntryPoint
class InvoiceFragment : BaseFragment<InvoiceFragmentBinding>() {
  val args: InvoiceFragmentArgs by navArgs()
  val serviceViewModel: ServicesViewModel by viewModels()

  val viewModel: InvoiceViewModel by viewModels()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    actOnGetIfNotInitialValueOrGetLiveData(
      Constants.ADD_CUSTOMER,
      InvoiceRequest(),
      viewLifecycleOwner,
      { it?.name?.trim()?.isNotEmpty() == true }
    ) {
      viewModel.setCustomer(it)
    }

    actOnGetIfNotInitialValueOrGetLiveData(
      Constants.ADD_SERVICE,
      InvoiceServiceModel(),
      viewLifecycleOwner,
      { it?.name?.trim()?.isNotEmpty() == true }
    ) {
      Log.d(TAG, "onViewCreated: `HERE")
      it?.let {
        Log.d(TAG, "onViewCreated: WORKED")
        viewModel.insertData(it)
        viewModel.calculateData()
      }
    }

    //copy invoice
    findNavController().currentBackStackEntry?.savedStateHandle?.actOnGetLiveDataToResetKey(
      Constants.COPY,
      -1,
      viewLifecycleOwner,
      { it != -1 }
    ) {
      it?.let {
        when (it) {
          5 -> viewModel.previewInvoice()
          6 -> findNavController().navigate(InvoiceFragmentDirections.actionInvoiceFragmentToCopyDialog())
          else -> {
            viewModel.request.invoiceId = it // set new type of invoice from copy
            viewModel.id = -1 // set id = -1 , for creating a new invoice not updating
            viewModel.createInvoice()//create invoice api after select different type
          }
        }
      }
    }

  }


  override
  fun getLayoutId() = R.layout.invoice_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.date = requireActivity().currentDateFormat()
    viewModel.adapter.serviceViewModel = serviceViewModel
    viewModel.setArgs(args.type, args.id, args.name)
//    viewModel.request.device_token = getDeviceId(requireContext())
  }

  private val TAG = "InvoiceFragment"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
      override fun handleOnBackPressed() {

        PrettyPopUpHelper.Builder(childFragmentManager)
          .setStyle(PrettyPopUpHelper.Style.STYLE_1_HORIZONTAL_BUTTONS)
          .setTitle(getString(R.string.back))
          .setTitleColor(getMyColor(R.color.colorPrimaryDark))
          .setContent(getString(R.string.do_you_want_to_exit_from_create_invoice))
          .setContentColor(getMyColor(R.color.gray))
          .setPositiveButtonBackground(R.drawable.app_bar)
          .setPositiveButtonTextColor(getMyColor(R.color.colorWhite))
          .setPositiveButton(getString(R.string.okay)) {
            it.dismiss()
            isEnabled = false
            activity?.onBackPressed()
          }
//      .setNegativeButtonBackground(R.drawable.btn_gray)
          .setNegativeButtonTextColor(getMyColor(R.color.white))
          .setNegativeButton(getMyString(R.string.cancel)) {
            it.dismiss()
          }
          .create()
      }
    })
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      Log.d(TAG, "event $it")
      when (it) {
        Constants.PREVIEW -> navigateSafe(
          InvoiceFragmentDirections.actionInvoiceFragmentToPreviewInvoiceFragmentWithoutClear(
            viewModel.data.pdf
          )
        )
        Constants.COPY -> navigateSafe(
          InvoiceFragmentDirections.actionInvoiceFragmentToPreviewInvoiceFragmentWithoutClear(
            viewModel.data.pdf
          )
        )
        Constants.DATE_TIME -> {
          Log.e(TAG, "setupObservers: ")
          requireActivity().showDate(binding.edtDateTime, false, Locale.FRANCE)
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          when (viewModel.isUpdate.value) {
            false -> {// new invoice
              val url = it.value.data as String
              navigateSafe(
                InvoiceFragmentDirections.actionInvoiceFragmentToPreviewInvoiceFragment(
                  url
                )
              )
            }
            else -> { // copy invoice
              val n = findNavController()
              n.navigateUp()
              n.currentBackStackEntry?.savedStateHandle?.set(
                Constants.RECALL,
                true
              )
            }
          }

        }
      }

    }

    lifecycleScope.launchWhenCreated {
      viewModel.responseInvoiceDetails.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          viewModel.setInvoiceDetails(it.value.data)
        }
      }
    }

    lifecycleScope.launchWhenCreated {
      viewModel.responseUpdate.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          val invoiceItem = InvoiceItem()
          invoiceItem.id = viewModel.id
          invoiceItem.customerName = viewModel.request.name
          invoiceItem.customerEmail = viewModel.request.email
//          invoiceItem.maturityTime = viewModel.request.time
          invoiceItem.total = viewModel.request.total
          invoiceItem.invoiceNumber = viewModel.request.number
          invoiceItem.items = viewModel.request.services.size.toString()
          val result = Bundle().apply {
            putSerializable("model", invoiceItem)
          }
          setFragmentResult("bundle", result)
          backToPreviousScreen()
        }
      }
    }

    lifecycleScope.launchWhenCreated {
      viewModel.responseDeleted.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          val result = Bundle().apply {
            putInt("deleted", 1)
          }
          setFragmentResult("bundle", result)
          backToPreviousScreen()
        }
      }
    }

    binding.edtInvoiceTax.addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable) {
        viewModel.request.tax = binding.edtInvoiceTax.text.toString()
        viewModel.calculateData()
      }

      override fun beforeTextChanged(
        s: CharSequence, start: Int,
        count: Int, after: Int
      ) {
      }

      override fun onTextChanged(
        s: CharSequence, start: Int,
        before: Int, count: Int
      ) {
      }
    })

    viewModel.adapter.click.observe(this, {
      when (it.type) {
        Constants.DELETE -> {
          viewModel.request.services.removeAt(it.position)
          viewModel.calculateData()
          viewModel.notifyAdapter()
          viewModel.notifyPropertyChanged(BR.request)
        }
      }
    })
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}