package com.structure.base_mvvm.presentation.home

import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.get
import androidx.core.view.isEmpty
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.nex3z.notificationbadge.NotificationBadge
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.NavHomeDirections
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseActivity
import com.structure.base_mvvm.presentation.base.extensions.hide
import com.structure.base_mvvm.presentation.base.extensions.show
import com.structure.base_mvvm.presentation.databinding.ActivityHomeBinding
import com.structure.base_mvvm.presentation.home.viewModels.HomeViewModel
import com.structure.base_mvvm.presentation.invoices.viewModels.InvoicesViewModel
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
  private lateinit var appBarConfiguration: AppBarConfiguration

  private val viewModel: HomeViewModel by viewModels()
  lateinit var binding:ActivityHomeBinding
  var prevId = null
  //  override
//  fun getLayoutId() = R.layout.activity_home
  var notification_count = 0
  var is_subscribed = 0

  fun setLocal(){
    val locale = Locale(LanguagesHelper.getCurrentLanguage(this))
    Locale.setDefault(locale)
    val resources: Resources = getResources()
    val config: Configuration = resources.getConfiguration()
    config.setLocale(locale)
    resources.updateConfiguration(config, resources.getDisplayMetrics())
  }



  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setLocal()
    Locale.setDefault(Locale(LanguagesHelper.getCurrentLanguage(this)));
    binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
    binding.viewModel = viewModel
    binding.lifecycleOwner = this
    setUpBottomNavigation()
//    setContentView(R.layout.activity_home)

  }
  private val TAG = "HomeActivity"
  lateinit var nav: NavController
  fun setUpBottomNavigation() {
    val navHostFragment =
      supportFragmentManager.findFragmentById(R.id.nav_home_container) as NavHostFragment
    nav = navHostFragment.findNavController()
    appBarConfiguration = AppBarConfiguration(
      setOf(R.id.home_fragment, R.id.invoices_Fragment, R.id.account_fragment, R.id.more_fragment,R.id.packagesFragment )
    )

    viewModel.titleToolbar.value = getString(R.string.home)
    nav.addOnDestinationChangedListener { controller, destination, arguments ->
      Log.d(TAG, "TT_setUpBottomNavigation: "+viewModel.titleToolbar.value)
      when (destination.id) {
        R.id.home_fragment, R.id.invoices_Fragment, R.id.account_fragment, R.id.more_fragment-> {
          if(destination.id == R.id.invoices_Fragment){
            viewModel.titleToolbar.value = getString(R.string.history)
          }
          binding.bottomNavigationView.show()
          if(binding.toolbar.menu.isEmpty()) {
            binding.toolbar.inflateMenu(R.menu.menu_home)
            updateNotification(notification_count,is_subscribed)
          }
        }
        R.id.packagesFragment -> {
          binding.bottomNavigationView.hide()
          binding.toolbar.menu.clear()
        }
        R.id.notificationFragment -> {
          updateNotification(0,1)
          binding.toolbar.menu.clear()
        }
        else -> {
          binding.bottomNavigationView.hide()
          binding.toolbar.menu.clear()
        }
      }
    }
    binding.toolbar.setupWithNavController(nav, appBarConfiguration)
    binding.bottomNavigationView.setupWithNavController(nav)
  }


  fun updateNotification(notificationCountData: Int = 0, subscribed: Int) {
    notificationCountData.let {
      this.notification_count = notificationCountData
      if(subscribed == 1) {
        is_subscribed = subscribed
        if (binding.toolbar.menu.isEmpty()) {
          binding.toolbar.inflateMenu(R.menu.menu_home)
        }
        val relative = binding.toolbar.menu[0].actionView
        val imgNotification: AppCompatImageView = relative.findViewById(R.id.img_notification)
        val textView: NotificationBadge = relative.findViewById(R.id.badge)
        if (notificationCountData != 0) {
          textView.setNumber(notificationCountData)
          textView.show()
        }

        imgNotification.setOnClickListener {
          textView.hide()
          moveToNotification()
        }
      }
    }
  }

  fun moveToNotification() {
    nav.navigate(NavHomeDirections.moveToNotification())
  }
}