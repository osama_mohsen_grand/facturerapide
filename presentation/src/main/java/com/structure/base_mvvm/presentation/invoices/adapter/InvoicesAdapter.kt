package com.structure.base_mvvm.presentation.invoices.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ItemInvoiceBinding
import com.structure.base_mvvm.presentation.invoices.viewModels.ItemInvoiceViewModel

class InvoicesAdapter: RecyclerView.Adapter<InvoicesAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<InvoiceItem> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<InvoiceItem>() {
    override fun areItemsTheSame(oldItem: InvoiceItem, newItem: InvoiceItem): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: InvoiceItem, newItem: InvoiceItem): Boolean {
      return oldItem == newItem
    }

  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_invoice, parent, false)
    return ViewHolder(view)
  }


  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemInvoiceViewModel(data,position)
    holder.setViewModel(itemViewModel)
    holder.itemLayoutBinding.btnSubmit.setOnClickListener {
      clickEvent.value = data
      Log.d("TAG", "positiondViewHolder: "+data.id)
    }

  }

  private val TAG = "InvoicesAdapter"
  fun insertData(insertList: List<InvoiceItem>) {
    val array = ArrayList<InvoiceItem>(differ.currentList)
    val size = array.size
    array.addAll(insertList)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }


  fun deletePosition(deleted: InvoiceItem) {
    val array = ArrayList<InvoiceItem>(differ.currentList)
    array.remove(deleted)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }

  fun findIndex(arr: ArrayList<InvoiceItem>, item: InvoiceItem): Int {
    return arr.indexOf(item)
  }

  fun updateItem(prev: InvoiceItem, itemNew: InvoiceItem) {
    val array = ArrayList<InvoiceItem>(differ.currentList)
    val index = findIndex(array, prev)
    if(index != -1) {
      array[index] = itemNew;
      Log.d(TAG, "updateItem: "+itemNew.invoiceNumber)
      differ.submitList(array)
      notifyDataSetChanged()
    }
  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }


  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemInvoiceBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemInvoiceViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }


}