package com.structure.base_mvvm.presentation.invoice.service

import android.util.Log
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.domain.utils.Constants.ADD_CUSTOMER
import com.structure.base_mvvm.domain.utils.Constants.ADD_SERVICE
import com.structure.base_mvvm.domain.utils.Constants.DISMISS
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ServiceFragmentBinding
import com.structure.base_mvvm.presentation.databinding.ServicesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ServicesFragment : BaseFragment<ServicesFragmentBinding>() {

  val viewModel: ServicesViewModel by viewModels()
  val serviceViewModel: ServicesViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.services_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.adapter.serviceViewModel = serviceViewModel
  }


  override
  fun setUpViews() {

  }

  private  val TAG = "ServicesFragment"
  override fun setupObservers() {
    setRecyclerViewScrollListener()
    lifecycleScope.launchWhenResumed {
      viewModel.responseServices.collect {
        println("collection here $it")
        handleLoading(it)
        if(it is Resource.Success){
          viewModel.setData(it.value.data)
        }
      }
    }
    viewModel.clickEvent.observe(this){
      when(it){
        DISMISS -> {
          Log.d(TAG, "setupObservers: DISMISS")
          if(this::dialog.isInitialized){
            Log.d(TAG, "setupObservers: INIT")
            if(dialog.isShowing) {
              dialog.dismiss()
            }
          }
          val n = findNavController()
          n.navigateUp()
          n.currentBackStackEntry?.savedStateHandle?.set(
            ADD_SERVICE,
            viewModel.invoiceServiceModel
          )
        }
        ADD_SERVICE -> {
          openService()
        }
      }
    }
  }


  private fun setRecyclerViewScrollListener() {

    val layoutManager = LinearLayoutManager(requireContext())
    binding.recyclerView.layoutManager = layoutManager

    binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(1)){
          viewModel.callService()
        }
      }
    })
  }


  private lateinit var dialog: BottomSheetDialog

  fun openService(){
    dialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogStyle)
    val binding: ServiceFragmentBinding = DataBindingUtil.inflate(
      LayoutInflater.from(requireContext()),
      R.layout.service_fragment,
      null,
      false
    )
    binding.viewModel = viewModel
    dialog.setContentView(binding.root)

    dialog.setOnDismissListener {
      viewModel.invoiceServiceModel = InvoiceServiceModel()
    }
    dialog.show()
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}