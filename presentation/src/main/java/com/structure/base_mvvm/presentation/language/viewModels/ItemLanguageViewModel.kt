package com.structure.base_mvvm.presentation.language.viewModels

import android.util.Log
import android.view.View
import androidx.fragment.app.findFragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.domain.language.Language
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.invoices.InvoicesFragmentDirections
import com.structure.base_mvvm.presentation.language.LanguageFragment

class ItemLanguageViewModel constructor(
  val model: Language,
  val position: Int,
  val isSelected: Boolean
) : BaseViewModel() {
  private val TAG = "ItemLanguageViewModel"
  fun submit(v: View) {
    try {
      val fragment = v.findFragment<LanguageFragment>()
      fragment.viewModel.updateLanguage(position)
    } catch (e: Exception) {
      e.printStackTrace()
    }

  }
}