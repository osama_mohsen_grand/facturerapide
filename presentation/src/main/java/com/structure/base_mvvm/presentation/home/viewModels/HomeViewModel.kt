package com.structure.base_mvvm.presentation.home.viewModels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.home.models.CategoryModel
import com.structure.base_mvvm.domain.home.models.HomeModel
import com.structure.base_mvvm.domain.home.use_case.HomeUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.invoices.adapter.HomeAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeUseCase: HomeUseCase,
                                        private val accountFirebase : AccountRepository,
                                        val saveUserToLocalUseCase: SaveUserToLocalUseCase) : BaseViewModel() {
//  private val _homeResponse =
//    MutableStateFlow<Resource<BaseResponse<NotificationPaginateData>>>(Resource.Default)
val homeResponse =
    MutableStateFlow<Resource<BaseResponse<HomeModel>>>(Resource.Default)

  private val TAG = "HomeViewModel"
  val adapter = HomeAdapter()
  val titleToolbar = MutableLiveData<String>("")
  init {
  }

  fun invoices(){
    clickEvent.value = Constants.INVOICES
  }


  fun updateFirebase(deviceId: String) {
//    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
//      if (!task.isSuccessful) {
//        return@OnCompleteListener
//      }
//      var result = task.result
//      Log.d(TAG, "setupFirebaseToken: "+result)
//      //shared perereference
//      saveUserToLocalUseCase(result)
//    })

    homeUseCase.home(deviceId, accountFirebase.getFirebaseToken().toString())
      .onEach { result ->
        homeResponse.value = result
      }
      .launchIn(viewModelScope)


    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
      if (!task.isSuccessful) {
        return@OnCompleteListener
      }
      var result = task.result
      Log.d(TAG, "setupFirebaseToken: $result")
      //shared perereference
      saveUserToLocalUseCase(result)
    })

  }

}