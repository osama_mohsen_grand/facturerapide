package com.structure.base_mvvm.presentation.more.info

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.backToPreviousScreen
import com.structure.base_mvvm.presentation.base.extensions.hideKeyboard
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.FragmentSignUpBinding
import com.structure.base_mvvm.presentation.databinding.InfoFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

@AndroidEntryPoint
class InfoFragment : BaseFragment<InfoFragmentBinding>() {

  val args: InfoFragmentArgs by navArgs()


  private val viewModel: InfoViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.info_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.type = args.type
    viewModel.callSettingsApi()
  }

  private val TAG = "SignUpFragment"

  override
  fun setupObservers() {
    lifecycleScope.launchWhenResumed {
      viewModel.responseResult.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          it.value.data?.let { it1 -> viewModel.setValue(it1)
          }
        }
      }
    }
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}