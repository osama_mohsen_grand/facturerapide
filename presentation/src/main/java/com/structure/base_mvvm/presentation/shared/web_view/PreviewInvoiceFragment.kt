package com.structure.base_mvvm.presentation.shared.web_view

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.PreviewInvoiceFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import android.app.DownloadManager
import android.app.Service
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Environment
import android.print.PrintManager
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat

import kotlin.random.Random
import androidx.core.content.ContextCompat.getSystemService as getSystemService1
import androidx.core.content.ContextCompat.getSystemService
import android.content.Intent
import android.webkit.WebChromeClient
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.extensions.shareApp
import com.structure.base_mvvm.presentation.base.utils.showNoApiErrorAlert


@AndroidEntryPoint
class PreviewInvoiceFragment :  BaseFragment<PreviewInvoiceFragmentBinding>()  {

  val args: PreviewInvoiceFragmentArgs by navArgs()

  private val viewModel: PreviewInvoiceViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.preview_invoice_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  private  val TAG = "PreviewInvoiceFragment"

  //  @SuppressLint("SetJavaScriptEnabled")
  @SuppressLint("SetJavaScriptEnabled")
  private fun setupWebView() {
    Log.d(TAG, "setupWebView: "+args.pdf)

    binding.webview.settings.javaScriptEnabled = true
    binding.webview.measure(100, 100);
    binding.webview.settings.useWideViewPort = true;
    binding.webview.settings.loadWithOverviewMode = true;

    binding.webview.settings.setDomStorageEnabled(true);
    binding.webview.settings.setLoadsImagesAutomatically(true);
    binding.webview.settings.setDatabaseEnabled(true);
    binding.webview.settings.setSupportZoom(true);
    binding.webview.settings.setBuiltInZoomControls(true);

    binding.progress.show()
    binding.webview.webViewClient = WebViewClient()


    binding.webview.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + args.pdf);
//    binding.webview.setListener(requireActivity(), this)
//    binding.webview.setMixedContentAllowed(true)
//    binding.webview.settings.javaScriptEnabled = true
//    binding.webview.settings.useWideViewPort = true
//    binding.webview.settings.loadWithOverviewMode = true



//    binding.webview.settings.builtInZoomControls = true
//    binding.webview.settings.displayZoomControls = false
    viewModel.clickEvent.observe(this) {
      Log.d(TAG, "event $it")
      when (it) {
        Constants.SAVE -> {
          if(checkStoragePermissions(requireActivity())) {
            if (args.pdf.isNullOrEmpty()) {
              showNoApiErrorAlert(requireActivity(), getString(R.string.some_error))
              
              return@observe
            }

            val uri: Uri = Uri.parse(args.pdf)
            val request = DownloadManager.Request(uri)
              .setTitle(getString(R.string.invoice))
              .setMimeType("application/pdf")
              .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
              .setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                "${Random.nextInt(1, 100000)}.pdf"
              )
              .setAllowedOverMetered(true)
            val manager = requireActivity().getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            manager.enqueue(request)
          }
        }
        Constants.SHARE -> {
          shareContent(args.pdf)
        }
        Constants.PRINT -> {
//          shareApp(args.url)
          val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(args.pdf))
          startActivity(browserIntent)
        }
      }
    }

  }

  fun save(){

//    val uri = Uri.parse("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf")
//    val request = DownloadManager.Request(uri)
//    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
//    val reference: Long = manager.enqueue(request)

//    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI) // Tell on which network you want to download file.
//
//    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) // This will show notification on top when downloading the file.
//
//    request.setTitle("Downloading data...") // Title for notification.
////    request.addRequestHeader("Authorization", "bearer my bearertoken");
//    request.setDestinationInExternalFilesDir(requireContext(), Environment.DIRECTORY_DOCUMENTS, "${Random.nextInt(0, 10000)}.pdf");
//




  }

  inner class WebViewClient : android.webkit.WebViewClient() {
    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
      view.loadUrl(url)
      return false
    }
    override fun onPageFinished(view: WebView, url: String) {
      super.onPageFinished(view, url)
      if (view.getTitle().equals("")) {
        view.reload();
      }else
        binding.progress.hide()
    }
  }


  override
  fun getFragmentArguments() {
  }

  override
  fun setUpViews() {
    setupWebView()
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }


  override fun onResume() {
    super.onResume()
    binding.webview.onResume();
  }

  override fun onPause() {
    binding.webview.onPause()
    super.onPause()
  }

  override fun onDestroy() {
//    binding.webview.onDestroy()
    super.onDestroy()
  }
}