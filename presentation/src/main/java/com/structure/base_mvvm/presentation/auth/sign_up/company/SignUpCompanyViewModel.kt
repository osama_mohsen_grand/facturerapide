package com.structure.base_mvvm.presentation.auth.sign_up.company

import android.content.Context
import android.util.Log
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.account.use_case.GetUserFromLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.request.RegisterCompanyRequest
import com.structure.base_mvvm.domain.auth.use_case.RegisterCompanyUseCase
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpCompanyViewModel @Inject constructor(
  private val useCase: RegisterCompanyUseCase,
  val userUseCase: GetUserFromLocalUseCase,
  val checkLoggedInUserUseCase: CheckLoggedInUserUseCase
) : BaseViewModel() {

  val sizeLogo = ObservableInt(1)
  private val TAG = "SignUpCompanyViewModel"
  var request = RegisterCompanyRequest()

  init {
    request.isLogin = checkLoggedInUserUseCase.invoke()
    if (request.isLogin) {
      userUseCase.accountRepository.getUserLocal().company.name?.let {
        request.name = it
      }
      userUseCase.accountRepository.getUserLocal().company.address?.let {
        request.address = it
      }
      userUseCase.accountRepository.getUserLocal().company.email?.let {
        request.email = it
      }
      userUseCase.accountRepository.getUserLocal().company.phone?.let {
        request.phone = it
      }
      userUseCase.accountRepository.getUserLocal().company.street?.let {
        request.street = it
      }
      userUseCase.accountRepository.getUserLocal().company.address2?.let {
        request.address2 = it
      }
      userUseCase.accountRepository.getUserLocal().company.commercialNumber?.let {
        request.commercialNumber = it
      }
      userUseCase.accountRepository.getUserLocal().company.image_size?.let {
        request.image_size = it
        when (it) {
          "s" -> sizeLogo.set(1)
          "m" -> sizeLogo.set(2)
          else -> sizeLogo.set(3)
        }
      }
      userUseCase.accountRepository.getUserLocal().company.companyVatNumber?.let {
        request.companyVatNumber = it
      }
      userUseCase.accountRepository.getUserLocal().company.website?.let {
        request.company_website = it
      }
      userUseCase.accountRepository.getUserLocal().company.invoiceColor?.let {
        request.invoiceColor = it
      }
//      request.email = userUseCase.accountRepository.getUserLocal().company.email.toString()
//      request.phone = userUseCase.accountRepository.getUserLocal().company.phone.toString()
//      request.street = userUseCase.accountRepository.getUserLocal().company.street
//      request.address2 = userUseCase.accountRepository.getUserLocal().company.address2
//      request.postalCode = userUseCase.accountRepository.getUserLocal().company.postalCode
//      request.commercialNumber =
//        userUseCase.accountRepository.getUserLocal().company.commercialNumber
//      request.image_size = userUseCase.accountRepository.getUserLocal().company.image_size
//      request.companyVatNumber = userUseCase.accountRepository.getUserLocal().company.companyVatNumber
//      request.company_website = userUseCase.accountRepository.getUserLocal().company.website
//      request.invoiceColor = userUseCase.accountRepository.getUserLocal().company.invoiceColor

//      Log.d(TAG, "${sizeLogo.get()}: ")
    }
    Log.d(TAG, ": ENDDD")
  }

  fun signup() {
    viewModelScope.launch(job) {
      useCase.submit(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun pickColor(){
    Log.d(TAG, "pickColor: worked")
    clickEvent.value = Constants.COLOR
  }

  fun changeLogoSize(size: Int) {
    sizeLogo.set(size)
    request.image_size = when (size) {
      1 -> "s"
      2 -> "m"
      else -> "l"
    }
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }

}