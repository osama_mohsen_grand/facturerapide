package com.structure.base_mvvm.presentation.invoices

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.InvoicesFragmentBinding
import com.structure.base_mvvm.presentation.invoices.viewModels.InvoicesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import android.view.inputmethod.EditorInfo

import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.activityViewModels
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.extensions.actOnGetIfNotInitialValueOrGetLiveData
import com.structure.base_mvvm.presentation.home.viewModels.HomeViewModel


@AndroidEntryPoint
class InvoicesFragment : BaseFragment<InvoicesFragmentBinding>() {

  private val viewModel: InvoicesViewModel by viewModels()

  private val activityViewModel: HomeViewModel by activityViewModels()

  override
  fun getLayoutId() = R.layout.invoices_fragment


  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    actOnGetIfNotInitialValueOrGetLiveData(
      Constants.RECALL,
      false,
      viewLifecycleOwner,
      { it == true }
    ) {
      Log.d(TAG, "onViewCreated: HERE")
      viewModel.submit(viewModel.type,viewModel.title)
    }
  }
  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  lateinit var invoiceItem : InvoiceItem

  private val TAG = "InvoicesFragment"
  override
  fun setUpViews() {
    viewModel.title = getString(R.string.create_an_invoice)
    activityViewModel.titleToolbar.value = getString(R.string.history)
//    viewModel.clickEvent.observe(this) {
//      Log.d(TAG, "TT_setUpViews: $it")
//      activityViewModel.titleToolbar.value = it
//    }
    setFragmentResultListener("bundle") { requestKey: String, bundle: Bundle ->
      if(bundle.containsKey("deleted"))
        viewModel.adapter.deletePosition(invoiceItem)
      else if(bundle.containsKey("model")){
        viewModel.adapter.updateItem(invoiceItem, bundle.getSerializable("model") as InvoiceItem)
      }
    }
    binding.edtNvoiceSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
      if (actionId == EditorInfo.IME_ACTION_SEARCH) {
        viewModel.submit(viewModel.type,viewModel.title)
        return@OnEditorActionListener true
      }
      false
    })
    setRecyclerViewScrollListener()
  }


  override fun setupObservers() {

    viewModel.adapter.clickEvent.observe(this){
      invoiceItem = it
      navigateSafe(InvoicesFragmentDirections.actionInvoicesFragmentToInvoiceFragment(viewModel.type,it.id,viewModel.title))
    }

    lifecycleScope.launchWhenResumed {
      viewModel.responseService.collect {
        println("collection here $it")
        handleLoading(it)
        if(it is Resource.Success){
          viewModel.setData(it.value.data)
        }
      }

    }
  }


  private fun setRecyclerViewScrollListener() {

    val layoutManager = LinearLayoutManager(requireContext())
    binding.recyclerView.layoutManager = layoutManager
    binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(1)){
          viewModel.getInvoices()
        }
      }
    })
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }

  /**
   * Called when a swipe gesture triggers a refresh.
   */
}