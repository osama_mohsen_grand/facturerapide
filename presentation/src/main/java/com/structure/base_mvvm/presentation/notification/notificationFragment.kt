package com.structure.base_mvvm.presentation.notification

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.showMessageSuccess
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.SwipeToDeleteCallback
import com.structure.base_mvvm.presentation.databinding.NotificationFragmentBinding
import com.structure.base_mvvm.presentation.notification.viewmodel.NotificationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class notificationFragment : BaseFragment<NotificationFragmentBinding>() {

  private val viewModel: NotificationViewModel by viewModels()

  override
  fun getLayoutId() = com.structure.base_mvvm.presentation.R.layout.notification_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
  }

  override
  fun setUpViews() {
    setRecyclerViewScrollListener()
    val swipeHandler = object : SwipeToDeleteCallback(requireActivity()) {
      override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        viewModel.remove(viewModel.adapter.differ.currentList[viewHolder.adapterPosition])
        viewModel.adapter.removeItem(viewHolder.adapterPosition)
        showMessageSuccess(getString(R.string.notification_removed_successfully))
      }
    }
    val itemTouchHelper = ItemTouchHelper(swipeHandler)
    itemTouchHelper.attachToRecyclerView(binding.recyclerView)


  }

  override fun setupObservers() {

    lifecycleScope.launchWhenResumed {
      viewModel.responseNotifications.collect {
        println("collection here $it")
        handleLoading(it)
        if(it is Resource.Success){
          viewModel.setData(it.value.data)
        }
      }
    }
  }


  private fun setRecyclerViewScrollListener() {

    val layoutManager = LinearLayoutManager(requireContext())
    binding.recyclerView.layoutManager = layoutManager

    binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(1)){
          viewModel.getNotifications()
        }
      }
    })
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}