package com.structure.base_mvvm.presentation.invoices.viewModels

import android.util.Log
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.repository.InvoiceRepository
import com.structure.base_mvvm.domain.invoice.usecase.InvoiceUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.invoices.adapter.InvoicesAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InvoicesViewModel @Inject constructor(
  private val repository: InvoiceRepository,
  private val useCase: InvoiceUseCase,
) : BaseViewModel() {
  var search = ""
  var title = ""
  @Bindable
  var type: Int = 1
  @Bindable
  var page: Int = 0
  @Bindable
  var callingService = false

  @Bindable
  var showBar = false

  var isLast = false

  @Bindable
  var adapter = InvoicesAdapter()


  val responseService =
    MutableStateFlow<Resource<BaseResponse<InvoicePaginateData>>>(Resource.Default)

  init {
    getInvoices()
  }

  private val TAG = "PackagesViewModel"
  fun getInvoices() {
    if (!callingService && !isLast) {
      showBar = search.trim().isEmpty()
      notifyPropertyChanged(BR.showBar)
      callingService = true
      notifyPropertyChanged(BR.callingService)
      viewModelScope.launch(job) {
        page++
        if(page > 1){
          notifyPropertyChanged(BR.page)
        }
        useCase.getInvoices( page,type,search)
          .catch { exception -> validationException.value = exception.message?.toInt() }
          .onEach { result ->
            responseService.value = result
          }.launchIn(viewModelScope)
      }
    }
  }

  fun setData(data: InvoicePaginateData?) {
    data?.let {
      println("size:" + data.list.size)
      isLast = data.links.next == null
      if (page == 1) {
//        adapter = InvoicesAdapter()
        adapter.differ.submitList(it.list)
      } else {
        adapter.insertData(it.list)
      }
      notifyPropertyChanged(BR.adapter)
      callingService = false
      notifyPropertyChanged(BR.callingService)
    }
  }


  fun submit(type: Int,title : String) {
    this.type = type
    this.title = title
    notifyPropertyChanged(BR.type)
    page = 0
    isLast = false
    callingService = false
    getInvoices()
    clickEvent.value = title
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}