package com.structure.base_mvvm.presentation.invoices.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.home.models.CategoryModel
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ItemHomeBinding
import com.structure.base_mvvm.presentation.databinding.ItemInvoiceBinding
import com.structure.base_mvvm.presentation.home.viewModels.ItemHomeViewModel
import com.structure.base_mvvm.presentation.invoices.viewModels.ItemInvoiceViewModel

class HomeAdapter: RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<CategoryModel> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<CategoryModel>() {
    override fun areItemsTheSame(oldItem: CategoryModel, newItem: CategoryModel): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CategoryModel, newItem: CategoryModel): Boolean {
      return oldItem == newItem
    }

  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false)
    val height = parent.measuredHeight / 2
    view.minimumHeight = height
    view.minimumWidth = height

    return ViewHolder(view)
  }


  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemHomeViewModel(data)
    holder.setViewModel(itemViewModel)
    holder.itemLayoutBinding.createInvoice.setOnClickListener {
      clickEvent.value = data
      Log.d("TAG", "positiondViewHolder: "+data.id)
    }

  }

  private val TAG = "HomeAdapter"
  fun insertData(insertList: List<CategoryModel>) {
    val array = ArrayList<CategoryModel>(differ.currentList)
    differ.submitList(array)
    val size = array.size
    array.addAll(insertList)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)

    differ.submitList(array)
    notifyDataSetChanged()
  }


  fun deletePosition(deleted: CategoryModel) {
    val array = ArrayList<CategoryModel>(differ.currentList)
    array.remove(deleted)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }

  fun findIndex(arr: ArrayList<CategoryModel>, item: CategoryModel): Int {
    return arr.indexOf(item)
  }

  fun updateItem(prev: CategoryModel, itemNew: CategoryModel) {
    val array = ArrayList<CategoryModel>(differ.currentList)
    val index = findIndex(array, prev)
    if(index != -1) {
      array[index] = itemNew;
      differ.submitList(array)
      notifyDataSetChanged()
    }
  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }


  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemHomeBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemHomeViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }


}