package com.structure.base_mvvm.presentation.language.helper;

import static com.structure.base_mvvm.domain.utils.Constants.LANGUAGE_ENGLISH;
import static com.structure.base_mvvm.domain.utils.Constants.LANGUAGE_FRENCH;
import static com.structure.base_mvvm.domain.utils.Constants.LANGUAGE_SPANISH;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.Log;

import com.structure.base_mvvm.domain.utils.Constants;

import java.util.Locale;


public class LanguagesHelper {

  public static void changeLanguage(Context context, String languageToLoad) {
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    setLanguage(context, languageToLoad);
  }


  public static void setLanguage(Context context, String language) {
    SharedPreferences userDetails = context.getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = userDetails.edit();
    editor.putString(Constants.LANGUAGE, language);
    editor.apply();
  }

  private static final String TAG = "LanguagesHelper";
  public static String getCurrentLanguage(Context context) {
    SharedPreferences preferences = context.getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
    if (preferences.getString(Constants.LANGUAGE, "").length() > 0) {
      return preferences.getString(Constants.LANGUAGE, Constants.DEFAULT_LANGUAGE);
    } else {
      String lang = Locale.getDefault().getLanguage();
      Log.d(TAG, "getCurrentLanguage: "+lang);
      if (lang.equals(LANGUAGE_ENGLISH) || lang.equals(LANGUAGE_SPANISH) || lang.equals(LANGUAGE_FRENCH))
        setLanguage(context, lang);
      else
        setLanguage(context, Constants.DEFAULT_LANGUAGE);
      return Constants.DEFAULT_LANGUAGE;
    }
  }
}
