package com.structure.base_mvvm.presentation.invoice.service

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.domain.invoice.usecase.InvoiceUseCase
import com.structure.base_mvvm.domain.service.ServicesPaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ServicesViewModel @Inject constructor(private val invoiceUseCase: InvoiceUseCase
) : BaseViewModel() {

  var request = InvoiceRequest()

  @Bindable
  var page: Int = 0

  @Bindable
  var callingService = false

  var isLast = false

  @Bindable
  var adapter = ServicesAdapter()


  val responseServices =
    MutableStateFlow<Resource<BaseResponse<ServicesPaginateData>>>(Resource.Default)
  var invoiceServiceModel = InvoiceServiceModel()



  init {
    callService()
  }

  fun callService() {
    if (!callingService && !isLast) {
      notifyPropertyChanged(BR.showBar)
      callingService = true
      notifyPropertyChanged(BR.callingService)
      viewModelScope.launch(job) {
        page++
        if(page > 1){
          notifyPropertyChanged(BR.page)
        }
        invoiceUseCase.getServices(page)
          .catch { exception -> validationException.value = exception.message?.toInt() }
          .onEach { result ->
            responseServices.value = result
          }.launchIn(viewModelScope)
      }
    }
  }

  fun setData(data: ServicesPaginateData?) {
    data?.let {
      println("size:" + data.list.size)
      isLast = data.links.next == null
      if (page == 1) {
//        adapter = InvoicesAdapter()
        adapter.differ.submitList(it.list)
      } else {
        adapter.insertData(it.list)
      }
      notifyPropertyChanged(BR.adapter)
      callingService = false
      notifyPropertyChanged(BR.callingService)
    }
  }

  fun addService(){
    clickEvent.value = Constants.ADD_SERVICE
  }

  private val TAG = "ServicesViewModel"
  var isAllowSubmit: Boolean  = false
  fun addServiceSubmit(context: Context) {
    isAllowSubmit = false
    if (invoiceServiceModel.name.isEmpty()) {
      Toast.makeText(context, ""+context.getString(R.string.empty_name), Toast.LENGTH_SHORT).show()
      validationException.value = AuthValidation.EMPTY_NAME.value
      return
    }
    if (invoiceServiceModel.price.isEmpty()) {
      Toast.makeText(context, ""+context.getString(R.string.please_enter_your_price), Toast.LENGTH_SHORT).show()
      validationException.value = AuthValidation.EMPTY_PRICE.value
      return
    }
    if (invoiceServiceModel.quantity.isEmpty()) {
      Toast.makeText(context, ""+context.getString(R.string.please_enter_your_qunatity), Toast.LENGTH_SHORT).show()
      validationException.value = AuthValidation.EMPTY_QUANTITY.value
      return
    }
//    if (invoiceServiceModel.description.isEmpty()) {
//      Toast.makeText(context, ""+context.getString(R.string.please_enter_your_description), Toast.LENGTH_SHORT).show()
//      validationException.value = AuthValidation.EMPTY_DESCRIPTION.value
//      return
//    }
    isAllowSubmit = true
    clickEvent.value = Constants.DISMISS
    Log.d(TAG, "addServiceSubmit: " + request.services.size)

  }

  fun selectService(position: Int) {
    if(position != -1) {
      invoiceServiceModel = adapter.differ.currentList[position]
      clickEvent.value = Constants.DISMISS
    }
  }
}