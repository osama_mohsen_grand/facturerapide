package com.structure.base_mvvm.presentation.auth.forgot_password

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.backToPreviousScreen
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.FragmentForgetPasswordBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ForgetPasswordFragment : BaseFragment<FragmentForgetPasswordBinding>() {

  val args :ForgetPasswordFragmentArgs by navArgs()

  private val viewModel: ForgotPasswordViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_forget_password

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.type = args.type
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      if (it == Constants.LOGIN)
        backToPreviousScreen()
    }
    lifecycleScope.launchWhenCreated {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          navigateSafe(ForgetPasswordFragmentDirections.actionForgotPasswordFragmentToFragmentConfirmCode(viewModel.request.email,viewModel.request.type,Constants.FORGET_PASSWORD))
        }
      }
    }

  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}