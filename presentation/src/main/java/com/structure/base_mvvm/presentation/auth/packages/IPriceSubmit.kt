package com.structure.base_mvvm.presentation.auth.packages

import com.structure.base_mvvm.domain.packages.entity.Package

interface IPriceSubmit {
  fun submit(position: Int, model: Package)
}