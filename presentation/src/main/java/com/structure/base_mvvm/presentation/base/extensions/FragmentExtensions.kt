package com.structure.base_mvvm.presentation.base.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.structure.base_mvvm.domain.utils.FailureStatus
import com.structure.base_mvvm.domain.utils.Resource.Failure

import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.base.utils.*
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import com.structure.base_mvvm.data.local.preferences.AppPreferences
import com.structure.base_mvvm.presentation.base.BaseActivity


fun Fragment.handleApiError(
  failure: Failure,
  retryAction: (() -> Unit)? = null,
  noDataAction: (() -> Unit)? = null,
  noInternetAction: (() -> Unit)? = null
) {
  when (failure.failureStatus) {
    FailureStatus.EMPTY -> {
      noDataAction?.invoke()
      failure.message?.let { showNoApiErrorAlert(requireActivity(), it) }
    }
    FailureStatus.NO_INTERNET -> {
      noInternetAction?.invoke()
      showNoInternetAlert(requireActivity())
    }
    FailureStatus.TOKEN_EXPIRED -> {
      val preferences = AppPreferences(requireContext());
      preferences.clearPreferences()
      openActivityAndClearStack(AuthActivity::class.java)
    }
    else -> {
      noDataAction?.invoke()
//      requireView().showSnackBar(
//        failure.message ?: resources.getString(R.string.some_error),
//        resources.getString(R.string.retry),
//        retryAction
//      )
    }
  }
}

fun <T> Fragment.actOnGetIfNotInitialValueOrGetLiveData(
  key: String,
  initialValue: T,
  owner: LifecycleOwner,
  conditionOnResultToSetBackToInitialValue: (T?) -> Boolean,
  actionWhenConditionIsMet: (T?) -> Unit
) = findNavController().currentBackStackEntry?.savedStateHandle?.actOnGetIfNotInitialValueOrGetLiveData(
  key, initialValue, owner, conditionOnResultToSetBackToInitialValue, actionWhenConditionIsMet
)

fun <T> SavedStateHandle.actOnGetLiveDataToResetKey(
  key: String,
  initialValue: T,
  owner: LifecycleOwner,
  conditionOnResultToSetBackToInitialValue: (T?) -> Boolean,
  actionWhenConditionIsMet: (T?) -> Unit
) {
  getLiveData(
    key,
    initialValue
  ).observe(owner) {
    if (conditionOnResultToSetBackToInitialValue(it)) {
      set<T>(key, initialValue)

      actionWhenConditionIsMet(it)
    }
  }
}

fun <T> SavedStateHandle.actOnGetIfNotInitialValueOrGetLiveData(
  key: String,
  initialValue: T,
  owner: LifecycleOwner,
  conditionOnResultToSetBackToInitialValue: (T?) -> Boolean,
  actionWhenConditionIsMet: (T?) -> Unit
) {
  val currentValue = get<T>(key)

  if (contains(key) && currentValue != initialValue) {
    remove<T>(key)

    actionWhenConditionIsMet(currentValue)
  }else {
    getLiveData(
      key,
      initialValue
    ).observe(owner) {
      if (conditionOnResultToSetBackToInitialValue(it)) {
        remove<T>(key)

        actionWhenConditionIsMet(it)
      }
    }
  }
}

fun Fragment.hideKeyboard() = hideSoftInput(requireActivity())

fun Fragment.showNoInternetAlert() = showNoInternetAlert(requireActivity())

fun Fragment.showMessage(message: String?) = showMessage(requireContext(), message)
fun Fragment.showMessageSuccess(message: String?) = showSuccess(requireActivity(), message)

fun Fragment.showError(
  message: String,
  retryActionName: String? = null,
  action: (() -> Unit)? = null
) =
  requireView().showSnackBar(message, retryActionName, action)

fun Fragment.getMyColor(@ColorRes id: Int) = ContextCompat.getColor(requireContext(), id)

fun Fragment.getMyDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(requireContext(), id)!!

fun Fragment.getMyString(id: Int) = resources.getString(id)

fun <A : Activity> Fragment.openActivityAndClearStack(activity: Class<A>) {
  requireActivity().openActivityAndClearStack(activity)
}

fun <A : Activity> Fragment.openActivity(activity: Class<A>) {
  requireActivity().openActivity(activity)
}

fun <T> Fragment.getNavigationResultLiveData(key: String = "result") =
  findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)

fun <T> Fragment.removeNavigationResultObserver(key: String = "result") =
  findNavController().currentBackStackEntry?.savedStateHandle?.remove<T>(key)

fun <T> Fragment.setNavigationResult(result: T, key: String = "result") {
  findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}

fun Fragment.onBackPressedCustomAction(action: () -> Unit) {
  requireActivity().onBackPressedDispatcher.addCallback(
    viewLifecycleOwner,
    object : OnBackPressedCallback(true) {
      override
      fun handleOnBackPressed() {
        action()
      }
    }
  )
}
fun startActivityWithClearHistory(fragment: Fragment, direction: Class<*>) {
  fragment.requireActivity().finishAffinity()
  fragment.startActivity(Intent(fragment.context,direction));
}
//fun AppCompatImageView.setImage(path : String){
//  val file = File(path)
//  val myBitmap: Bitmap = BitmapFactory.decodeFile(file.absolutePath)
//  setImageBitmap(myBitmap)
//}

fun Fragment.navigateSafe(directions: NavDirections, navOptions: NavOptions? = null) {
  findNavController().navigate(directions, navOptions)
}

fun Fragment.backToPreviousScreen() {
  findNavController().navigateUp()
}


fun Fragment.shareApp(message : String = ""){
  val sendIntent = Intent()
  sendIntent.action = Intent.ACTION_SEND
  sendIntent.putExtra(
    Intent.EXTRA_TEXT,
    "Hey check out my app at: https://play.google.com/store/apps/details?id=app.facturerapide\n"+message
  )
  sendIntent.type = "text/plain"
  startActivity(sendIntent)
}

fun Fragment.shareContent(message : String = ""){
  val sendIntent = Intent()
  sendIntent.action = Intent.ACTION_SEND
  sendIntent.putExtra(
    Intent.EXTRA_TEXT, message
  )
  sendIntent.type = "text/plain"
  startActivity(sendIntent)
}


fun Fragment.rateApp(){
  val uri = Uri.parse("market://details?id=${requireActivity().packageName}")
  val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
  try {
    startActivity(myAppLinkToMarket)
  } catch (e: ActivityNotFoundException) {
    Toast.makeText(requireContext(), "Impossible to find an application for the market", Toast.LENGTH_LONG).show()
  }
}
