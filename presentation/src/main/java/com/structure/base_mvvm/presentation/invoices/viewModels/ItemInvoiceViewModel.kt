package com.structure.base_mvvm.presentation.invoices.viewModels

import android.util.Log
import android.view.View
import androidx.navigation.findNavController
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.invoice.InvoiceFragmentDirections
import com.structure.base_mvvm.presentation.invoices.InvoicesFragmentDirections

class ItemInvoiceViewModel  constructor(val model: InvoiceItem,val position:Int) : BaseViewModel(){
  private val TAG = "ItemInvoiceViewModel"
  fun submit(){
    Log.d(TAG, "submit: position $position")
  }

  fun print(view: View) {
    view.findNavController().navigate(
      InvoicesFragmentDirections.actionInvoicesFragmentToPreviewInvoiceFragment(
        model.pdf.orEmpty()
      )
    )
  }
}