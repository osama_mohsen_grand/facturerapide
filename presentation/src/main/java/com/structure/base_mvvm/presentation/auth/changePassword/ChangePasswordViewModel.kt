package com.structure.base_mvvm.presentation.auth.changePassword

import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.auth.entity.request.ChangePasswordRequest
import com.structure.base_mvvm.domain.auth.entity.request.ForgetPasswordRequest
import com.structure.base_mvvm.domain.auth.use_case.ChangePasswordUseCase
import com.structure.base_mvvm.domain.auth.use_case.ForgetPasswordUseCase
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(private val useCase: ChangePasswordUseCase,
                                                  private val accountRepository: AccountRepository,
                                                  val checkLoggedInUserUseCase: CheckLoggedInUserUseCase) :
  BaseViewModel() {
  var request = ChangePasswordRequest()

  init {
      if(!checkLoggedInUserUseCase.invoke())
        accountRepository.clearUser()
  }

  fun submit() {
    viewModelScope.launch(job){
      useCase(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}