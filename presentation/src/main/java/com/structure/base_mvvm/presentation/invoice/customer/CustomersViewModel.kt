package com.structure.base_mvvm.presentation.invoice.customer

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.account.use_case.GetUserFromLocalUseCase
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.customer.CustomersPaginateData
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.invoice.usecase.InvoiceUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CustomersViewModel @Inject constructor(private val invoiceUseCase: InvoiceUseCase
) : BaseViewModel() {

  var request = InvoiceRequest()

  @Bindable
  var page: Int = 0

  @Bindable
  var callingService = false

  var isLast = false

  @Bindable
  var adapter = CustomerAdapter()


  val responseCustomers =
    MutableStateFlow<Resource<BaseResponse<CustomersPaginateData>>>(Resource.Default)

  init {
    callService()
  }

  fun callService() {
    if (!callingService && !isLast) {
      notifyPropertyChanged(BR.showBar)
      callingService = true
      notifyPropertyChanged(BR.callingService)
      viewModelScope.launch(job) {
        page++
        if(page > 1){
          notifyPropertyChanged(BR.page)
        }
        invoiceUseCase.getCustomers(page)
          .catch { exception -> validationException.value = exception.message?.toInt() }
          .onEach { result ->
            responseCustomers.value = result
          }.launchIn(viewModelScope)
      }
    }
  }

  fun setData(data: CustomersPaginateData?) {
    data?.let {
      println("size:" + data.list.size)
      isLast = data.links.next == null
      if (page == 1) {
//        adapter = InvoicesAdapter()
        adapter.differ.submitList(it.list)
      } else {
        adapter.insertData(it.list)
      }
      notifyPropertyChanged(BR.adapter)
      callingService = false
      notifyPropertyChanged(BR.callingService)
    }
  }

  fun addCustomer(){
    clickEvent.value = Constants.ADD_CUSTOMER
  }

  fun selectCustomer(position: Int){
    if(position < adapter.differ.currentList.size) {
      request.name = adapter.differ.currentList[position].name.orEmpty()
      request.postalCode = adapter.differ.currentList[position].postalCode.orEmpty()
      request.address = adapter.differ.currentList[position].address.orEmpty()
      clickEvent.value = Constants.DISMISS
    }
  }

  fun addCustomerSubmit() {
    if (request.name.isEmpty()) {
      validationException.value = AuthValidation.EMPTY_NAME.value
      return
    }
//    if (request.email.trim().isNotEmpty() && !request.email.isValidEmail()) {
//      validationException.value = AuthValidation.INVALID_EMAIL.value
//      return
//    }
    clickEvent.value = Constants.DISMISS
    notifyPropertyChanged(BR.request)
  }
}