package com.structure.base_mvvm.presentation.auth.packages

import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.presentation.base.extensions.showMessageSuccess
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.PackagesFragmentBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.android.billingclient.api.*
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.collect.ImmutableList
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.extensions.showMessage
import com.structure.base_mvvm.presentation.databinding.CustomerFragmentBinding
import com.structure.base_mvvm.presentation.databinding.SubscribeSuccessFragmentBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject


@AndroidEntryPoint
class PackagesFragment : BaseFragment<PackagesFragmentBinding>(), IPriceSubmit {
  private val TAG = "PackagesFragment"
  private val viewModel: PackagesViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.packages_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    val list = ArrayList<Package>().also {
      it.add(Package(id = 1, title = getString(R.string.ultimated_invoices)))
      it.add(Package(id = 2, title = getString(R.string.share_invoices)))
      it.add(Package(id = 3, title = getString(R.string.print_invoices)))
      it.add(Package(id = 4, title = getString(R.string.convert_invoices_type)))
    }
    viewModel.adapter.differ.submitList(list)
    viewModel.adapterPrices.iPrintSubmit = this
    pay()
  }

  lateinit var billingClient: BillingClient

  fun pay() {
    Log.d(TAG, "pay: pay")
    billingClient = BillingClient.newBuilder(requireContext())
      .enablePendingPurchases()
      .setListener(
        object : PurchasesUpdatedListener {
          override fun onPurchasesUpdated(
            billingResult: BillingResult,
            list: MutableList<Purchase>?
          ) {
            Log.d(TAG, "onPurchasesUpdated: here")
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && list != null) {
              Log.d(TAG, "onPurchasesUpdated: list ${list.size}")
              for (purchase in list) {
                verifySubPurchase(purchase)
              }
            }
          }

        }
      ).build()

    //start the connection after initializing the billing client

    //start the connection after initializing the billing client
    establishConnection()
  }


  fun establishConnection() {
    Log.d(TAG, "establishConnection: ")
    billingClient.startConnection(object : BillingClientStateListener {
      override fun onBillingSetupFinished(billingResult: BillingResult) {
        Log.d(TAG, "onBillingSetupFinished: ${billingResult.responseCode}")
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
          // The BillingClient is ready. You can query purchases here.
          showProducts()
        }
      }

      override fun onBillingServiceDisconnected() {
        // Try to restart the connection on the next request to
        // Google Play by calling the startConnection() method.
        establishConnection()
      }
    })
  }

  fun showProducts() {
    Log.d(TAG, "showProducts: ")
    val productList = ImmutableList.of(
      //Product 1
      QueryProductDetailsParams.Product.newBuilder()
        .setProductId("00001")
        .setProductType(BillingClient.ProductType.SUBS)
        .build(),  //Product 2
      QueryProductDetailsParams.Product.newBuilder()
        .setProductId("00002")
        .setProductType(BillingClient.ProductType.SUBS)
        .build(),  //Product 3
//      QueryProductDetailsParams.Product.newBuilder()
//        .setProductId("one_year")
//        .setProductType(BillingClient.ProductType.SUBS)
//        .build()
    )

    val params = QueryProductDetailsParams.newBuilder()
      .setProductList(productList)
      .build()

    billingClient.queryProductDetailsAsync(
      params
    ) { billingResult: BillingResult?, prodDetailsList: List<ProductDetails?>? ->
      // Process the result
      Log.d(TAG, "showProducts: ${billingResult?.responseCode}")
      Log.d(TAG, "showProducts: ${prodDetailsList?.size}")
      prodDetailsList?.let { viewModel.updateGooglePrices(requireContext(), it) }
//      prodDetailsList?.forEach {
//        it?.let { productDetails ->
//          launchPurchaseFlow(productDetails)
//        }
//        Log.d(TAG, "showProducts: ${it?.productId} ${it?.productType} ${it?.name} , ${it?.description} , ${it?.oneTimePurchaseOfferDetails} , ${it?.subscriptionOfferDetails}")
//      }
    }
  }

  fun launchPurchaseFlow(productDetails: ProductDetails) {
    productDetails.subscriptionOfferDetails?.forEach {
//      Log.d(TAG, "launchPurchaseFlow: ${it.pricingPhases.pricingPhaseList.size} , ${it.installmentPlanDetails} , ${it.offerToken} , ${it.offerTags}")
      it.pricingPhases.pricingPhaseList.forEach { pricingPhase ->
        Log.d(
          TAG,
          "launchPurchaseFlow: ${pricingPhase.formattedPrice} , ${pricingPhase.billingPeriod} , ${pricingPhase.billingCycleCount} , ${pricingPhase.priceAmountMicros} , ${pricingPhase.priceCurrencyCode} , ${pricingPhase.recurrenceMode}"
        )
      }
    }
    assert(productDetails.subscriptionOfferDetails != null)
    val productDetailsParamsList = ImmutableList.of(
      BillingFlowParams.ProductDetailsParams.newBuilder()
        .setProductDetails(productDetails)
        .setOfferToken(productDetails.subscriptionOfferDetails!![0].offerToken)
        .build()
    )
    val billingFlowParams = BillingFlowParams.newBuilder()
      .setProductDetailsParamsList(productDetailsParamsList)
      .build()

    val billingResult = billingClient.launchBillingFlow(requireActivity(), billingFlowParams)

    Log.d(TAG, "launchPurchaseFlow: ${billingResult.responseCode}")
  }

  fun verifySubPurchase(purchases: Purchase) {
    Log.d(TAG, "verifySubPurchase: purchase")
    val acknowledgePurchaseParams = AcknowledgePurchaseParams
      .newBuilder()
      .setPurchaseToken(purchases.purchaseToken)
      .build()
    billingClient.acknowledgePurchase(
      acknowledgePurchaseParams
    ) { billingResult: BillingResult ->
      Log.d(TAG, "verifySubPurchase: ${billingResult.responseCode}")
      if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
        //call eman hear
        Log.d(TAG, "verifySubPurchase: DONE HERE PAY SUCCESS")
        viewModel.updateSubscribe()
      }
    }
    Log.d(TAG, "Purchase Token: " + purchases.purchaseToken)
    Log.d(TAG, "Purchase Time: " + purchases.purchaseTime)
    Log.d(TAG, "Purchase OrderID: " + purchases.orderId)
  }


  override
  fun setupObservers() {


    viewModel.clickEvent.observe(this) {
      if (it == Constants.SUBMIT) {
        if (viewModel.packageSelected == null) {
          showMessage(getString(R.string.please_select) + " " + getString(R.string.subscription_packages))
        } else if (viewModel.packageSelected!!.productDetails < viewModel.productDetailsList.size) {
          viewModel.adapterPrices.updateSelected(viewModel.packageSelected!!)
          launchPurchaseFlow(viewModel.productDetailsList[viewModel.packageSelected!!.productDetails])
        }
      }

    }

    lifecycleScope.launchWhenResumed {
      viewModel.packagesResponse.collect {
        println("collection here $it")
        handleLoading(it)
        if (it is Resource.Success) {
          println("collection success ${it.value.message}")
          val packagesResponse = it.value.data
          viewModel.updatePackages(packagesResponse)
        }
      }
    }
    lifecycleScope.launchWhenCreated {
      viewModel.response.collect {
        println("collection is $it")
        handleLoading(it)
        if (it is Resource.Success) {
          subscribeSuccess(it.value.message)
        }
      }
    }
  }

  private lateinit var dialog: BottomSheetDialog

  private fun subscribeSuccess(message: String) {
    dialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogStyle)


    val binding: SubscribeSuccessFragmentBinding = DataBindingUtil.inflate(
      LayoutInflater.from(requireContext()),
      com.structure.base_mvvm.presentation.R.layout.subscribe_success_fragment,
      null,
      false
    )
    binding.success.text = message
    dialog.setContentView(binding.root)
    dialog.setCancelable(false)
    dialog.show()
    lifecycleScope.launch(Main) {
      delay(1500L)
      dialog.dismiss()
      navigateSafe(PackagesFragmentDirections.actionPackagesFragmentToHomeFragment())
    }
  }


  override fun onResume() {
    super.onResume()
    if (this::billingClient.isInitialized) {
      billingClient.queryPurchasesAsync(
        QueryPurchasesParams.newBuilder().setProductType(BillingClient.ProductType.SUBS).build()
      ) { billingResult: BillingResult, list: List<Purchase> ->
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
          Toast.makeText(requireContext(), "OK ON RESUME", Toast.LENGTH_SHORT).show()
          for (purchase in list) {
            if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED && !purchase.isAcknowledged) {
              verifySubPurchase(purchase)
            }
          }
        }
      }
    }
  }

  override fun submit(position: Int, model: Package) {
    viewModel.packageSelected = model

    viewModel.adapterPrices.setPosition(position)
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}