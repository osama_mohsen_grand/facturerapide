package com.structure.base_mvvm.presentation.intro.intro

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.use_case.SettingsUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class IntroViewModel @Inject constructor(val generalUseCases: GeneralUseCases,
                                         private val settingsUseCase: SettingsUseCase) : BaseViewModel() {

  private  val TAG = "IntroViewModel"
  val openLogIn = SingleLiveEvent<Void>()
  val responseResult = MutableStateFlow<Resource<BaseResponse<IntroModel>>>(Resource.Default)
  var clickEventSubmit: SingleLiveEvent<String> = SingleLiveEvent()

  var intro : IntroModel = IntroModel()
  init {
    Log.d(TAG, "introViewModel")
  }

  fun callIntroApi() {
    Log.d(TAG, "callIntroApi: ")
    viewModelScope.launch(job) {
      settingsUseCase.welcome()
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          responseResult.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun welcome() {
    clickEventSubmit.value = Constants.AUTH
  }

  fun setFirstTime(isFirstTime: Boolean) = generalUseCases.setFirstTimeUseCase(isFirstTime)
  fun setValue(value: IntroModel) {
    Log.d(TAG, "setValue: ${value.image}")
    this.intro = value
    notifyChange()
  }
}