package com.structure.base_mvvm.presentation.language

import androidx.fragment.app.viewModels
import com.structure.base_mvvm.domain.language.Language
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.FragmentLanguageBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
import com.structure.base_mvvm.presentation.language.viewModels.LanguageViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LanguageFragment : BaseFragment<FragmentLanguageBinding>() {

  val viewModel: LanguageViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_language

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.selectedLanguage(LanguagesHelper.getCurrentLanguage(requireContext()))
    val list = ArrayList<Language>()
    list.add(Language(id=1,getString(R.string.Français),abbreviation=Constants.LANGUAGE_FRENCH))
    list.add(Language(id=2,getString(R.string.english),abbreviation=Constants.LANGUAGE_ENGLISH))
    list.add(Language(id=3,getString(R.string.español),abbreviation=Constants.LANGUAGE_SPANISH))
    viewModel.adapter.differ.submitList(list)
  }

  override fun setupObservers() {
    super.setupObservers()
    viewModel.clickEvent.observe(viewLifecycleOwner){
      if(it == Constants.LANGUAGE) openActivityAndClearStack(HomeActivity::class.java)
    }
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }


}