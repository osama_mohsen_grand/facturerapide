package com.structure.base_mvvm.presentation.more.support

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.backToPreviousScreen
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.extensions.showMessage
import com.structure.base_mvvm.presentation.base.extensions.showMessageSuccess
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.SupportFragmentBinding
import com.structure.base_mvvm.presentation.more.info.InfoFragmentArgs
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect



@AndroidEntryPoint
class SupportFragment : BaseFragment<SupportFragmentBinding>() {

  private val args: SupportFragmentArgs by navArgs()

  private val viewModel: SupportViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.support_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.type = args.type
  }

  private val TAG = "SupportFragment"

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      Log.d(TAG, "event $it")
      when (it) {
        Constants.LOGIN -> backToPreviousScreen()
        Constants.PICKER_IMAGE -> {
          singleTedBottomPicker(requireActivity())
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          viewModel.reset()
        }
      }
    }
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}