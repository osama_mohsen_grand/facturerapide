package com.structure.base_mvvm.presentation.invoice.customer

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.customer.CustomerModel
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.domain.settings.models.NotificationData
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ItemCustomerBinding
import com.structure.base_mvvm.presentation.databinding.ItemInvoiceBinding
import com.structure.base_mvvm.presentation.databinding.ItemNotificationBinding
import com.structure.base_mvvm.presentation.home.viewModels.ItemNotificationViewModel
import com.structure.base_mvvm.presentation.invoices.viewModels.ItemInvoiceViewModel

class CustomerAdapter: RecyclerView.Adapter<CustomerAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<CustomerModel> = SingleLiveEvent()
  private val differCallback = object : DiffUtil.ItemCallback<CustomerModel>() {
    override fun areItemsTheSame(oldItem: CustomerModel, newItem: CustomerModel): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CustomerModel, newItem: CustomerModel): Boolean {
      return oldItem == newItem
    }

  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_customer, parent, false)
    return ViewHolder(view)
  }


  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemCustomerViewModel(data,position)
    holder.setViewModel(itemViewModel)
  }

  private val TAG = "NotificationAdapter"
  fun insertData(insertList: List<CustomerModel>) {
    val array = ArrayList<CustomerModel>(differ.currentList)
    val size = array.size
    array.addAll(insertList)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }

  fun removeItem(i: Int){
    val array = ArrayList(differ.currentList)
    array.remove(array[i])
    differ.submitList(array)
    notifyItemRemoved(i)
  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemCustomerBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemCustomerViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }


}