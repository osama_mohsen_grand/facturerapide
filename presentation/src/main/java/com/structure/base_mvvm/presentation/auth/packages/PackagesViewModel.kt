package com.structure.base_mvvm.presentation.auth.packages

import android.content.Context
import android.util.Log
import androidx.databinding.Bindable
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.viewModelScope
import com.android.billingclient.api.ProductDetails
import com.structure.base_mvvm.domain.auth.entity.model.PackageGooglePayRequest
import com.structure.base_mvvm.domain.auth.entity.model.PackageRequest
//import app.facturerapide.core.utils.Utils.getDeviceIdWithoutPermission
import com.structure.base_mvvm.domain.auth.use_case.PackagesUseCase
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.extensions.showError
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class PackagesViewModel @Inject constructor(
  private val useCase: PackagesUseCase
) : BaseViewModel() {
  val packagesResponse = MutableStateFlow<Resource<BaseResponse<List<Package>>>>(Resource.Default)
  var packageSelected: Package? = null

  //  val adapter = PackagesAdapter()
  @Bindable
  val adapter = PackagesBenefitAdapter()

  @Bindable
  val adapterPrices = PackagesPricesAdapter()
  val request = PackageRequest()
  val showPage = ObservableBoolean(false)

  //  private val loginResponse = MutableStateFlow<Resource<BaseResponse<User>>>(Resource.Default)
  init {
//    getPackages()
//    adapterPrices.list.add(Package(id=1,title="osama", description = "mohsen"))
  }

  fun addPackagesBenefit() {

  }

  private val TAG = "PackagesViewModel"
  private fun getPackages() {
    Log.d(TAG, "getPackages: start")
    viewModelScope.launch(job) {
      Log.d(TAG, "getPackages: HI")
      useCase.getPackages()
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          Log.d(TAG, "getPackages working: ")
          packagesResponse.value = result

//          val packagesResponse = it.value.data?.packages

        }.launchIn(viewModelScope)
    }
  }

  fun choose() {
    clickEvent.value = Constants.SUBMIT
  }

  fun updateSubscribe() {

    val request = PackageGooglePayRequest()
    packageSelected?.let {
      request.apply {
        packageId = it.packageId
        packageName = it.packageId
        durationType = it.durationType
        paid = it.price.toString()
      }
    }
    viewModelScope.launch(job) {
      useCase.setPackage(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun updatePackages(packagesResponse: List<Package>?) {
    adapter.differ.submitList(packagesResponse)
    notifyChange()
  }

  val productDetailsList = ArrayList<ProductDetails>()
  var decfor = DecimalFormat("0.00", DecimalFormatSymbols(Locale.ENGLISH))

  fun updateGooglePrices(
	  context: Context,
	  listProductDetails: List<ProductDetails?>,
	  errorAction: () -> Unit = {}
	) {


    productDetailsList.clear()
    val list = ArrayList<Package>()
    var counter = 1
    listProductDetails.forEach { productDetails ->
//      Log.d(TAG, "updateGooglePrices: ${productDetails?.name}")
      productDetails?.let {
        productDetailsList.add(it)
      }
      Log.d(
        TAG,
        "updateGooglePrices: ${productDetails?.oneTimePurchaseOfferDetails?.formattedPrice}"
      )
      Log.d(
        TAG,
        "updateGooglePrices: ${productDetails?.oneTimePurchaseOfferDetails?.priceCurrencyCode}"
      )
      productDetails?.subscriptionOfferDetails?.forEachIndexed { index, subscriptionOfferDetails ->
        subscriptionOfferDetails.pricingPhases.pricingPhaseList.forEach { prices ->

          if (prices.formattedPrice.indexOf(prices.priceCurrencyCode) != -1) {
//            val price = prices.formattedPrice.
            try {
              val price = prices.formattedPrice.removePrefix(prices.priceCurrencyCode).trim()
              Log.d(TAG, "updateGooglePrices:$price, ${price.length}")
              val priceValue = price.toDouble()
              val title = when (counter) {
                1 -> "$price ${prices.priceCurrencyCode}/ ${context.getString(R.string.monthly)}"
                else -> "$price ${prices.priceCurrencyCode}/ ${context.getString(R.string.yearly)}"
              }
              val description = when (counter) {
                1 -> ""
                else -> "${decfor.format((priceValue / 12))} ${prices.priceCurrencyCode} / ${
                  context.getString(
                    R.string.month_billed_annually
                  )
                }"
              }
              val packageId = when(counter){
                1 -> "mois"
                else -> "annuel"
              }
              val durationType = when(counter){
                1 -> "month"
                else -> "year"
              }
              list.add(
                Package(
                  id = counter,
                  title = title,
                  description = description,
                  packageId = packageId,
                  price = priceValue,
                  durationType = durationType,
                  productDetails = productDetailsList.size - 1
                )
              )
              counter++
            } catch (e: Exception) {
              Log.d(TAG, "updateGooglePricesEx: ${e.printStackTrace()}")
              e.printStackTrace()
            }
          }
//          Log.d(TAG, "updateGooglePricesC: ${prices.priceCurrencyCode}")
//          Log.d(TAG, "updateGooglePricesC: ${prices.billingCycleCount}")
//          Log.d(TAG, "updateGooglePricesC: ${prices.billingPeriod}")
//          val price = prices.formattedPrice.split(" ")
//          Log.d(TAG, "updateGooglePrices: ${price.size}")
//          price.forEach {
//            Log.d(TAG, "updateGooglePrices: $it")
//          }
//          Log.d(TAG, "updateGooglePrices: ------------------------")


        }
      }
    }
    Log.d(TAG, "updateGooglePrices: ${list.size}")
	  if (list.isEmpty()) {
		  errorAction()
	  }
    adapterPrices.insert(list)
    this.showPage.set(true)

  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}