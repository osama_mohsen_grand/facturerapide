package com.structure.base_mvvm.presentation.invoice.copy

import android.view.View
import android.widget.RatingBar
import androidx.databinding.Bindable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CopyDialogViewModel @Inject constructor(
) : BaseViewModel() {
  var type = -1
  fun selectTYpe(type: Int){
    this.type = type
    clickEvent.value = Constants.DISMISS
  }
}