package com.structure.base_mvvm.presentation.intro.splash

import android.content.Intent
import android.os.Looper
import android.util.Log
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.structure.base_mvvm.domain.account.use_case.AccountUseCases
import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.logging.Handler
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
  val accountUseCases: AccountUseCases, val generalUseCases: GeneralUseCases,
  val saveUserToLocalUseCase: SaveUserToLocalUseCase,
  val checkLoggedInUserUseCase: CheckLoggedInUserUseCase
) : BaseViewModel() {

  private val _logOuResponse = MutableStateFlow<Resource<BaseResponse<Boolean>>>(Resource.Default)
  val logOutResponse = _logOuResponse
  var clickEventPressed: SingleLiveEvent<String> = SingleLiveEvent()


  fun setupFirebaseToken() {
    Log.d(TAG, "setupFirebaseToken: ")
    if (generalUseCases.checkFirstTimeUseCase.invoke())
      clickEventPressed.value = Constants.WELCOME
    else if (!checkLoggedInUserUseCase.invoke()) {
      clickEventPressed.value = Constants.AUTH
    } else {
      val user = accountUseCases.user.accountRepository.getUserLocal()
      println("steps needed ${user.register_steps}")
      clickEventPressed.value = when (user.register_steps) {
        1 -> Constants.COMPANY_REGISTER
        else -> Constants.HOME
      }
    }

    //shared perereference
  }

  private val TAG = "SplashViewModel"

  init {
//    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
//      if (!task.isSuccessful) {
//        return@OnCompleteListener
//      }
//      var result = task.result
//      Log.d(TAG, "setupFirebaseToken: $result")
//    })
  }
}