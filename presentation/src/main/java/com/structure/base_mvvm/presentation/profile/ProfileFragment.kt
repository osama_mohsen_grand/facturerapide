package com.structure.base_mvvm.presentation.profile

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.sign_up.basic.SignUpViewModel
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.FragmentSignUpBinding
import com.structure.base_mvvm.presentation.databinding.ProfileFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ProfileFragment : BaseFragment<ProfileFragmentBinding>() {

  private val viewModel: SignUpViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.profile_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    binding.imgLoginLogo.loadCircleImage(viewModel.userUseCase.accountRepository.getUserLocal().image,null)
  }

  private val TAG = "ProfileFragment"

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      Log.d(TAG, "event $it")
      when (it) {
        Constants.LOGIN -> backToPreviousScreen()
        Constants.PICKER_IMAGE -> {
          singleTedBottomPicker(requireActivity())
        }
        Constants.CHANGE_PASSWORD ->{
          navigateSafe(ProfileFragmentDirections.actionProfileFragmentToChangePasswordFragment())
        }
      }
    }
    selectedImages.observeForever {
      selectedImages.value?.path?.let { it1 ->
        run {
          viewModel.request.setImage(it1,Constants.IMAGE)
          viewModel.notifyChange()
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.responseUser.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          backToPreviousScreen()
        }
      }
    }
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}