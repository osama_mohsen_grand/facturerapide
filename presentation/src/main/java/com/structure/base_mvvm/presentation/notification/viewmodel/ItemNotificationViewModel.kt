package com.structure.base_mvvm.presentation.notification.viewmodel

import com.structure.base_mvvm.domain.settings.models.NotificationData
import com.structure.base_mvvm.presentation.base.BaseViewModel

class ItemNotificationViewModel  constructor(val model: NotificationData) : BaseViewModel()