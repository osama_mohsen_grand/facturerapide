package com.structure.base_mvvm.presentation.invoice

import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.databinding.Bindable
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.structure.base_mvvm.domain.account.use_case.GetUserFromLocalUseCase
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.invoice.model.InvoiceDetailsResponse
import com.structure.base_mvvm.domain.invoice.request.InvoiceRequest
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.domain.invoice.usecase.InvoiceUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.domain.utils.isValidEmail
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.invoice.service.ServicesAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

@HiltViewModel
class InvoiceViewModel @Inject constructor(
  private val userUseCase: GetUserFromLocalUseCase,
  val invoiceUseCase: InvoiceUseCase
) : BaseViewModel() {

  val responseInvoiceDetails =
    MutableStateFlow<Resource<BaseResponse<InvoiceDetailsResponse>>>(Resource.Default)
  val responseUpdate = MutableStateFlow<Resource<BaseResponse<*>>>(Resource.Default)
  val responseDeleted = MutableStateFlow<Resource<BaseResponse<*>>>(Resource.Default)

  @Bindable
  var request = InvoiceRequest()

  @Bindable
  var showPage: Boolean = true

  private val TAG = "CompanyViewModel"

  var name: String = ""
  @Bindable
  var user = userUseCase.invoke()

  @Bindable
  var id = -1

  val isUpdate = MutableLiveData(false)

  //  var serviceIndex = 0
  @Bindable
  val adapter = ServicesAdapter()

  fun setArgs(type: Int, id: Int, name: String) {
    Log.d(TAG, "setArgs: ${user.toString()}")
    this.request.invoiceId = type
    this.name = name
    this.id = id
    if (id != -1) {
      isUpdate.value = true
      notifyPropertyChanged(BR.id)
      showPage = false
      notifyPropertyChanged(BR.showPage)
      getInvoiceDetails()
    }
    adapter.showDelete = true
  }

  fun addCustomer(v: View) {
    v.findNavController().navigate(InvoiceFragmentDirections.actionInvoiceFragmentToCustomersFragment())
  }

  fun addService(v: View) {
    v.findNavController().navigate(InvoiceFragmentDirections.actionInvoiceFragmentToServicesFragment())
  }

  infix fun Double.toDecimalClean(value: Int) : BigDecimal{
    return total.toBigDecimal().setScale(value, RoundingMode.HALF_EVEN)
  }

  var total: Double = 0.0
  private fun calculate() {
    total = 0.0
    for (service in request.services) {
      total += service.price.toInt() * service.quantity.toInt()
    }
    request.subTotal = total.toDecimalClean(2).toString()
    if (request.tax.isNotEmpty()) {
      total += (request.tax.toDouble() * total) / 100
    }
    request.total = total.toDecimalClean(2).toString()
    Log.d(TAG, "calculate: " + request.total)
    Log.d(TAG, "calculate: " + request.subTotal)
    Log.d(TAG, "calculate: " + request.tax)
    notifyPropertyChanged(BR.request)
  }

  var time = ""
  fun dateDialog() {
    Log.e(TAG, "setupObserversDateDialog: ", )

    clickEvent.value = Constants.DATE_TIME
  }


  fun notifyAdapter() {
    adapter.differ.submitList(request.services)
    notifyPropertyChanged(BR.adapter)
  }

  fun getInvoiceDetails() {
    viewModelScope.launch(job) {
      invoiceUseCase.getInvoiceDetails(id)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          responseInvoiceDetails.value = result
//          responseInvoiceDetails.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun createInvoice() {
    viewModelScope.launch(job) {
      Log.d(TAG, "createInvoice_start_check: ")
      when (id) {
        -1 -> {
          Log.d(TAG, "createInvoice_start_-1: ")
          Log.d(TAG, "createInvoice_tax: " + request.tax)
          Log.d(TAG, "createInvoice_notes: " + request.notes)
          invoiceUseCase.submit(request)
            .catch { exception -> validationException.value = exception.message?.toInt() }
            .onEach { result ->
              response.value = result
            }.launchIn(viewModelScope)
        }
        else -> {
          invoiceUseCase.update(id, request)
            .catch { exception -> validationException.value = exception.message?.toInt() }
            .onEach { result ->
              responseUpdate.value = result
            }.launchIn(viewModelScope)

        }
      }
    }
  }


  fun previewInvoice() {
    clickEvent.value = Constants.PREVIEW
  }

  fun copyInvoice(v: View) {
    v.findNavController().navigate(InvoiceFragmentDirections.actionInvoiceFragmentToCopyOrPasteDialog())
  }


  fun deleteInvoice() {
    viewModelScope.launch(job) {
      invoiceUseCase.deleteInvoice(id)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          responseDeleted.value = result
        }.launchIn(viewModelScope)
    }
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }

  lateinit var data: InvoiceDetailsResponse
  fun setInvoiceDetails(data: InvoiceDetailsResponse?) {
    data?.let {
      it
      this.data = it
      data.company.name?.let {
        user.company.name = data.company.name
      }
      data.company.email?.let {
        user.company.email = data.company.email
      }
      data.company.phone?.let {
        user.company.phone = data.company.phone
      }
      data.company.address?.let {
        user.company.address = data.company.address
      }
//      user.company.email = data.company.email
//      user.company.phone = data.company.phone
//      user.company.address = data.company.address
      it.customerAddress?.let{
        request.address = it
      }
      it.customerEmail?.let{
        request.email = it
      }
      it.customerName?.let{
        request.name = it
      }
      it.customerPostalCode?.let{
        request.postalCode = it
      }
      it.invoiceNumber?.let{
        request.number = it
      }
//      request.email = it.customerEmail
//      request.name = it.customerName
//      request.postalCode = it.customerPostalCode
      request.tax = it.tax.toString()
      request.total = it.total.toString()
      request.subTotal = it.subtotal.toString()
      request.services.addAll(it.services)
      request.date = it.invoiceCreatedAt.toString()
      request.notes = it.notes
      request.notes2 = if (it.notes2 == null) "" else it.notes2.toString()
      showPage = true
      notifyPropertyChanged(BR.user)
      notifyPropertyChanged(BR.showPage)
      notifyAdapter()
      notifyPropertyChanged(BR.request)
    }
  }

  fun setCustomer(it: InvoiceRequest?) {
    request.name = it?.name.toString()
    request.address = it?.address.toString()
    request.postalCode = it?.postalCode.toString()
    notifyPropertyChanged(BR.request)
  }

  fun calculateData() {
    calculate()
//    notifyAdapter()
  }

  fun insertData(it: InvoiceServiceModel) {
    adapter.insertData(it)
    request.services.add(it)
    notifyPropertyChanged(BR.adapter)
  }

  fun updateService(invoiceServiceModel: InvoiceServiceModel, position: Int) {
    adapter.update(invoiceServiceModel,position)
    if(position < request.services.size) {
      request.services[position] = invoiceServiceModel
      calculateData()
    }
    notifyPropertyChanged(BR.adapter)
  }

}