package com.structure.base_mvvm.presentation.more.more

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.entity.request.RegisterRequest
import com.structure.base_mvvm.domain.auth.use_case.RegisterUseCase
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoreViewModel @Inject constructor() : BaseViewModel() {

  var request = RegisterRequest()

  private val TAG = "MoreViewModel"

  fun navigate(type: String){
    clickEvent.value = type
  }
  fun shareApp(){
    clickEvent.value = Constants.SHARE

  }
  fun rateApp(){
    clickEvent.value = Constants.RATE_APP
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}