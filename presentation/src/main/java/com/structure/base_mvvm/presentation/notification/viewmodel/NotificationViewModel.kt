package com.structure.base_mvvm.presentation.notification.viewmodel

import android.util.Log
import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.invoice.repository.InvoiceRepository
import com.structure.base_mvvm.domain.invoice.usecase.InvoiceUseCase
import com.structure.base_mvvm.domain.settings.models.NotificationData
import com.structure.base_mvvm.domain.settings.models.NotificationPaginateData
import com.structure.base_mvvm.domain.settings.repository.SettingsRepository
import com.structure.base_mvvm.domain.settings.use_case.SettingsUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.invoices.adapter.InvoicesAdapter
import com.structure.base_mvvm.presentation.notification.adapter.NotificationAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
  private val useCase: SettingsUseCase
) : BaseViewModel() {
  @Bindable
  var type: Int = 1
  @Bindable
  var page: Int = 0
  @Bindable
  var callingService = false
  var isLast = false

  @Bindable
  var adapter = NotificationAdapter()

  val responseNotifications =
    MutableStateFlow<Resource<BaseResponse<NotificationPaginateData>>>(Resource.Default)

  init {
    getNotifications()
  }

  private val TAG = "PackagesViewModel"
  fun getNotifications() {
    if (!callingService && !isLast) {
      callingService = true
      notifyPropertyChanged(BR.callingService)
      viewModelScope.launch(job) {
        page++
        if(page > 1){
          notifyPropertyChanged(BR.page)
        }
        useCase.notifications( page)
          .catch { exception -> validationException.value = exception.message?.toInt() }
          .onEach { result ->
            responseNotifications.value = result
          }.launchIn(viewModelScope)
      }
    }
  }

  lateinit var result: NotificationPaginateData
  fun setData(data: NotificationPaginateData?) {
    data?.let {
      result = data
      println("size:" + data.list.size)
      isLast = data.links.next == null
      if (page == 1) {
//        adapter = InvoicesAdapter()
        adapter.differ.submitList(it.list)
      } else {
        adapter.insertData(it.list)
      }
      notifyPropertyChanged(BR.adapter)
      callingService = false
      notifyPropertyChanged(BR.callingService)
    }
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }

  fun remove(notificationData: NotificationData?) {
    notificationData?.let {
      useCase.deleteNotification( it.id)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }
}