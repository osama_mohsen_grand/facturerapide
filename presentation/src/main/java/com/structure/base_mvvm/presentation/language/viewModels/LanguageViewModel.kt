package com.structure.base_mvvm.presentation.language.viewModels

import android.content.Context
import android.view.View
import androidx.databinding.Bindable
import com.structure.base_mvvm.domain.account.use_case.AccountUseCases
import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.general.use_case.GeneralUseCases
import com.structure.base_mvvm.domain.language.Language
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.BR
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.presentation.language.LanguageAdapter
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LanguageViewModel @Inject constructor() : BaseViewModel() {

  @Bindable
  val adapter: LanguageAdapter = LanguageAdapter()

  init {
  }

  fun updateLanguage(position: Int) {
    adapter.update(position)
    notifyPropertyChanged(BR.adapter)
  }

  fun selectedLanguage(currentLanguage: String?) {
    currentLanguage.let { language ->
      adapter.selected = when (language) {
        Constants.LANGUAGE_FRENCH -> 0
        Constants.LANGUAGE_ENGLISH -> 1
        Constants.LANGUAGE_SPANISH -> 2
        else -> 0
      }
    }
  }

  fun submit(context: Context,v: View){
    val lang = adapter.differ.currentList[adapter.selected].abbreviation
    LanguagesHelper.changeLanguage(context,lang)
    LanguagesHelper.setLanguage(context,lang)
    clickEvent.value = Constants.LANGUAGE
  }


}