package com.structure.base_mvvm.presentation.auth.confirmCode

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.entity.request.ConfirmCodeRequest
import com.structure.base_mvvm.domain.auth.entity.request.ForgetPasswordRequest
import com.structure.base_mvvm.domain.auth.use_case.ConfirmCodeUseCase
import com.structure.base_mvvm.domain.auth.use_case.ForgetPasswordUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConfirmCodeViewModel @Inject constructor(
  private val useCase: ConfirmCodeUseCase,
  private val forgetPasswordUseCase: ForgetPasswordUseCase
) : BaseViewModel() {
  private val TAG = "ConfirmViewModel"
  val responseConfirm = MutableStateFlow<Resource<BaseResponse<*>>>(Resource.Default)

  var text = ""
  var counter = 60
  val time = flow<Int> {
    while (counter > 0){
      delay(1000L)
      counter--
    }
  }

  init {
    viewModelScope.launch {
      time.collect {
        text = "$it:00"
      }
    }
  }


  val forgetPassword by lazy{
    ForgetPasswordRequest()
  }

  init {
    Log.d(TAG, "init Confirm code")
    startTimer()
  }
  var request = ConfirmCodeRequest()



  fun submit() {
    useCase.submit(request)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        response.value = result
      }
      .launchIn(viewModelScope)
  }

  fun resend() {
    //call api
    resend = false
    countDownTimer.start()
    forgetPassword.email = request.email
    forgetPasswordUseCase(forgetPassword)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        responseConfirm.value = result
      }
      .launchIn(viewModelScope)
  }


  var timerText = "60:00"
  var resend = false
  lateinit var countDownTimer: CountDownTimer
  private fun startTimer() {
    Log.d(TAG, "startTimer: ")
    countDownTimer = object : CountDownTimer(60000, 1000) {
      override fun onTick(millisUntilFinished: Long) {
        timerText = when {
          (millisUntilFinished / 1000) < 10 -> "0" + (millisUntilFinished / 1000)
          else -> (millisUntilFinished / 1000)
        }.toString().plus(": 00")
        notifyChange()
        Log.d(TAG, "onTick: $timerText")
      }

      override fun onFinish() {
        resend = true
        Log.d(TAG, "onFinish: resend")
        notifyChange()
      }
    }.start()
  }

  override fun onCleared() {
    Log.d(TAG, "onCleared: cancel")
    countDownTimer.cancel()
    super.onCleared()
  }



}