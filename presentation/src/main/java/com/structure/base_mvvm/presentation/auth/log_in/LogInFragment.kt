package com.structure.base_mvvm.presentation.auth.log_in

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.FailureStatus
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.FragmentLogInBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class LogInFragment : BaseFragment<FragmentLogInBinding>() {

  private val viewModel: LogInViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_log_in

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.device_token = getDeviceId(requireContext())
  }

  override fun setUpViews() {
    super.setUpViews()
  }

  private  val TAG = "LogInFragment"

  override
  fun setupObservers() {
    Log.d("asdasdasd sad dsadsa ", "setUpViews: "+arguments?.getString("first"))

    viewModel.clickEvent.observe(this) {
      if (Constants.FORGET_PASSWORD == it)
        openForgotPassword()
      else if (it == Constants.REGISTER)
        openSignUp()
      else if (it == Constants.HOME)
        startActivity(Intent(context, HomeActivity::class.java))
    }

    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        val bundle = Bundle()
        when (it) {
          is Resource.Success -> {
            openHome(bundle)
          }
          is Resource.Failure -> if (it.failureStatus == FailureStatus.VERIFY_CODE) openVerifyCode()
          is Resource.CompanyRegister -> openSignUpCompany()
          else -> {}
        }
      }
    }
  }

  private fun openForgotPassword() {
    navigateSafe(LogInFragmentDirections.actionOpenForgotPasswordFragment(Constants.RESET))
  }

  private fun openSignUp() {
    navigateSafe(LogInFragmentDirections.actionOpenSignUpFragment())
  }

  private fun openSignUpCompany() {
    navigateSafe(LogInFragmentDirections.actionLogInFragmentToSignUpCompanyFragment())
  }

//  private fun openPackages() {
//    navigateSafe(LogInFragmentDirections.actionLogInFragmentToPackagesFragment())
//  }

  private fun openVerifyCode() {
    navigateSafe(
      LogInFragmentDirections.actionLogInFragmentToFragmentConfirmCode(
        viewModel.request.email,
        Constants.VERIFY,
        Constants.LOGIN
      )
    )
  }

  private fun openHome(bundle: Bundle) {
    requireActivity().openActivityAndClearStack(HomeActivity::class.java,bundle)
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}