package com.structure.base_mvvm.presentation.auth.sign_up.basic

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.account.use_case.CheckLoggedInUserUseCase
import com.structure.base_mvvm.domain.account.use_case.GetUserFromLocalUseCase
import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
import com.structure.base_mvvm.domain.auth.entity.model.UserModel
import com.structure.base_mvvm.domain.auth.entity.request.RegisterRequest
import com.structure.base_mvvm.domain.auth.use_case.RegisterUseCase
import com.structure.base_mvvm.domain.invoice.model.InvoicePaginateData
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(private val useCase: RegisterUseCase,
                                          val userUseCase: GetUserFromLocalUseCase,
                                          val saveUserToLocalUseCase: SaveUserToLocalUseCase,
                                          val checkLoggedInUserUseCase: CheckLoggedInUserUseCase
) : BaseViewModel() {

  var request = RegisterRequest()

  val responseUser =
    MutableStateFlow<Resource<BaseResponse<UserModel>>>(Resource.Default)

  init {
    request.isLogin = checkLoggedInUserUseCase.invoke()
    if(request.isLogin){
      request.name = userUseCase.accountRepository.getUserLocal().name
      request.email = userUseCase.accountRepository.getUserLocal().email
    }
  }

  fun test(){}

  private val TAG = "SignUpViewModel"
  fun signup(){
    viewModelScope.launch(job){
      Log.d(TAG, "signup: "+request.password)
      useCase.submit(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun profileUpdate(){
    viewModelScope.launch(job){
      Log.d(TAG, "signup: "+request.password)
      useCase.profileUpdate(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          responseUser.value = result
        }.launchIn(viewModelScope)
    }

  }



  fun changePassword(){
    clickEvent.value = Constants.CHANGE_PASSWORD
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}