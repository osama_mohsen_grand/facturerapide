package com.structure.base_mvvm.presentation.invoice.copy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.databinding.CopyDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CopyDialog : BottomSheetDialogFragment() {
  private val viewModel: CopyDialogViewModel by viewModels()
  lateinit var binding: CopyDialogBinding
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    binding = DataBindingUtil.inflate(inflater, R.layout.copy_dialog, container, false)
    binding.viewModel = viewModel
    setupObserver()
    return binding.root
  }

  fun setupObserver(){
    viewModel.clickEvent.observe(this,{
      val n = findNavController()
      n.navigateUp()
      n.currentBackStackEntry?.savedStateHandle?.set(
        Constants.COPY,
        viewModel.type
      )
    })
  }

  override fun getTheme(): Int {
    return R.style.CustomBottomSheetDialogTheme;
  }

  override fun onDestroy() {
    super.onDestroy()
  }
}