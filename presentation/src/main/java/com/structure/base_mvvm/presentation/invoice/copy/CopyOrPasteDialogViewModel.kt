package com.structure.base_mvvm.presentation.invoice.copy

import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CopyOrPasteDialogViewModel @Inject constructor(
) : BaseViewModel() {
    var type = -1
    fun selectTYpe(type: Int){
        this.type = type
        clickEvent.value = Constants.DISMISS
    }
}