package com.structure.base_mvvm.presentation.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.structure.base_mvvm.domain.auth.enums.AuthValidation
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.hideLoadingDialog
import com.structure.base_mvvm.presentation.base.utils.showLoadingDialog
import com.structure.base_mvvm.presentation.base.utils.showNoApiErrorAlert
import gun0912.tedbottompicker.TedRxBottomPicker
import java.util.Locale

abstract class BaseFragment<VB : ViewDataBinding> : Fragment() {

  private var _binding: VB? = null
  open val binding get() = _binding!!
  private var mRootView: View? = null
  private var hasInitializedRootView = false
  private var progressDialog: Dialog? = null
  val selectedImages = SingleLiveEvent<Uri>()

  override
  fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    if (mRootView == null) {
      initViewBinding(inflater, container)
    }

    return mRootView
  }

  private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?) {
    _binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)

    mRootView = binding.root
    binding.lifecycleOwner = this
    binding.executePendingBindings()
    validationHandle()
  }

  private fun validationHandle() {
    validationObserver().observe(this) {
      Log.d(TAG, "validationHandle: $it")
      when (it) {
        AuthValidation.EMPTY_NAME.value -> {
          requireView().showSnackBar(resources.getString(R.string.empty_name))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.empty_name))
        }
        AuthValidation.EMPTY_EMAIL.value -> {
          requireView().showSnackBar(resources.getString(R.string.empty_email))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.empty_email))

        }
        AuthValidation.INVALID_EMAIL.value -> {
          requireView().showSnackBar(resources.getString(R.string.invalid_email))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.invalid_email))

        }
        AuthValidation.EMPTY_PASSWORD.value -> {
          requireView().showSnackBar(resources.getString(R.string.empty_password))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.empty_password))

        }
        AuthValidation.IMAGE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_image))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_image))

        }
        AuthValidation.EMPTY_CODE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_code))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_code))

        }
        AuthValidation.EMPTY_PHONE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_phone))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_phone))

        }
        AuthValidation.EMPTY_MESSAGE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_message))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_message))

        }
        AuthValidation.EMPTY_ADDRESS.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_address))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_address))
        }
        AuthValidation.EMPTY_STREET.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_street))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_street))
        }
        AuthValidation.EMPTY_COMMERCIAL.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_commercial_number))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_commercial_number))
        }
        AuthValidation.EMPTY_POSTAL_CODE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_postal_code))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_postal_code))
        }
        AuthValidation.EMPTY_PRICE.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_price))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_price))

        }
        AuthValidation.EMPTY_QUANTITY.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_qunatity))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_qunatity))

        }
        AuthValidation.EMPTY_DESCRIPTION.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_description))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_description))

        }

        AuthValidation.EMPTY_TIME.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_time))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_time))

        }
        AuthValidation.EMPTY_TAX.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_tax))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_tax))

        }

        AuthValidation.EMPTY_SERVICES.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_services))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_services))

        }

        AuthValidation.EMPTY_CUSTOMER.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_owner))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_owner))

        }
        //
        AuthValidation.EMPTY_OLD_PASSWORD.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_old_password))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_old_password))

        }
        AuthValidation.EMPTY_NEW_PASSWORD.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_new_password))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_new_password))

        }

        AuthValidation.EMPTY_CONFIRM_PASSWORD.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_confirm_password))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_confirm_password))

        }

        AuthValidation.EMPTY_NOTES.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_notes))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_notes))
        }

        AuthValidation.BOTH_PASSWORDS_MUST_BE_SAME.value -> {
          requireView().showSnackBar(resources.getString(R.string.both_password_must_be_same))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.both_password_must_be_same))
        }

        AuthValidation.EMPTY_VAT.value -> {
          requireView().showSnackBar(resources.getString(R.string.please_enter_your_vat))
          showNoApiErrorAlert(requireActivity(),resources.getString(R.string.please_enter_your_vat))
        }


      }
    }
  }

  override
  fun onResume() {
    super.onResume()

    registerListeners()
  }


  override
  fun onPause() {
    unRegisterListeners()

    super.onPause()
  }

  override
  fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    
    if (!hasInitializedRootView) {
      Log.d(TAG, "onViewCreatedDONE: ")

      getFragmentArguments()
      setBindingVariables()
      setUpViews()
      observeAPICall()
      setupObservers()

      hasInitializedRootView = true
    }
    Log.d(TAG, "onViewCreated: ")

  }

  fun handleLoading(it: Resource<BaseResponse<*>>) {
    println("handleAction $it")
    Log.d(TAG, "handleLoading: $it")
    when (it) {
      Resource.Loading -> {
        hideKeyboard()
        showLoading()
      }
      is Resource.HideProgress -> {
        hideLoading()
      }
      is Resource.Success -> {
        hideLoading()
      }
      is Resource.Failure -> {
        hideLoading()
        handleApiError(it)
      }
      else -> {}
    }


  }

  @LayoutRes
  abstract fun getLayoutId(): Int

  abstract fun validationObserver() : SingleLiveEvent<Int>

  open fun registerListeners() {}

  open fun unRegisterListeners() {}

  open fun getFragmentArguments() {}

  open fun setBindingVariables() {}

  open fun setUpViews() {}

  open fun observeAPICall() {}

  open fun setupObservers() {}

  fun showLoading() {
    hideLoading()
    progressDialog = showLoadingDialog(requireActivity(), null)
  }

  fun showLoading(hint: String?) {
    Log.d(TAG, "showLoading: herere $hint")
    hideLoading()
    progressDialog = showLoadingDialog(requireActivity(), hint)
  }

  fun hideLoading() = hideLoadingDialog(progressDialog, requireActivity())

  fun setLanguage(language: String) {
    (requireActivity() as BaseActivity<*>).updateLocale(language)
  }

  val currentLanguage: Locale
    get() = Locale.getDefault()

  private val TAG = "BaseFragment"
  // check Permissions
  private fun checkGalleryPermissions(fragmentActivity: FragmentActivity): Boolean {
    return fragmentActivity.checkGalleryPermissions()
  }

  fun checkStoragePermissions(fragmentActivity: FragmentActivity): Boolean {
    return fragmentActivity.checkStoragePermissions()
  }


  // Pick Single image
  @SuppressLint("CheckResult")
  fun singleTedBottomPicker(fragmentActivity: FragmentActivity) {
    if (checkGalleryPermissions(fragmentActivity)) {
      TedRxBottomPicker.with(fragmentActivity)
        .show()
        .subscribe({
          selectedImages.value = it
        }, Throwable::printStackTrace)
    }
  }

  @SuppressLint("CheckResult")
  fun multiTedBottomPicker(fragmentActivity: FragmentActivity) {
    if (checkGalleryPermissions(fragmentActivity)) {
      TedRxBottomPicker.with(fragmentActivity)
        .show()
        .subscribe({
          selectedImages.value = it
        }, Throwable::printStackTrace)
    }
  }



}