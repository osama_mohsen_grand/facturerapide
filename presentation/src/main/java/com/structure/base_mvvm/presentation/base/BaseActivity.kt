package com.structure.base_mvvm.presentation.base

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavController
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
import com.structure.base_mvvm.presentation.language.helper.MyContextWrapper
import com.zeugmasolutions.localehelper.LocaleHelper
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegate
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import java.util.Locale
//import com.google.android.gms.tasks.OnSuccessListener
//import com.google.firebase.messaging.FirebaseMessaging
//import com.structure.base_mvvm.presentation.R


abstract class BaseActivity<VB : ViewDataBinding> : AppCompatActivity() {
  private val localeDelegate: LocaleHelperActivityDelegate = LocaleHelperActivityDelegateImpl()
  private var _binding: VB? = null
  open val binding get() = _binding!!
  lateinit var nav: NavController

  override
  fun createConfigurationContext(overrideConfiguration: Configuration): Context {
    val context = super.createConfigurationContext(overrideConfiguration)
    return LocaleHelper.onAttach(context)
  }

  override
  fun getApplicationContext(): Context =
    localeDelegate.getApplicationContext(super.getApplicationContext())

  override
  fun onResume() {
    super.onResume()
    localeDelegate.onResumed(this)
  }

  override
  fun onPause() {
    super.onPause()
    localeDelegate.onPaused()
  }

  override
  fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    initViewBinding()
    setContentView(binding.root)

    if (savedInstanceState == null) {
      setUpBottomNavigation()
      setUpNavigationDrawer()
    }

    setUpViews()

    //when use rtl remove that
//    val locale = Locale.ENGLISH
//    Locale.setDefault(locale)
//    val resources: Resources = resources
//    val config: Configuration = resources.getConfiguration()
//    config.setLocale(locale)
//    resources.updateConfiguration(config, resources.getDisplayMetrics())
  }

  private val TAG = "BaseActivity"


  override
  fun onRestoreInstanceState(savedInstanceState: Bundle) {
    super.onRestoreInstanceState(savedInstanceState)
    setUpBottomNavigation()
    setUpNavigationDrawer()
  }

  private fun initViewBinding() {
    _binding = DataBindingUtil.setContentView(this, getLayoutId())
    binding.lifecycleOwner = this
    binding.executePendingBindings()
  }

  @LayoutRes
  abstract fun getLayoutId(): Int

  open fun setUpBottomNavigation() {}
  open fun setUpNavigationDrawer() {}

  open fun setUpViews() {}

  open fun updateLocale(language: String) {
    localeDelegate.setLocale(this, Locale(language))
  }

  override
  fun onSupportNavigateUp(): Boolean {
    return nav.navigateUp() || super.onSupportNavigateUp()
  }



  override fun attachBaseContext(newBase: Context?) {
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && newBase != null) {
      super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage(newBase)))
    }else {
      super.attachBaseContext(newBase)
    }
  }

  fun changeLanguage(context: Context,language: String) {
    LanguagesHelper.changeLanguage(context,language)
  }




  override
  fun getDelegate() = localeDelegate.getAppCompatDelegate(super.getDelegate())
}