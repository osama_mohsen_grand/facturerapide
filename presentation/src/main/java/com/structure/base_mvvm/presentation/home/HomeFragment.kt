package com.structure.base_mvvm.presentation.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import codes.pretty_pop_up.PrettyPopUpHelper
import com.structure.base_mvvm.domain.home.models.CategoryModel
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.NavHomeDirections
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.HomeFragmentBinding
import com.structure.base_mvvm.presentation.home.viewModels.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeFragmentBinding>() {

  private val viewModel: HomeViewModel by viewModels()


  override
  fun getLayoutId() = R.layout.home_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    setupList()
    viewModel.updateFirebase(getDeviceId(requireContext()))
  }

  fun setupList() {
    val categoryModels = ArrayList<CategoryModel>()
    categoryModels.add(
      CategoryModel(
        1,
        getString(R.string.create_an_invoice),
        "",
        R.drawable.img_create_indexation
      )
    )
    categoryModels.add(
      CategoryModel(
        2,
        getString(R.string.create_an_return),
        "",
        R.drawable.img_create_invoice
      )
    )
    categoryModels.add(
      CategoryModel(
        3,
        getString(R.string.create_an_assay),
        "",
        R.drawable.img_create_an_bounce
      )
    )
    categoryModels.add(
      CategoryModel(
        4,
        getString(R.string.create_an_order),
        "",
        R.drawable.img_create_order
      )
    )
    viewModel.adapter.insertData(categoryModels)
  }

  override
  fun setUpViews() {
    setUpToolBar()
  }

  fun nonInlined(block: () -> Unit) {
    println("before")
    block()
    println("after")
  }

  private val TAG = "HomeFragment"
  fun setUpToolBar() {
    nonInlined {
      println("do something here")
    }
  }

  val lastValue = ""
  override
  fun setupObservers() {
    lifecycleScope.launchWhenCreated {
      viewModel.homeResponse.collect {
        Log.d(TAG, "setupObservers: $it")
        handleLoading(it)
        if (it is Resource.Success) {
          if (it.value.data?.isSubscribed == 0) {
            navigateSafe(HomeFragmentDirections.actionHomeFragmentToPackagesFragment())
          }
          it.value.data?.notificationCount?.let { it1 ->
            (requireActivity() as HomeActivity).updateNotification(
              it1, it.value.data!!.isSubscribed
            )
          }
        }
      }
    }

    viewModel.clickEvent.observe(this) {
      if (it == Constants.INVOICES) {
        (requireActivity() as HomeActivity).binding.bottomNavigationView.selectedItemId = R.id.invoices_Fragment;
      }
    }
    viewModel.adapter.clickEvent.observe(this) {
      navigateSafe(HomeFragmentDirections.actionHomeFragmentToInvoiceFragment(it.id, -1,it.title))
    }
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}