package com.structure.base_mvvm.presentation.auth.log_in

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.account.use_case.GetUserFromLocalUseCase
import com.structure.base_mvvm.domain.account.use_case.SaveUserToLocalUseCase
//import app.facturerapide.core.utils.Utils.getDeviceIdWithoutPermission
import com.structure.base_mvvm.domain.auth.entity.request.LogInRequest
import com.structure.base_mvvm.domain.auth.use_case.LogInUseCase
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LogInViewModel @Inject constructor(
  private val logInUseCase: LogInUseCase,
  private val accountFirebase : AccountRepository,
  val saveUserToLocalUseCase: SaveUserToLocalUseCase,
  val rep : AccountRepository
) : BaseViewModel() {

  private  val TAG = "LogInViewModel"
  var request = LogInRequest()

  init {
    accountFirebase.getFirebaseToken()?.let {
      if(it.isEmpty()){
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
          if (!task.isSuccessful) {
            return@OnCompleteListener
          }
          var result = task.result
          Log.d(TAG, "setupFirebaseToken: $result")
          //shared perereference
          saveUserToLocalUseCase(result)
        })
      }

    }
  }
//  private val loginResponse = MutableStateFlow<Resource<BaseResponse<User>>>(Resource.Default)

  fun onLogInClicked() {
    viewModelScope.launch(job){
      logInUseCase.login(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
//    val job=
//    job.launchIn(viewModelScope)
  }
  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}