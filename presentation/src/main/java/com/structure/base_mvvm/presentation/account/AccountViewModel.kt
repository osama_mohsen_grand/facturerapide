package com.structure.base_mvvm.presentation.account

import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.account.repository.AccountRepository
import com.structure.base_mvvm.domain.account.use_case.AccountUseCases
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(private val accountUseCases: AccountUseCases,
                                           private  val accountRepository: AccountRepository,
) : BaseViewModel() {

  fun company(){
    clickEvent.value = Constants.COMPANY_REGISTER
  }

  fun profile(){
    clickEvent.value = Constants.PROFILE
  }

  fun packages(){
    clickEvent.value = Constants.PACKAGES
  }

  fun clearUser(){
    accountRepository.clearPreferences()
    clickEvent.value = Constants.LOGOUT
  }
}