package com.structure.base_mvvm.presentation.home.viewModels

import com.structure.base_mvvm.domain.settings.models.NotificationData
import com.structure.base_mvvm.presentation.base.BaseViewModel

class ItemNotificationViewModel  constructor(val model: NotificationData) : BaseViewModel()