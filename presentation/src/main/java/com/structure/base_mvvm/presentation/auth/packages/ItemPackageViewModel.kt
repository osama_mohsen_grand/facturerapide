package com.structure.base_mvvm.presentation.auth.packages

import android.content.Context
import android.graphics.Paint
import android.icu.number.Precision.currency
import android.util.Log
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.extensions.hide
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext

class ItemPackageViewModel constructor(val model: Package,val selected : Boolean = false,val position : Int = -1) : BaseViewModel() {

  private  val TAG = "ItemPackageViewModel"
  init {
//    Log.d(TAG, ": ${model.packageId}")
//    Log.d(TAG, ": ${model.durationType}")
//    Log.d(TAG, ": ${model.price}")
//    Log.d(TAG, "===================: ")
  }
  companion object {

    @BindingAdapter("app:priceBefore")
    @JvmStatic
    fun priceBefore(textView: AppCompatTextView, model: Package) {
      when (model.priceBefore) {
        0.0 -> textView.hide()
        else -> {
          textView.text =
            "${model.priceBefore} " + textView.context.getString(R.string.currency_france) // SomeString = your old price
          textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
      }
    }

    @BindingAdapter("app:priceAfter")
    @JvmStatic
    fun priceAfter(textView: AppCompatTextView, model: Package) {
      textView.text =
        "${model.price} " + textView.context.getString(R.string.currency_france) // SomeString = your old price

    }

    @BindingAdapter("app:textTitle")
    @JvmStatic
    fun loadText(textView: AppCompatTextView, model: Package) {
      when (model.isFree) {
        1 -> {
          textView.text =
            textView.context.getString(R.string.free_trial, model.period, model.periodType)
        }
        else -> {
          textView.text =
            textView.context.getString(R.string.package_duration, model.period, model.periodType)
        }
      }
    }
  }


  fun getTextTitle(): Package {
    return model
  }


  fun getPriceBefore(): Package {
    return model
  }

  fun getPriceAfter(): Package {
    return model
  }

}