package com.structure.base_mvvm.presentation.auth.sign_up.company

import android.content.Intent
import android.graphics.Color
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.FragmentSignUpBinding
import com.structure.base_mvvm.presentation.databinding.FragmentSignUpCompanyBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import top.defaults.colorpicker.ColorPickerPopup

@AndroidEntryPoint
class SignUpCompanyFragment : BaseFragment<FragmentSignUpCompanyBinding>() {

  private val viewModel: SignUpCompanyViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.fragment_sign_up_company

  private  val TAG = "SignUpCompanyFragment"
  override
  fun setBindingVariables() {
    Log.d(TAG, "setBindingVariables: done")
    binding.viewModel = viewModel
    if(viewModel.request.isLogin){
      Log.d(TAG, "setBindingVariables: isLogin")
      binding.imgLoginLogo.loadCircleImage(viewModel.userUseCase.accountRepository.getUserLocal().company.image,null)
    }
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      when(it){
        Constants.PICKER_IMAGE -> singleTedBottomPicker(requireActivity())
        Constants.COLOR -> colorPicker()
      }
    }
    selectedImages.observeForever {
      selectedImages.value?.path?.let { it1 ->
        run {
          viewModel.request.setImage(it1,"company_image")
          viewModel.notifyChange()
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          requireActivity().finishAffinity()
          startActivity(Intent(context, HomeActivity::class.java))
//          navigateSafe(SignUpCompanyFragmentDirections.actionSignUpCompanyFragmentToLogInFragment())
        }
      }
    }
  }
  fun colorPicker(){
    ColorPickerPopup.Builder(requireContext())
      .initialColor(Color.RED) // Set initial color
      .enableBrightness(true) // Enable brightness slider or not
      .enableAlpha(true) // Enable alpha slider or not
      .okTitle(getString(R.string.choose))
      .cancelTitle(getString(R.string.cancel))
      .showIndicator(true)
      .showValue(true)
      .build()
      .show(binding.root, object : ColorPickerPopup.ColorPickerObserver() {
        override fun onColorPicked(color: Int) {
          binding.tvPickPdfColor.setTextColor(color)
          binding.imgShapeColor.setBackgroundColor(color)
          val hexColor = color.toColorHex()
          viewModel.request.invoiceColor = hexColor
//          viewModel.request.invoiceColor = color.toString()
        }

        fun onColor(color: Int, fromUser: Boolean) {}
      })
  }


  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}