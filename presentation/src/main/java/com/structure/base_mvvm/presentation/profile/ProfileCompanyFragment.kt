package com.structure.base_mvvm.presentation.profile

import android.graphics.Color
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.sign_up.company.SignUpCompanyViewModel
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ProfileCompanyFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import top.defaults.colorpicker.ColorPickerPopup
import top.defaults.colorpicker.ColorPickerPopup.ColorPickerObserver
import java.lang.String


@AndroidEntryPoint
class ProfileCompanyFragment : BaseFragment<ProfileCompanyFragmentBinding>() {

  private val viewModel: SignUpCompanyViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.profile_company_fragment
  var colorInt = 0

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    if (viewModel.userUseCase.accountRepository.getUserLocal().company.image != null && viewModel.userUseCase.accountRepository.getUserLocal().company.image?.isNotEmpty() == true)
      binding.imgLoginLogo.loadRoundImage(
        viewModel.userUseCase.accountRepository.getUserLocal().company.image,
        null
      )

    if (viewModel.request.invoiceColor.isNotEmpty()) {
      colorInt = viewModel.request.invoiceColor.toColorInt()
      binding.tvPickPdfColor.setTextColor(colorInt)
      binding.imgShapeColor.setBackgroundColor(colorInt)
    }
  }

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      when (it) {
        Constants.PICKER_IMAGE -> singleTedBottomPicker(requireActivity())
        Constants.COLOR -> colorPicker()
      }
    }
    selectedImages.observeForever {
      selectedImages.value?.path?.let { it1 ->
        run {
          viewModel.request.setImage(it1, "company_image")
          viewModel.notifyChange()
        }
      }
    }
    lifecycleScope.launchWhenResumed {
      viewModel.response.collect {
        handleLoading(it)
        if (it is Resource.Success) {
          showMessageSuccess(it.value.message)
          backToPreviousScreen()
        }
      }
    }
  }


  private val TAG = "ProfileCompanyFragment"
  fun colorPicker() {
    Log.d(TAG, "colorPicker: here")
    val colorPicker = ColorPickerPopup.Builder(requireContext())
      .initialColor(Color.RED) // Set initial color
      .enableBrightness(true) // Enable brightness slider or not
      .enableAlpha(true) // Enable alpha slider or not
      .okTitle(getString(R.string.choose))
      .cancelTitle(getString(R.string.cancel))
      .showIndicator(true)
      .showValue(true)
    if (viewModel.request.invoiceColor.isNotEmpty() && colorInt != 0) {
      colorPicker.initialColor(colorInt);
    }
    colorPicker.build()
      .show(binding.root, object : ColorPickerObserver() {
        override fun onColorPicked(color: Int) {
//          v.setBackgroundColor(color)
          binding.tvPickPdfColor.setTextColor(color)
          binding.imgShapeColor.setBackgroundColor(color)
          val hexColor = color.toColorHex()
          viewModel.request.invoiceColor = hexColor
        }

        fun onColor(color: Int, fromUser: Boolean) {}
      })
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}