package com.structure.base_mvvm.presentation.invoice.service

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.structure.base_mvvm.domain.invoice.request.InvoiceServiceModel
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.databinding.ServiceFragmentBinding
import com.structure.base_mvvm.presentation.invoice.InvoiceFragment

class ItemServiceViewModel constructor(
  val model: InvoiceServiceModel,
  val position: Int,
  val showDelete: Boolean,
  val viewModel: ServicesViewModel
) : BaseViewModel() {

  private val TAG = "ItemServiceViewModel"

  private lateinit var dialog: BottomSheetDialog
  fun submit(context: Context, v: View) {
    Log.d(TAG, "submit: HERE")
    val destination = v.findNavController().currentDestination
    when (destination?.id) {
      R.id.servicesFragment -> {
        val fragment = v.findFragment<ServicesFragment>()
        fragment.viewModel.selectService(position)
      }
      R.id.invoiceFragment -> {
        if (!this::dialog.isInitialized) {
          val fragment = v.findFragment<InvoiceFragment>()
          dialog = BottomSheetDialog(context, R.style.BottomSheetDialogStyle)

          val binding: ServiceFragmentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.service_fragment,
            null,
            false
          )
          viewModel.invoiceServiceModel = model
          viewModel.clickEvent = clickEvent
          binding.viewModel = viewModel
          dialog.setContentView(binding.root)
          viewModel.clickEvent.observeForever {
            if(viewModel.isAllowSubmit && viewModel.isAllowSubmit){
              fragment.viewModel.updateService(viewModel.invoiceServiceModel,position)
              dialog.dismiss()
            }
          }
        }
        dialog.show()
      }
    }
  }
}