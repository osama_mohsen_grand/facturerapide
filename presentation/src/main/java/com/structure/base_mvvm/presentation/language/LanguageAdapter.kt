package com.structure.base_mvvm.presentation.language

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.structure.base_mvvm.domain.invoice.model.InvoiceItem
import com.structure.base_mvvm.domain.language.Language
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.ItemInvoiceBinding
import com.structure.base_mvvm.presentation.databinding.ItemLanguageBinding
import com.structure.base_mvvm.presentation.invoices.viewModels.ItemInvoiceViewModel
import com.structure.base_mvvm.presentation.language.viewModels.ItemLanguageViewModel

class LanguageAdapter: RecyclerView.Adapter<LanguageAdapter.ViewHolder>() {
  var clickEvent: SingleLiveEvent<Language> = SingleLiveEvent()
  var selected = -1

  private val differCallback = object : DiffUtil.ItemCallback<Language>() {
    override fun areItemsTheSame(oldItem: Language, newItem: Language): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Language, newItem: Language): Boolean {
      return oldItem == newItem
    }

  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_language, parent, false)
    return ViewHolder(view)
  }


  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemLanguageViewModel(data,position,selected == position)
    holder.setViewModel(itemViewModel)
  }

  private val TAG = "InvoicesAdapter"
  fun insertData(insertList: List<Language>) {
    val array = ArrayList<Language>(differ.currentList)
    val size = array.size
    array.addAll(insertList)
    Log.d(TAG, "insertData: "+size)
//    notifyItemRangeInserted(size,array.size)
    differ.submitList(array)
    notifyDataSetChanged()
  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

  fun update(position: Int) {
    selected = position
    notifyDataSetChanged()
  }


  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    lateinit var itemLayoutBinding: ItemLanguageBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemLanguageViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }


}