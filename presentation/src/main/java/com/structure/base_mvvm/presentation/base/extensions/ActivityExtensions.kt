package com.structure.base_mvvm.presentation.base.extensions

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.util.Log
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.utils.DateUtils
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


fun <A : Activity> Activity.openActivityAndClearStack(activity: Class<A>) {
  Intent(this, activity).apply {
    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(this)
    finish()
  }
}

fun <A : Activity> Activity.openActivityAndClearStack(activity: Class<A>, bundle: Bundle) {
  Intent(this, activity).apply {
    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    putExtra(Constants.BUNDLE, bundle)
    startActivity(this)
    finish()
  }
}

fun <A : Activity> Activity.openActivity(activity: Class<A>) {
  Intent(this, activity).apply {
    startActivity(this)
  }
}

fun Activity.checkGalleryPermissions(): Boolean {
  val array = arrayOf(
    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
    Manifest.permission.READ_EXTERNAL_STORAGE
  )
  val deniedPermissions = ArrayList<String>()

  for (permission in array)
    if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED)
      deniedPermissions.add(permission)

  if (deniedPermissions.isNotEmpty()) {
    val permissions = arrayOfNulls<String>(deniedPermissions.size)
    for (index in 0 until deniedPermissions.size) {
      permissions[index] = deniedPermissions[index]
    }
    ActivityCompat.requestPermissions(this, permissions, Constants.PERMISSION_GALLERY)
  }
  return deniedPermissions.isEmpty()
}

fun Activity.checkStoragePermissions(): Boolean {
  val array = arrayOf( Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
  )
  val deniedPermissions = ArrayList<String>()

  for (permission in array)
    if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED)
      deniedPermissions.add(permission)

  if (deniedPermissions.isNotEmpty()) {
    val permissions = arrayOfNulls<String>(deniedPermissions.size)
    for (index in 0 until deniedPermissions.size) {
      permissions[index] = deniedPermissions[index]
    }
    ActivityCompat.requestPermissions(this, permissions, Constants.PERMISSION_GALLERY)
  }
  return deniedPermissions.isEmpty()
}
