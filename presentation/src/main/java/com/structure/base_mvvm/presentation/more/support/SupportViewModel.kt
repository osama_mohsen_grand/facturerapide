package com.structure.base_mvvm.presentation.more.support

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.entity.request.RegisterRequest
import com.structure.base_mvvm.domain.auth.use_case.RegisterUseCase
import com.structure.base_mvvm.domain.settings.request.SupportRequest
import com.structure.base_mvvm.domain.settings.use_case.SettingsUseCase
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SupportViewModel @Inject constructor(private val useCase: SettingsUseCase) : BaseViewModel() {

  var request = SupportRequest()

  private val TAG = "SupportViewModel"
  fun send(){
    viewModelScope.launch(job){
      useCase.support(request)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          response.value = result
        }.launchIn(viewModelScope)
    }
  }

  fun reset() {
    request.reset()
    notifyChange()
  }

  override fun onCleared() {
    job.cancel()
    super.onCleared()
  }
}