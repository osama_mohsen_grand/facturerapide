package com.structure.base_mvvm.presentation.auth.forgot_password

import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.entity.request.ForgetPasswordRequest
import com.structure.base_mvvm.domain.auth.use_case.ForgetPasswordUseCase
import com.structure.base_mvvm.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(private val useCase: ForgetPasswordUseCase) :
  BaseViewModel() {
  var request = ForgetPasswordRequest()

  fun submit() {
    useCase(request)
      .catch { exception -> validationException.value = exception.message?.toInt() }
      .onEach { result ->
        response.value = result
      }
      .launchIn(viewModelScope)
  }
}