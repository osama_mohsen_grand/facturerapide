package com.structure.base_mvvm.presentation.base.extensions

import android.graphics.Color

fun Int.toColorHex(): String{
  return java.lang.String.format("#%06X", 0xFFFFFF and this)
}

fun String.toColorInt(): Int{
  try{
    return Color.parseColor(this)
  }catch (e: Exception){
    e.printStackTrace()
  }
  return 38867
}