package com.structure.base_mvvm.presentation.base.extensions

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.util.Log
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.base.utils.DateUtils
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


fun Activity.showDateTime(dateTimeText: AppCompatEditText,locale: Locale) {
  val sdf = SimpleDateFormat(DateUtils.API_DATE_FORMAT,locale)
  val d = Date()
  sdf.format(d)
  Log.e("ACTIVITY", "format")

  val calender = Calendar.getInstance()

//  calender.time = d
  val yearCurrent = calender.get(Calendar.YEAR)
  val monthCurrent = calender.get(Calendar.MONTH)
  val dayCurrent = calender.get(Calendar.DAY_OF_MONTH)
  val dpd = DatePickerDialog(
    this,
    { view, year, monther, dayOfMonth ->
      val str_day = if (dayOfMonth < 10) "0$dayOfMonth" else "" + dayOfMonth
      var month = monther
      month++
      val str_month = if (month < 10) "0$month" else "" + month
      var date_select = "$year-$str_month-$str_day"

      val calendar: Calendar = Calendar.getInstance()
      val hour = calendar.get(Calendar.HOUR)
      val minute = calendar.get(Calendar.MINUTE)

      val timePickerDialog = TimePickerDialog(this, {
          view,hour,minute ->
        val str_hour = if (hour < 10) "0$hour" else "" + hour
        val str_minute = if (minute < 10) "0$minute" else "" + minute

        dateTimeText.setText("$date_select $str_hour:$str_minute")
      }, hour, minute, true)
      timePickerDialog.show()

    },
    yearCurrent,
    monthCurrent,
    dayCurrent
  )
//  dpd.datePicker.minDate = System.currentTimeMillis() - 1000

  dpd.show()

}


fun Activity.showDate(dateTimeText: AppCompatEditText,disablePreviousDays : Boolean,locale: Locale) {
  val sdf = SimpleDateFormat(DateUtils.UI_DATE_FORMAT,locale)
  val d = Date()
  sdf.format(d)

  val calender = Calendar.getInstance()

//  calender.time = d
  val yearCurrent = calender.get(Calendar.YEAR)
  val monthCurrent = calender.get(Calendar.MONTH)
  val dayCurrent = calender.get(Calendar.DAY_OF_MONTH)
  val dpd = DatePickerDialog(
    this,
    { view, year, monther, dayOfMonth ->
      val str_day = if (dayOfMonth < 10) "0$dayOfMonth" else "" + dayOfMonth
      var month = monther
      month++
      val str_month = if (month < 10) "0$month" else "" + month
      var date_select = "$str_day-$str_month-$year"
      dateTimeText.setText(date_select)

    },
    yearCurrent,
    monthCurrent,
    dayCurrent
  )
  if(disablePreviousDays) dpd.datePicker.minDate = System.currentTimeMillis() - 1000

  dpd.show()

}

fun Activity.currentDateFormat(format:String = "dd-MM-yyyy",local: Locale = Locale.getDefault()) : String{
  val c: Date = Calendar.getInstance().time
  println("Current time => $c")

  val df = SimpleDateFormat(format, local)
  return df.format(c)
}

fun Activity.showTime(dateTimeText: AppCompatEditText) {
  val calendar: Calendar = Calendar.getInstance()
  val hourCurrent = calendar.get(Calendar.HOUR)
  val minuteCurrent = calendar.get(Calendar.MINUTE)

  val timePickerDialog = TimePickerDialog(this, {
      view,hour,minute ->
    val str_hour = if (hour < 10) "0$hour" else "" + hour
    val str_minute = if (minute < 10) "0$minute" else "" + minute
    dateTimeText.setText("$str_hour : $str_minute")
  }, hourCurrent, minuteCurrent, true)
  timePickerDialog.show()

}