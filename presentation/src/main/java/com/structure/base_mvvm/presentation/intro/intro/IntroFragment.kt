package com.structure.base_mvvm.presentation.intro.intro

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.AuthActivity
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.extensions.openActivityAndClearStack
import com.structure.base_mvvm.presentation.base.extensions.startActivityWithClearHistory
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.databinding.FragmentIntroBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class IntroFragment : BaseFragment<FragmentIntroBinding>() {
  //intro fragment
  private val viewModel: IntroViewModel by viewModels()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    Log.d(TAG, "onCreateView: ")
    return super.onCreateView(inflater, container, savedInstanceState)
  }

  override
  fun getLayoutId() = R.layout.fragment_intro

  override
  fun setBindingVariables() {
    Log.d(TAG, "setBindingVariables: ")
    binding.viewModel = viewModel

  }

  private  val TAG = "IntroFragment"
  override
  fun setUpViews() {
    Log.d(TAG, "setUpViews: ")
    viewModel.callIntroApi()
    lifecycleScope.launchWhenCreated {
      Log.d(TAG, "setUpViews: HERE")
      viewModel.responseResult.collect {
        Log.d(TAG, "setUpViews: HANDLE : $it")
        handleLoading(it)
        if (it is Resource.Success) {
          it.value.data?.let { it1 -> viewModel.setValue(it1) }
        }
      }
    }
    viewModel.clickEventSubmit.observe(this, {
      viewModel.setFirstTime(false)
      navigateSafe(IntroFragmentDirections.actionIntroFragmentToLogInFragment())
    })
  }


  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      viewModel.setFirstTime(false)
//      navigateSafe(IntroFragmentDirections.)
    }
  }

  private fun openLogIn() {
    openActivityAndClearStack(AuthActivity::class.java)
  }

  private fun openHome() {
    openActivityAndClearStack(HomeActivity::class.java)
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}