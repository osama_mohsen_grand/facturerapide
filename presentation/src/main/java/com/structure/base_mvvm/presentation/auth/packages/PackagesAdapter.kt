package com.structure.base_mvvm.presentation.auth.packages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.databinding.ItemPackageBinding

class PackagesAdapter: CardSliderAdapter<PackagesAdapter.ViewHolder>() {

  override fun bindVH(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemPackageViewModel(data)
    holder.setViewModel(itemViewModel)
  }

  private val differCallback = object : DiffUtil.ItemCallback<Package>() {
    override fun areItemsTheSame(oldItem: Package, newItem: Package): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Package, newItem: Package): Boolean {
      return oldItem == newItem
    }
  }
  val differ = AsyncListDiffer(this, differCallback)
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_package, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val data = differ.currentList[position]
    val itemViewModel = ItemPackageViewModel(data)
    holder.setViewModel(itemViewModel)
  }

  override fun getItemId(position: Int): Long {
    return super.getItemId(position)
  }

  override fun getItemCount(): Int {
    return differ.currentList.size
  }

  override fun onViewAttachedToWindow(holder: ViewHolder) {
    super.onViewAttachedToWindow(holder)
    holder.bind()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder) {
    super.onViewDetachedFromWindow(holder)
    holder.unBind()
  }

  inner class ViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    private lateinit var itemLayoutBinding: ItemPackageBinding

    init {
      bind()
    }

    fun bind() {
      itemLayoutBinding = DataBindingUtil.bind(itemView)!!
    }

    fun unBind() {
      itemLayoutBinding.unbind()
    }

    fun setViewModel(itemViewModel: ItemPackageViewModel) {
      itemLayoutBinding.viewModel = itemViewModel
    }
  }

  /**
   * This method should update the contents of the {@link VH#itemView} to reflect the item at the
   * given position.
   *
   * @param holder The ViewHolder which should be updated to represent the contents of the
   *        item at the given position in the data set.
   * @param position The position of the item within the adapter's data set.
   */



}