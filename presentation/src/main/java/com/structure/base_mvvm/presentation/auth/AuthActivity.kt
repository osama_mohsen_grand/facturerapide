package com.structure.base_mvvm.presentation.auth

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import androidx.core.view.postDelayed
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.auth.log_in.LogInFragmentDirections
import com.structure.base_mvvm.presentation.base.BaseActivity
import com.structure.base_mvvm.presentation.base.extensions.hide
import com.structure.base_mvvm.presentation.base.extensions.navigateSafe
import com.structure.base_mvvm.presentation.base.extensions.show
import com.structure.base_mvvm.presentation.databinding.ActivityAuthBinding
import com.structure.base_mvvm.presentation.home.HomeActivity
import com.structure.base_mvvm.presentation.language.helper.LanguagesHelper
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class AuthActivity : BaseActivity<ActivityAuthBinding>() {
  private lateinit var appBarConfiguration: AppBarConfiguration

  override
  fun getLayoutId() = R.layout.activity_auth

  private  val TAG = "AuthActivity"


  fun setLocal(){
    val locale = Locale(LanguagesHelper.getCurrentLanguage(this))
    Locale.setDefault(locale)
    val resources: Resources = getResources()
    val config: Configuration = resources.getConfiguration()
    config.setLocale(locale)
    resources.updateConfiguration(config, resources.getDisplayMetrics())
  }

  override fun setUpBottomNavigation() {
    super.setUpBottomNavigation()
    setLocal()
    Locale.setDefault(Locale(LanguagesHelper.getCurrentLanguage(this)));

    val navHostFragment =
      supportFragmentManager.findFragmentById(R.id.nav_host_container) as NavHostFragment
    nav = navHostFragment.findNavController()
    val sets = setOf(R.id.log_in_fragment,R.id.splashFragment2,R.id.log_in_fragment)

    nav.addOnDestinationChangedListener { controller, destination, arguments ->
      binding.toolbar.background = if (destination.id == R.id.signUpCompanyFragment) {
        binding.toolbar.post {
          binding.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_special_back_2)
        }

        ContextCompat.getDrawable(this, R.drawable.app_bar)
      }else {
        ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent))
      }
      when (destination.id) {
        R.id.splashFragment2,R.id.introFragment,R.id.log_in_fragment-> {
          binding.toolbar.hide()
        }
        else -> binding.toolbar.show()
      }
    }

    appBarConfiguration = AppBarConfiguration(sets)

    setSupportActionBar(binding.toolbar)
    binding.toolbar.setupWithNavController(nav, appBarConfiguration)
  }

}