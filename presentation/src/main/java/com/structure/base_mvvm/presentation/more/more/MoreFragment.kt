package com.structure.base_mvvm.presentation.more.more

import androidx.fragment.app.viewModels
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseFragment
import com.structure.base_mvvm.presentation.base.extensions.*
import com.structure.base_mvvm.presentation.base.extensions.shareApp
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import com.structure.base_mvvm.presentation.base.utils.getDeviceId
import com.structure.base_mvvm.presentation.databinding.MoreFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoreFragment : BaseFragment<MoreFragmentBinding>() {

  private val viewModel: MoreViewModel by viewModels()

  override
  fun getLayoutId() = R.layout.more_fragment

  override
  fun setBindingVariables() {
    binding.viewModel = viewModel
    viewModel.request.device_token = getDeviceId(requireContext())
  }

  private val TAG = "MoreFragment"

  override
  fun setupObservers() {
    viewModel.clickEvent.observe(this) {
      when (it) {
        Constants.TERMS_AND_CONDITION, Constants.PRIVACY, Constants.ABOUT_US -> openInfoPage(
          it, when (it) {
            Constants.TERMS_AND_CONDITION -> getString(R.string.terms_and_conditions)
            Constants.PRIVACY -> getString(R.string.privacy_policy)
            else -> getString(R.string.about_app)
          }
        )
        Constants.CONTACT_US, Constants.COMPLAINT -> navigateSafe(
          MoreFragmentDirections.actionMoreFragmentToSupportFragment(
            when (it) {
              Constants.CONTACT_US -> getString(R.string.technical_support)
              else -> getString(R.string.suggestions_and_complaints)
            }, it
          )
        )
        Constants.LANGUAGE -> {
          navigateSafe(MoreFragmentDirections.actionMoreFragmentToLanguageFragment())
        }
        Constants.SHARE -> shareApp()
        Constants.RATE_APP -> rateApp()
      }
    }
  }

  private fun openInfoPage(type: String, title: String) {
    navigateSafe(MoreFragmentDirections.actionMoreFragmentToInfoFragment(title, type))
  }

  override fun validationObserver(): SingleLiveEvent<Int> {
    return viewModel.validationException
  }
}