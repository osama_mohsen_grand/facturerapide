package com.structure.base_mvvm.presentation.more.info

import android.os.Build
import android.text.Html
import android.util.Log
import android.webkit.WebView
import android.webkit.WebView.RENDERER_PRIORITY_BOUND
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.viewModelScope
import com.structure.base_mvvm.domain.auth.entity.request.RegisterRequest
import com.structure.base_mvvm.domain.auth.use_case.RegisterUseCase
import com.structure.base_mvvm.domain.packages.entity.Package
import com.structure.base_mvvm.domain.settings.models.IntroModel
import com.structure.base_mvvm.domain.settings.use_case.SettingsUseCase
import com.structure.base_mvvm.domain.utils.BaseResponse
import com.structure.base_mvvm.domain.utils.Constants
import com.structure.base_mvvm.domain.utils.Resource
import com.structure.base_mvvm.presentation.R
import com.structure.base_mvvm.presentation.base.BaseViewModel
import com.structure.base_mvvm.presentation.base.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InfoViewModel @Inject constructor(private val useCase: SettingsUseCase) : BaseViewModel() {

  private val TAG = "InfoViewModel"
  val responseResult = MutableStateFlow<Resource<BaseResponse<IntroModel>>>(Resource.Default)
  var intro: IntroModel = IntroModel()
  var type = ""

  fun callSettingsApi() {
    println("set call settings Api $type")
    Log.d(TAG, "callIntroApi: callSettingsApi")
    viewModelScope.launch(job) {
      useCase.info(type)
        .catch { exception -> validationException.value = exception.message?.toInt() }
        .onEach { result ->
          responseResult.value = result
        }.launchIn(viewModelScope)
    }
  }


  fun setValue(value: IntroModel) {
    this.intro = value
    show = true
    notifyChange()
  }
}