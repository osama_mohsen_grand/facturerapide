package com.structure.base_mvvm.presentation.invoice.customer

import android.view.View
import androidx.fragment.app.findFragment
import androidx.fragment.app.viewModels
import com.structure.base_mvvm.domain.customer.CustomerModel
import com.structure.base_mvvm.domain.settings.models.NotificationData
import com.structure.base_mvvm.presentation.base.BaseViewModel

class ItemCustomerViewModel  constructor(val model: CustomerModel,val position: Int) : BaseViewModel(){

  fun submit(v: View){
    v.findFragment<CustomersFragment>().viewModel.selectCustomer(position)
  }
}