plugins {
  id(Config.Plugins.androidLibrary)
  id(Config.Plugins.kotlinAndroid)
  id(Config.Plugins.kotlinKapt)
  id(Config.Plugins.hilt)
  id(Config.Plugins.navigationSafeArgs)
  id(Config.Plugins.google_services)
}

android {
  compileSdk = Config.AppConfig.compileSdkVersion

  defaultConfig {
    minSdk = Config.AppConfig.minSdkVersion

    vectorDrawables.useSupportLibrary = true
  }

  buildTypes {
    getByName("release") {
      proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
    }
  }

  dataBinding {
    isEnabled = true
  }
}

dependencies {

  // Support
  implementation(Libraries.appCompat)
  implementation(Libraries.coreKtx)
  implementation(Libraries.androidSupport)

  // Arch Components
  implementation(Libraries.viewModel)
  implementation(Libraries.lifeData)
  implementation(Libraries.lifecycle)
  implementation(Libraries.viewModelState)

  // Kotlin Coroutines
  implementation(Libraries.coroutinesCore)
  implementation(Libraries.coroutinesAndroid)

  // UI
  implementation(Libraries.materialDesign)
  implementation(Libraries.navigationFragment)
  implementation(Libraries.navigationUI)
  implementation(Libraries.loadingAnimations)
  implementation(Libraries.alerter)
  implementation(Libraries.coil)

  // Utils
  implementation(Libraries.playServices)
  implementation(Libraries.localization)
  implementation(Libraries.permissions)
  implementation(Libraries.gson)

  // Hilt
  implementation(Libraries.hilt)
  implementation("androidx.legacy:legacy-support-v4:1.0.0")
  implementation("com.google.firebase:firebase-messaging-ktx:23.0.0")
  implementation(project(mapOf("path" to ":data")))
//  implementation(project(mapOf("path" to ":data")))
//  implementation("com.android.billingclient:billing:4.0.0")
//  implementation("com.android.billingclient:billing-ktx:4.0.0")
//  implementation("com.google.android.gms:play-services-wallet:19.1.0")
  implementation("com.google.firebase:firebase-crashlytics-buildtools:2.9.2")
  kapt(Libraries.hiltDaggerCompiler)

  // Map
  implementation(Libraries.map)
  implementation(Libraries.playServicesLocation)
  implementation(Libraries.rxLocation)
  implementation(Libraries.firebase_messaging)

  //Ted bottom picker
  implementation(Libraries.ted_bottom_picker)

  //cards slider
  implementation(Libraries.cards_slider)

  //Pin code
  implementation(Libraries.pin_code)
  //smarteist
  implementation(Libraries.smartteist)
//  lottie
  implementation(Libraries.lottie)

  //notification Badge
  implementation(Libraries.notificationBadge)

  //html
  implementation("com.github.SakaGamer:html-textview:1.0.8")

  //color picker
  implementation("com.github.duanhong169:colorpicker:1.1.6")


  //billing
  implementation("com.android.billingclient:billing:5.0.0")
  implementation("com.android.billingclient:billing-ktx:5.0.0")
  implementation("com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava")
  implementation("com.google.guava:guava:24.1-jre")

  // Project Modules
  implementation(project(Config.Modules.domain))
  implementation(project(Config.Modules.prettyPopUp))
  implementation(project(Config.Modules.appTutorial))
}
